<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 02/08/18
 * Time: 12:41 AM
 */


return [

    'dash'                              =>  [
        'data'                          =>  'Information about users',
        'num_users'                     =>  'Users quantity',
        'num_roles'                     =>  'Number of roles',
        'num_permissions'               =>  'Number of permits',
    ],

];