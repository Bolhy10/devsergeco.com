<?php

namespace Modules\Auth\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(array(
            'name' => 'admin',
            'display_name' => 'Administrador',
            'description' => 'Administra los módulos de usuarios',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ));

        DB::table('roles')->insert(array(
            'name' => 'supervisor',
            'display_name' => 'Supervisor',
            'description' => 'Encargado de las Ots creadas',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ));

        DB::table('roles')->insert(array(
            'name' => 'inspector',
            'display_name' => 'Inspector',
            'description' => 'Usuario al cual se le asigna una Ot',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ));

        DB::table('roles')->insert(array(
            'name' => 'warehouse-chief',
            'display_name' => 'Almacenista en Jefe',
            'description' => 'Encargado del ingreso de inventarios (Jefe)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ));

        DB::table('roles')->insert(array(
            'name' => 'warehouseman',
            'display_name' => 'Almacenista Local',
            'description' => 'Usuario encargado de la parte de ingresos de inventarios',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ));


     for($i=1; $i < 29; $i++){
         DB::table('permission_role')->insert(array(
             'permission_id' => $i,
             'role_id' => 1
         ));
     }

    }
}
