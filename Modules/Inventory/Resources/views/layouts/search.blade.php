
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="panel panel-default card-view panel-refresh">
        <div class="refresh-container">
            <div class="la-anim-1"></div>
        </div>
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">{{ trans('inventory::ui.inventory.search') }}</h6>
            </div>
            <div class="pull-right">
                <a class="pull-left inline-block mr-15" data-toggle="collapse"
                   href="#collapse_2" aria-expanded="true">
                    <i class="fa fa-arrows-v"></i>
                </a>
                <a class="pull-left inline-block close-panel" href="#" data-effect="fadeOut">
                    <i class="fa fa-close"></i>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="collapse_2" class="panel-wrapper collapse" aria-expanded="true" style="">

            <div class="panel-body">

                <form method="POST" @submit.prevent="searchItem">
                    <div class="col-lg-12 col-xs-12">
                        <div class="input-group" :class="{'has-error': errors.has('code')}">
                            <input type="number" min="1"
                                   name="code"
                                   v-model="code"
                                   v-validate="'required'"
                                   class="form-control"
                                   placeholder="Código">
                            <div class="input-group-btn">
                                <button type="submit"
                                        class="btn
                                        btn-info">Buscar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
