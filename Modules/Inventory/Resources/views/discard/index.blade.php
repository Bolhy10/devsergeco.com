@extends('layouts.master')

@section('pageTitle', trans('inventory::ui.discard.title'))

@section('content')

    <div id="discard">

        @include('inventory::discard.lists')

       @include('inventory::discard.edit')

    </div>

@stop
