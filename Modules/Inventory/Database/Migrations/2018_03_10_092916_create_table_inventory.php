<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('inventoryitems_id')->unsigned();
            $table->integer('clients_id')->unsigned();
            $table->integer('warehouses_id')->unsigned();
            $table->integer('quantity');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('inventoryitems_id')->references('id')->on('inventoryitems');
            $table->foreign('clients_id')->references('id')->on('clients');
            $table->foreign('warehouses_id')->references('id')->on('warehouses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
