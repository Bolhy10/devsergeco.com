<?php

namespace Modules\Returns\Entities;

use Illuminate\Database\Eloquent\Model;

class Returns extends Model
{
    protected $fillable = [
        'ot_id',
        'status',
        'prints'
    ];
    protected $table = 'returns';

    public function returns_item()
    {
        return $this->hasMany('Modules\Returns\Entities\ReturnsItems');
    }

    public function ot()
    {
        return $this->belongsTo('Modules\Ot\Entities\Ot');
    }

    public function get_return($id)
    {

        return $this->select('returns.status','returns.created_at','returns.updated_at','returns_items.id AS sti_id',
            'returns_items.quantity AS sti_qty','returns_items.inventory_id AS sti_invid','returns_items.returns_id AS sti_dispatch',
            'inventoryitems.code_sku','inventoryitems.description','clients.fullname',
            'inventorytypes.type')
            ->join('returns_items','returns.id','=','returns_items.returns_id')
            ->join('inventory','returns_items.inventory_id','=','inventory.id')
            ->join('inventoryitems','inventory.inventoryitems_id','=','inventoryitems.id')
            ->join('inventorytypes','inventoryitems.inventorytypes_id','=','inventorytypes.id')
            ->join('clients','inventory.clients_id','=','clients.id')
            ->where(['returns.id' => $id,'returns_items.deleted_at' => null])
            ->orderBy('inventorytypes.type', 'asc')
            ->get();
    }
}
