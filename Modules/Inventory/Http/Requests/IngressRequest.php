<?php

namespace Modules\Inventory\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IngressRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'receiver_to' => 'required',
            'deliver_to' => 'required',
            'provider' => 'required',
            'warehouses_provider' => 'required',
            'no_document' => 'required',
            'observations' => 'required',
            'clients' => 'required',
            'warehouses_clients' => 'required',
            'status' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
