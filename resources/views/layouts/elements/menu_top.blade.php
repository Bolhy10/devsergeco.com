<ul class="nav navbar-right top-nav">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown">
            <i class="fa fa-users top-nav-icon"></i>
        </a>
        <ul class="dropdown-menu user-auth-dropdown"
            data-dropdown-in="flipInX"
            data-dropdown-out="flipOutX">

            @foreach(trans('ui.menu_action.data') as $user=>$value)
                <li>
                    <a href="{{ url($value['url']) }}">
                        <i class="{{ $value['icon']  }}"></i>
                        <span>
                            {{ $value['title'] }}
                        </span>
                    </a>
                </li>
                @endforeach
        </ul>
    </li>
</ul>