<?php

namespace Modules\Inventory\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class IngressitemsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'items' => new InventoryitemsResource($this->inventoryitems),
            'quantity' => $this->quantity,
            'observations' => $this->observations
        ];
    }

}
