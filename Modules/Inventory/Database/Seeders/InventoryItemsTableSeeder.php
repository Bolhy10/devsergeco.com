<?php

namespace Modules\Inventory\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class InventoryItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < 30; $i++){
            DB::table('inventoryitems')->insert(array(

                'inventorytypes_id' => $faker->randomElement([1, 2]),
                'code_sku' => $faker->numberBetween($min = 100, $max = 90000),
                'check_unique' => $faker->randomElement([true, false]),
                'description' => $faker->text($maxNbChars = 50),
                'status' => $faker->randomElement([1, 0]),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ));
        }
    }
}
