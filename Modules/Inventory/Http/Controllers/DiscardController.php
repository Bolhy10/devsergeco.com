<?php

namespace Modules\Inventory\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Modules\Auth\Entities\Role;
use Modules\Clients\Entities\Clients;
use Modules\Inventory\Entities\Discard;
use Modules\Inventory\Entities\Discarditems;
use Modules\Inventory\Entities\Inventory;
use Modules\Inventory\Entities\Inventoryitems;
use Modules\Inventory\Http\Requests\DiscardRequest;
use Modules\Inventory\Transformers\DiscarditemsResource;
use Modules\Inventory\Transformers\DiscardResource;
use Modules\Inventory\Transformers\InventoryitemsResource;
use Modules\Inventory\Transformers\InventoryResource;

class DiscardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->can('read-discard')){

            $discard = Discard::orderBy('created_at', 'desc')
                ->with('warehouses', 'clients', 'users', 'discarditems')->paginate(5);

            return DiscardResource::collection($discard);
        }

        return $this->response_error('No autorizado para esta vista.');
    }

    public function viewDiscard(){

        if(Auth::user()->can('read-discard')){

            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor') || Auth::user()->hasRole('inspector'))
            {
                $users = Role::where('name', 'warehouseman')
                    ->orWhere('name', '=', 'warehouse-chief')
                    ->with('users')->get();

            }else if (Auth::user()->hasRole('warehouse-chief')){

                $users = Role::where('name', 'warehouseman')->with('users')->first();

            }else{

                $users = Auth::user();
            }

            $clients = Clients::all()->sortBy("shortname");

            return view('inventory::discard.index', compact('users', 'clients'));

        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        if(Auth::user()->can('create-discard')){

            if(Auth::user()->hasRole('admin')
                || Auth::user()->hasRole('supervisor')
                || Auth::user()->hasRole('inspector'))
            {

                $users = Role::where('name', 'warehouseman')
                    ->orWhere('name', '=', 'warehouse-chief')->with('users')->get();

            }else if (Auth::user()->hasRole('warehouse-chief')){
                $users = Role::where('name', 'warehouseman')
                    ->with('users')
                    ->first();
            }else{

                $users = Auth::user();
            }

            $clients = Clients::all()->sortBy('shortname');

            return view('inventory::discard.create', compact('users', 'clients'));

        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(DiscardRequest $request)
    {
        if (Auth::user()->can('create-discard')){

            $data = Discard::create([
                'receive_to' => $request->receive_to,
                'no_document' => $request->no_document,
                'users_id' => $request->delivered,
                'observations' => $request->observations,
                'clients_id' => $request->clients,
                'warehouses_id' => $request->warehouses_clients,
                'status' => $request->status
            ]);

            $discard = Discard::findOrFail($data->id);

            if (count($discard) > 0){

                $items = $request->items;
                $arrayItems = [];

                for ($d = 0; $d < count($items); $d++){

                    $push_Items = Discarditems::create([
                        'inventoryitems_id' => $items[$d]['items']['id'],
                        'discard_id' => $discard->id,
                        'quantity' => $items[$d]['quantity'],
                        'observations' => $items[$d]['observations']
                    ]);

                    array_push($arrayItems, $push_Items->id);
                }

                if (count($arrayItems) > 0){

                    return json_encode(['status' => 200, 'message' => 'Se proceso correctamente los datos.', 'id' => $data->id]);
                }

                Discard::destroy($discard->id);

                return $this->response_error('Error al almacenar los Items del descarte. Intente nuevamente.');

            }else{

                return $this->response_error('Error al almacenar los datos de descarte. Intente nuevamente.');

            }

        }

        return $this->response_error('No esta autorizado para crear ingresos.');

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(DiscardRequest $request, $id)
    {

        if (Auth::user()->can('update-discard')){

            $discard = Discard::find($id);

            $discard->update([
                'receive_to' => $request->receive_to,
                'no_document' => $request->no_document,
                'observations' => $request->observations,
                'clients_id' => $request->clients,
                'warehouses_id' => $request->warehouses_clients,
                'users_id' => $request->delivered,
                'status' => $request->status
            ]);

            if (count($discard) > 0){

                $items = $request->items;

                for ($d =0; $d < count($items); $d++){

                    $itemsDiscard = Discarditems::find($items[$d]['id']);

                    if (count($itemsDiscard) > 0){

                        $itemsDiscard->update([
                            'inventoryitems_id' => $items[$d]['items']['id'],
                            'quantity' => $items[$d]['quantity'],
                            'observations' => $items[$d]['observations']
                        ]);
                    }

                    if (count($itemsDiscard) < 1){

                        Discarditems::create([
                            'inventoryitems_id' => $items[$d]['items']['id'],
                            'discard_id' => $id,
                            'quantity' => $items[$d]['quantity'],
                            'observations' => $items[$d]['observations']
                        ]);
                    }

                }

                return json_encode(['status' => 200, 'message' => 'Se actualizo el registro correctamente.']);
            }

            return $this->response_error('No se encontro ningún registro con el ID seleccionado.');
        }

        return $this->response_error('No tiene acceso para actualizar este registro.');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        if (Auth::user()->can('delete-discard')){

            $discard = Discard::find($id);

            $delDiscard = Discard::destroy($discard->id);

            if ($delDiscard == 1) {

                return json_encode([
                    'status' => 200,
                    'message' => 'Registro eliminado correctamente.'
                ]);
            }

            return $this->response_error('Error al eliminar el registro seleccionado.');
        }

        return $this->response_error('No esta autorizado para eliminar registros.');

    }

    //METHODS API

    public function items(Request $request){

        if (Auth::user()->can('read-discard') && Auth::user()->can('create-discard')){

            $inventory = Inventory::orderBy('id', 'desc')

                ->where('clients_id', $request->clients)

                ->where('warehouses_id', $request->warehouses)

                ->where('quantity', '>', 0)->where('status', 1)

                ->with('inventoryitems', 'warehouses')->get();

            return InventoryResource::collection($inventory);

        }

        return $this->response_error('No esta autorizado para leer los items.');
    }


    public function generateDiscard($type, $id){

        if (Auth::user()->can('read-discard')){

            $discard = Discard::with('warehouses', 'clients', 'users', 'discarditems')->find($id);

            if (count($discard) > 0){

                $view = 'inventory::discard.print';

                $data = new DiscardResource($discard);

                return $this->createPDF($data, $view, $type);

            }

            return $this->response_error('El ID seleccionado no se encuentra en la base de datos.');

        }

        return $this->response_error('No tiene acceso para generar el documento.');
    }


    public function createPDF($data, $view, $type){

        if (Auth::user()->can('read-discard')){

            $date = Carbon::now();

            $title = 'descarte_#doc'.$data->no_document.'_'.$date;

            $vista = \View::make($view, compact('data', 'date', 'title'))->render();

            $pdf = \App::make('dompdf.wrapper');

            $pdf->loadHTML($vista);

            if ($type == 1){

                if ($data->status == 0){

                    $discard = Discard::find($data->id);

                    if (count($discard) > 0){

                        $items = $data->discarditems;

                        for ($j = 0; $j < count($items); $j++){


                            $inventory = Inventory::where('inventoryitems_id', $items[$j]['inventoryitems_id'])->first();

                            if (count($inventory) > 0){

                                if ($inventory->quantity < 1 &&  $inventory->quantity < $items[$j]['quantity']){

                                    return $this->response_error('No contiene una cantidad valida o la cantidad descartada es mayor a la disponible.');
                                }

                                $quantity = ($inventory->quantity) - ($items[$j]['quantity']);

                                $inventory->quantity = $quantity;

                                $inventory->save();

                                $discard->status = true;
                                $discard->save();

                                if (count($inventory) < 1){
                                    $discard->status = false;
                                    $discard->save();
                                }

                            }

                        }

                        return $pdf->stream('original_'.$title.'.pdf');

                    }

                    return $this->response_error('Error al crear el formulario de ingreso. Intente nuevamente');

                }

                return $pdf->stream($title.'.pdf');
            }

            if ($type == 2){

                return $pdf->download($title.'.pdf');

            }

        }

        return $this->response_error('No tiene acceso para generar el documento.');
    }


//    ERROR THE AUTHENTICATION
    public function response_error($message){
        return json_encode(
            [
                'status' => 400,
                'message' => $message
            ]
        );
    }

    public function notAuthenticate(){
        Session::flash('message_error', trans('auth::ui.user.message_not'));
    }
}
