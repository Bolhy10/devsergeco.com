const ingress = new Vue({
    el: '#ingress',
    data:function (){
        return {

            hello: '',

            clients: '',

            warehouses: [],

            items:[],

            code:[],

            errorRes:[],

            formIncome:{
                receiver_to: '',
                deliver_to: '',
                provider: '',
                warehouses_provider: '',
                no_document: '',
                no_order: '',
                no_transfer: '',
                observations: '',
                clients: null,
                warehouses_clients: '',
                status: false,
                items:[]
            },

            inventory:[],

            ingress: [],

            meta: {
                'current_page': 0,
                'from': 0,
                'last_page': 0,
                'path': '',
                'per_page': 0,
                'to': 0,
                'total' : 0
            },

            offset: 3,

            changeEdit: true,

            fillIngress: ''

        };
    },
    created(){
        this.getIngress();
        this.getItems();
    },

    computed: {
        isActived() {
            return this.meta.current_page;
        },
        pagesNumber(){

            if(!this.meta.to){
                return [];
            }

            var from = this.meta.current_page - 3;
            if(from < 1){
                from = 1;
            }

            var to = from + (3 * 2);
            if(to >= this.meta.last_page){
                to = this.meta.last_page;
            }

            var pagesArray = [];

            while(from <= to){
                pagesArray.push(from);
                from++;
            }

            return pagesArray;

        }
    },

    methods:{
        getIngress(page){
            var url = '/inventory/api/ingress?page='+page;
            axios.get(url).then(res => {
                this.ingress = res.data.data;
                //Pagination
                this.meta = res.data.meta;






            });
        },

        changePage:function (page) {
            this.meta.current_page = page;
            this.getIngress(page);
        },

        deleteIngress:function (income) {
            var url = '/inventory/api/ingress/'+income.id;
            if(confirm('Seguro que desea eliminar el registro seleccionado.')){
                axios.delete(url).then(res => {
                    this.getIngress();
                    $.toast({
                        heading: 'Registro con #Doc:'+income.no_document,
                        text: 'El registro seleccionado ha sido eliminado.',
                        position: 'top-right',
                        icon: 'success',
                    });

                }).catch(err => {
                    $.toast({
                        heading: 'Registro con #Doc:'+income.no_document,
                        text: 'Error al eliminar el registro seleccionado.',
                        position: 'top-right',
                        icon: 'error'
                    });
                });
            };
        },

        getWare:function (id) {
            var url = '/inventory/api/warehouse/'+id;
            if (id > 0){
                axios.get(url).then(res => {

                    this.warehouses = res.data;

                }).catch(err => {
                    alert(err);
                });
            }
        },

        getItems:function () {
            axios.get('/inventory/api/items').then(res=>{
                console.log(res.data);
                this.items = res.data.data;
            }).catch(err=>console.log('Error en la consulta'+ err));
        },

        pushDetails:function (item) {

            if(item.items.id != ""){
                var hash = {};
                this.formIncome.items = this.formIncome.items.filter(function(current) {
                    var exists = !hash[current.items.id] || false;
                    hash[current.items.id] = true;
                    return exists;
                });

                for(var i=0;i<this.items.length;i++){
                    if(item.items.id == this.items[i].id){
                        item.items.description = this.items[i].description;
                        item.items.type.name = this.items[i].types.name;
                    }
                }

                return;
            }

            $.toast("Debe seleccionar un CÓDIGO.");
        },

        addItem: function(){
            // this.inventory.push({ id: '', description: '' , type: '', quantity: '', observations: '' });
            this.formIncome.items.push(
                {
                    id: '',
                    items: {
                        id: '',
                        code: '',
                        description: '',
                        type: {
                            id: '',
                            name: ''
                        },
                    },
                    quantity: '',
                    observations: ''
                }
            );
        },

        removeItem:function (item) {
            var index2 = this.formIncome.items.indexOf(item);
            this.formIncome.items.splice(index2, 1);
        },


        validateBeforeSubmit: function() {

            if(this.formIncome.items.length > 0){

                this.$validator.validateAll().then((result) => {
                    if (result) {

                        if(confirm('¿Se encuentra seguro de guardar el ingreso?')){

                            var url = '/inventory/api/ingress';

                            axios.post(url, this.formIncome).then(res=>{

                                if(res.data.status == 200){

                                    if(confirm('¿Desea imprimir el formulario de ingreso?')){
                                        window.open('/inventory/generate/ingress/1/'+res.data.id);
                                    }

                                    $.toast(res.data.message);
                                    this.cleanForm();
                                }

                            }).catch(error=>{

                                $.toast(error.response.data.message+' Verifique los campos requeridos.');

                            });

                        }

                        return;
                    }

                    $.toast('Faltan campos por llenar. Verifique todos los campos y ITEMS de la tabla.');

                });

                return;
            }

            $.toast('Debe agregar ITEMS a su formulario de ingreso.')

        },

        cleanForm:function () {

            this.formIncome.receiver_to = '';
            this.formIncome.deliver_to  = '';
            this.formIncome.provider    = '';
            this.formIncome.warehouses_provider = '';
            this.formIncome.no_document = '';
            this.formIncome.no_order    = '';
            this.formIncome.no_transfer = '';
            this.formIncome.observations = '';
            this.formIncome.clients = null;
            this.formIncome.warehouses_clients = '';
            this.formIncome.items = [];
        },

        viewPrint:function (income) {

            if(confirm("Seguro que deseas imprimir?. \n" +
                    "Al aceptar se procesa el ingreso al sistema. \n" +
                    "Las siguientes impresiones seran copias. \n" +
                    "Asegúrese de imprimir el archivo original.")){

                this.getIngress(2);
                window.open('/inventory/generate/ingress/1/'+income.id);

            }
        },

        editBtn:function (income) {

            this.fillIngress = income.id;

            this.changeEdit = false;

            this.getWare(income.client.id);

            this.formIncome.receiver_to = income.receive_to.id;
            this.formIncome.deliver_to = income.deliver_to;
            this.formIncome.provider = income.provider;
            this.formIncome.warehouses_provider = income.warehouses_provider;
            this.formIncome.no_document = income.no_document;
            this.formIncome.no_order = income.no_order;
            this.formIncome.no_transfer = income.no_transfer;
            this.formIncome.observations = income.observations;
            this.formIncome.clients = income.client.id;
            this.formIncome.warehouses_clients = income.warehouse.id;
            this.formIncome.items = income.ingressitems;

        },

        updateIngress:function (id) {

            var url = 'api/ingress/'+id;

            if(confirm('Desea actualizar el registro seleccionado?')){
                axios.put(url, this.formIncome).then(res=>{
                    if(res.data.status == 200){

                        this.changeEdit = true;

                        this.getIngress();

                        this.cleanForm();

                        $.toast('Se actualizo el registro correctamente.');

                        return;
                    }
                    $.toast('Error al actualizar el registro. Intente nuevamente.');

                }).catch(err=>{

                    $.toast('Error en el servidor:', err.response.data);

                });
            }
        }

    }

});
