<?php

namespace Modules\Prestamos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use Modules\Clients\Entities\Clients;
use Modules\Dispatch\Entities\dispatch;
use Modules\Prestamos\Entities\Prestamos;
use Modules\Inventory\Entities\Inventory;
use Modules\Inventory\Entities\Inventoryitems;
use Modules\Warehouses\Entities\Warehouses;

class PrestamosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $titulo = 'Préstamos realizados';
        $bodegas = Warehouses::all();
        $method = $request->method();
        if (Auth::user()->can('read-loan'))
        {
            if ($request->isMethod('post')) {
                $bodega = $request->bodega;
                $ruta_dt = route('prestamos.table', ['id' => $bodega] );
                return view('prestamos::index', ['bodegas' => $bodegas, 'ruta_dt' => $ruta_dt, 'titulo' => $titulo] );
            }
            else
            {
                $ruta_dt = route('prestamos.table', ['id' => 0] );
                return view('prestamos::index', ['bodegas' => $bodegas, 'ruta_dt' => $ruta_dt, 'titulo' => $titulo] );
            }
        }
        else
        {
            print "NO tienes acceso a esta area. ";
        }
        //$prestamos = Prestamos::with(['inventory.warehouses','dispatch.status_item','dispatch.ot'])->get();
        //return response()->json($prestamos);
        /*$prestamos = new Prestamos;
        $datos_prestamos = $prestamos->datos_prestamos();
        $data = array();
        $titulo = 'Prestamos entre inventarios';

        foreach ($datos_prestamos as $dp):
            $data_interna = array();
            $prestamo_id = $dp->id;
            $pd_id = $dp->pd_id;    //id del despacho
            $inv_tomadoid = $dp->invtomado_id;
            $inventario_tomado = Clients::find($dp->invtomado_id);
            $invt_name = $inventario_tomado->shortname;

            $inv_recibidoid = $dp->invrecibido_id;
            $inventario_recibido = Clients::find($dp->invrecibido_id);
            $invr_name = $inventario_recibido->shortname;

            $prestamo_status =  $dp->p_status;
            $warehouses_id =  $dp->warehouses_id;
            $status_item = $dp->item_status;
            $warehouse_name = $dp->w_name;
            $code_ot = $dp->code_ot;
            $project_name = $dp->project_name;
            $data_interna = array('prestamo_id' => $prestamo_id, 'despacho_id' => $pd_id, 'inv_tomadoid' => $inv_tomadoid,
            'tomado_nombre' => $invt_name, 'recibido_nombre' => $invr_name,'status_prestamo' => $prestamo_status,'status_material'=>$status_item,
                'bodega_nombre'=>$warehouse_name,'code_ot' => $code_ot, 'proyecto' => $project_name);
            array_push($data,$data_interna);

        endforeach;

        //return response()->json($data);
        re-turn view('prestamos::index',['data' => $data,'titulo'=>$titulo]); */
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('prestamos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        if (Auth::user()->can('read-loan'))
        {
            //Se consultan los datos del prestamo. los datos del item en la consulta principal son de donde se tomaron.
            $prestamos = Prestamos::with('dispatch.ot.clients','dispatch.ot.zones.warehouses','inventory.inventoryitems.inventorytypes',
            'inventory.clients','inventory.warehouses')->find($id);

            //id del material que se presto
            $id_inv = $prestamos->inventory->id;
            //cantidad que se presto
            $cant_prestamo = Prestamos::with(['dispatch.status_item' => function($q) use(&$id_inv){
                $q->where('inventory_id',$id_inv);
            }])->find($id);

            $cant_t = $cant_prestamo->dispatch->status_item[0]->quantity;  //cantidad que se tomo prestada
         
            //codigo sku del material prestado.
            $code_sku = $prestamos->inventory->inventoryitems->code_sku;
            $id_invitems = $prestamos->inventory->inventoryitems->id;
            //id del cliente que recibio el prestamo
            $id_cr =  $prestamos->dispatch->ot->clients->id;

            //id del cliente de donde se tomo el prestamo.
            $id_ct = $prestamos->inventory->clients->id;

            //id de la bodega del cliente que recibio el prestamo
            $w_cr = $prestamos->dispatch->ot->zones->warehouses->id;

            //id de la bodega del cliente donde se tomo el prestamo.
            $w_ct = $prestamos->inventory->warehouses->id;

            //datos para devolver el material prestado
            /*$cant_cr = Inventoryitems::with(['inventory' => function($query) use(&$id_cr, &$w_cr){
                $query->where(['clients_id' => $id_cr, 'warehouses_id' => $w_cr]);
            }])->where('code_sku',$code_sku)->get();*/
            $cant_cr = Inventory::with(['inventoryitems'])->where(['clients_id'=>$id_cr, 'inventoryitems_id' => $id_invitems])->get(); 
            
            $cant_ct = Inventory::with(['inventoryitems'])->where(['clients_id'=>$id_ct, 'inventoryitems_id' => $id_invitems])->get();
            /*$cant_ct = Inventoryitems::with(['inventory' => function($query) use(&$id_ct, &$w_ct){
                $query->where(['clients_id' => $id_ct, 'warehouses_id' => $w_ct]);
            }])->where('code_sku',$code_sku)->get();*/

           return view('prestamos::pages.view',['prestamos'=>$prestamos, "dat_cr" => $cant_cr, "dat_ct" => $cant_ct, "cant_prestamo"=>$cant_t]);

            
            //return response()->json($test2);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('prestamos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * funcion para realizar una devolucion de un material prestado
     */
    public function return($id)
    {
        //$title = "Devolver Material";
        
        //return view('prestamos::pages.return',["title"=>$title,"id"=>$id]);

        $prestamos = Prestamos::with('dispatch.ot.clients','dispatch.ot.zones.warehouses','inventory.inventoryitems.inventorytypes','inventory.clients','inventory.warehouses')->find($id);

        $id_inv = $prestamos->inventory->id;
            //cantidad que se presto
            $cant_prestamo = Prestamos::with(['dispatch.status_item' => function($q) use(&$id_inv){
                $q->where('inventory_id',$id_inv);
            }])->find($id);

            $cant_t = $cant_prestamo->dispatch->status_item[0]->quantity;
            
            //codigo sku del material prestado.
            $code_sku = $prestamos->inventory->inventoryitems->code_sku;
            //id del cliente que recibio el prestamo
            $id_cr =  $prestamos->dispatch->ot->clients->id;

            //id del cliente de donde se tomo el prestamo.
            $id_ct = $prestamos->inventory->clients->id;

            //id de la bodega del cliente que recibio el prestamo
            $w_cr = $prestamos->dispatch->ot->zones->warehouses->id;

            //id de la bodega del cliente donde se tomo el prestamo.
            $w_ct = $prestamos->inventory->warehouses->id;

            //datos para devolver el material prestado
            $cant_cr = Inventoryitems::with(['inventory' => function($query) use(&$id_cr, &$w_cr){
                $query->where(['clients_id' => $id_cr, 'warehouses_id' => $w_cr]);
            }])->where('code_sku',$code_sku)->get();
            
            $cant_ct = Inventoryitems::with(['inventory' => function($query) use(&$id_ct, &$w_ct){
                $query->where(['clients_id' => $id_ct, 'warehouses_id' => $w_ct]);
            }])->where('code_sku',$code_sku)->get();
        
            //si el despacho ha sido finalizado.
        if($prestamos->dispatch->status == 2 && $prestamos->status == 1)
        {
            //si la cantidad del que recibio es mayor o igual al prestamo
            if((int)$cant_cr[0]->inventory->quantity >= (int)$cant_t )
            {
                //se resta del cliente que lo recibio
                $cant_act_cr = (int)$cant_cr[0]->inventory->quantity - (int)$cant_t;
                //se le suma al cliente que lo presto
                $cant_act_ct = (int)$cant_ct[0]->inventory->quantity + (int)$cant_t;

                $inv_cr = Inventory::find((int)$cant_cr[0]->inventory->id);
                $inv_cr->quantity = $cant_act_cr;
                $inv_cr->save();

                $inv_ct = Inventory::find((int)$cant_ct[0]->inventory->id);
                $inv_ct->quantity = $cant_act_ct;
                $inv_ct->save();

                $status_prestamo = Prestamos::find($id);
                $status_prestamo->status = 2;
                $status_prestamo->save();
                
                //print "cr = ".$cant_act_cr." ct = ".$cant_act_ct;
            }
            else
            {
                print "Aún no cuenta con la cantidad sufieciente";
            }
        }
        else
        {
            print "Disculpe el despacho no ha sido finalizado";
        }
        
        return redirect()->route('prestamos.view', $prestamos->id);
        //return response()->json($status_prestamo);

    }

    public function data_table($id)
    {
        $datos = array();
        $materiales = '';

        if($id == 0)
        {
   
            $prestamos = Prestamos::with(['dispatch.ot:id,code_ot','inventory.inventoryitems:id,description',
            'inventory.clients:id,shortname'])
            ->get();   
            
        }else
        {
            $prestamos = Prestamos::whereHas('dispatch.ot',function($query) use($id){
                $query->where('ot.zone','=',$id);
            })
            ->get(); 
            $prestamos->load('inventory.inventoryitems','inventory.clients'); 
        }
        
        if(!empty($prestamos)){
            foreach ($prestamos as $p):
                $name_i = Inventory::with('clients:id,shortname')->find($p->invrecibido_id);
                array_push($datos,[ "id" => $p->id, "code_ot" => $p->dispatch->ot->code_ot, "inv_o" => $p->inventory->clients->shortname,
                "inv_r"=>$name_i->clients->shortname,"description" => $p->inventory->inventoryitems->description, "status" => $p->status, "created_at" => $p->created_at->format('d M Y - H:i:s') ]);
            endforeach;
        }
        
        //return response()->json($datos);
        return datatables()->of($datos)->setRowId('id')->toJson();
    }
    
}
