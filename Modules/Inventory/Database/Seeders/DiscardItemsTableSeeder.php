<?php

namespace Modules\Inventory\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class DiscardItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        for ($i = 0; $i < 10; $i++){

            DB::table('discarditems')->insert(array(
                'inventoryitems_id' => $faker->numberBetween($min = 1, $max = 10),
                'discard_id' => $faker->numberBetween($min = 1, $max = 9),
                'quantity' => $faker->randomNumber(2),
                'observations' => $faker->text(50),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ));
        }

    }
}
