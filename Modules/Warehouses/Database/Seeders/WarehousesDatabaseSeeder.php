<?php

namespace Modules\Warehouses\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class WarehousesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ProvicesTableSeeder::class);
        $this->call(ZonesTableSeeder::class);
        $this->call(WarehousesTableSeeder::class);
    }
}
