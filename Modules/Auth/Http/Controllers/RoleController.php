<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Auth\Entities\Permission;
use Modules\Auth\Entities\Role;
use Modules\Auth\Http\Requests\RoleRequest;
use Nwidart\Modules\Routing\Controller;

class RoleController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
    	if (Auth::user()->can('read-roles')){

    		$roles = Role::with('permissions')->paginate(5);

    		return view('auth::role.index', compact('roles'));
		}

		return redirect('auth/logout');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->can('create-roles')){

        	$permissions = Permission::pluck('display_name', 'id');

        	return view('auth::role.create', compact('permissions'));

        }

		return redirect('auth/logout');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RoleRequest $request)
    {
    	if (Auth::user()->can('create-roles')){
    		$data = Role::create($request->all());

    		$role = Role::findOrFail($data->id);

    		$data->attachPermissions($request->input('permission_id'));

    		Session::flash('message', trans('auth::ui.role.manage_create', array('name'=> $role->name)));

    		return redirect('auth/role/create');
		}

		return redirect('auth/logout');

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
    	if (Auth::user()->can('update-roles')){

    		$role = Role::findOrFail($id);

    		$role_permission = Role::find($id)->permissions()->pluck('permission_id')->toArray();

    		$permissions = Permission::orderBy('display_name', 'asc')->pluck('display_name', 'id');

    		return view('auth::role.edit', compact('role', 'permissions', 'role_permission'));
		}

		return redirect('auth/logout');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, RoleRequest $request)
    {
		if(Auth::user()->can('update-roles')) {

			$role = Role::findOrFail($id);

			$role->update($request->all());

			if($role->permissions->count()) {

				$role->permissions()->detach($role->permissions()->pluck('permission_id')->toArray());
			}

			$role->attachPermissions($request->input('permission_id'));

			Session::flash('message', trans('auth::ui.role.message_update', array('name' => $role->name)));

			return redirect('auth/role');

		}

		return redirect('auth/logout');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
