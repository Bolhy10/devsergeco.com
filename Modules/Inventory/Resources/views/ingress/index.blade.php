@extends('layouts.master')

@section('pageTitle', trans('inventory::ui.ingress.title'))

@section('content')

    <div id="ingress">

        @include('inventory::ingress.lists')

        @include('inventory::ingress.edit')

    </div>

    @stop


