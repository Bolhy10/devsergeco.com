<?php

namespace Modules\Dispatch\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Modules\Dispatch\Entities\Dispatch;
use Modules\Dispatch\Entities\StatusItem;
use Modules\Inventory\Entities\Inventory;
use Modules\Inventory\Entities\Inventoryitems;
use Modules\Ot\Entities\Stot;
use Modules\Ot\Entities\Ot;
use Modules\Warehouses\Entities\Warehouses;
use Modules\Warehouses\Entities\Zones;
use Modules\Prestamos\Entities\Prestamos;

use Yajra\Datatables\Datatables;

class DispatchController extends Controller
{
    /**
     * Estados de los despachos =
     * 1 => EN DESPACHO
     * 2 => FINALIZADO
     * 3 => ELIMINADO
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $bodegas = Warehouses::all();
        $method = $request->method();
        if (Auth::user()->can('read-dispatch'))
        {
            if ($request->isMethod('post')) {
                $bodega = $request->bodega;
                $ruta_dt = route('dispatch.table',['id' => $bodega]);
                return view('dispatch::index',['bodegas' => $bodegas, 'ruta_dt' => $ruta_dt]);
            }
            else
            {
                $ruta_dt = route('dispatch.table',['id' => 0]);
                return view('dispatch::index',['bodegas' => $bodegas, 'ruta_dt' => $ruta_dt]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        //extraer datos de las ot's con estado 1 o iniciado
        $zones = Zones::all();
        return view('dispatch::pages.create',['zones' => $zones]);
        //return response()->json($ots);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        //validar si el usuario puede leer despachos
        if (Auth::user()->can('read-dispatch'))
        {
            //validar el rol del usuario para devolver la vista indicada
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor') || Auth::user()->hasRole('inspector'))
            {
                $despacho = new Dispatch;
                $desp = Dispatch::find($id);
                //$ot_status =
                $ot_id = $desp->ot_id;
                //$data = $despacho->get_dispatch($id);
                $data = Dispatch::with('ot.clients','status_item.inventory.inventoryitems.inventorytypes','status_item.inventory.clients','status_item.inventory.uinventary')->find($id);

                $ot = Ot::find($ot_id);

                //return response()->json($des);
                return view('dispatch::pages.viewdispatch',['despacho' => $desp, 'data' => $data, 'ot' => $ot]);
                //return response()->json($data);
            }
            elseif(Auth::user()->hasRole('warehouse-chief') || Auth::user()->hasRole('warehouseman') )
            {
                $despacho = Dispatch::with(['ot.zones','status_item.inventory.inventoryitems.inventorytypes','status_item.inventory.clients','status_item.inventory.uinventary.uistatus','status_item.inventory.uinventary.uistatus' => function($q) use(&$id){
                    $q->where('dispatch_id',$id);
                }])->find($id);
                return view('dispatch::almacenista.dispatch_view',['despacho' => $despacho]);
                //return response()->json($despacho);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /*public function delete_item($d_id,$id)
    {
        $despacho_id = $d_id;
        $item = $id;
    }*/

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor'))
        {
            $despacho = Dispatch::find($id);
            $despacho->status = 3;
            $despacho->save();

            //$des_del = Dispatch::find($id);
            //$des_del->delete();

            $message=array("st"=>1,"ms"=>"El despacho ha  sido eliminado");
            return response()->json($message);
        }
        else
        {
            return redirect('ot');
        }
    }

    //FINALIZAR UN DESPACHO
    public function end_dispatch(Request $request)
    {
        $error_ms = array();
        $cont_error = 0;

        $id = $request->id;
        $despacho = Dispatch::find($id);

        if($despacho->status == 1){

            foreach ($despacho->status_item as $clave => $sti):
                $inv_id = $sti->inventory_id;
                /** @var  $qty_st almacena la cantidad solicitada  */
                $qty_st = $sti->quantity;
                $inventory = Inventory::find($inv_id);
                /** @var  $qty_item almacena cantidad del inventario */
                $qty_inventory = $inventory->quantity;

                //si la cantidad solicitada es mayor que la restante en el inventario
                if ($qty_st > $qty_inventory)
                {
                    //id del item_status
                    $id_qtyst = $sti->id;
                    $cant_demas = $qty_inventory;

                    $error_ms[$clave] = array("id"=>$id_qtyst,"cantidad"=>$cant_demas);
                    $cont_error ++;
                }
            endforeach;

            if($cont_error > 0){
                $message = array("st"=>0,"ms"=>$error_ms);
                return response()->json($message);
            }
            else
            {
                //se recorre y se actualiza en la base de datos el item del inventario
                foreach ($despacho->status_item as $clave => $st):
                    $inventario_id = $st->inventory_id;
                    $cantidad_solicitada = $st->quantity;

                    /*SE ACTUALIZA EL STATUS DEL ITEM A FINALIZADO '2'*/
                    $status_items = StatusItem::find($st->id);
                    $status_items->status = 2;
                    $status_items->save();

                    $inventario = Inventory::find($inventario_id);
                    $cantidad_actual = $inventario->quantity;
                    $cant_final = $cantidad_actual - $cantidad_solicitada;
                    $inventario->quantity = $cant_final;
                    $inventario->save();
                endforeach;

                $despacho->status = 2;  //se actualiza el estado a despacho finalizado 2
                $despacho->save();

                $message=array("st"=>1,"ms"=>"Los items se han acualizado en el inventario");
                return response()->json($message);
            }

        }

    }

    public function data_table($id)
    {
        $datos = array();
        $materiales = '';

        if($id == 0)
        {
            $despachos = Dispatch::with(['status_item.inventory.inventoryitems','ot.zones'])
            ->get();
        }else
        {
            $despachos = Dispatch::with(['status_item.inventory.inventoryitems','ot.zones'=>function($query) use($id){
                $query->where('zones.id','=',$id);
            }])
            ->get();
        }
        foreach ($despachos as $d):
            $materiales = '';
            if( !is_null($d->ot->zones))
            {
                foreach($d->status_item as $st):
                    $materiales.= $st->inventory->inventoryitems->description." ";
                endforeach;
                array_push($datos,[ "id" => $d->id, "code_ot" => $d->ot->code_ot, "zone" => $d->ot->zones->name, "project_name" => $d->ot->project_name,
                "materiales" => $materiales, "status" => $d->status, "created_at" => $d->created_at->format('d M Y - H:i:s') ]);
            }
        endforeach;
        return datatables()->of($datos)->setRowId('id')->toJson();
    }

// ----------------------------------Funciones usadas en en vue de despacho------------------------------------------
    //funcion para consultar las ots dependiendo de la zona
    public function getOtByZone( $id )
    {
        $zone_id = $id;
        $ots = Stot::with(['ot' => function($q) use($zone_id){
            $q->where('zone', $zone_id);
        }])->where('status',1)->get();
        return response()->json($ots);
    }

    // funcion para consultar los items del inventario relacionados a una OT
    public function getInventoryOts( $ot_id )
    {
        $inventary = Ot::with('clients.inventory.inventoryitems.inventorytypes')->find($ot_id);
        return response()->json($inventary);
    }

    // funcion para consultar en otros inventarios el mismo item con mas cantidad
    public function searchSku($sku, $cantps, $clientid)
    {
        $data = array();
        //$materiales = Inventoryitems::with('inventory')->where('code_sku',$sku)->get();
        $materiales = Inventory::with(['clients:id,shortname','inventoryitems' => function($q) use($sku){
            $q->where(['code_sku'=>$sku, 'status' => 1]);
        }])->where('status',1)->get();

        foreach($materiales as $m):
            if(!empty($m->inventoryitems)){
              if($m->quantity >= $cantps && $m->clients->id != $clientid){
                array_push($data,$m);
              }
            }
        endforeach;
        return response()->json($data);
    }

    public function getDataItem($id_item)
    {
        $data = Inventory::with(['clients', 'inventoryitems.inventorytypes'])->find($id_item);
        return response()->json($data);
    }

    public function saveNewDispatch (Request $request) {
        $data = $request->data;  //materiales solicitados en el despacho
        $entregar_a = $request->entregar_a;   
        $observacion = $request->observacion;
        $ot_id = $request->ot_id;

        $ot_data = Ot::find($ot_id);
        $id_cliente = $ot_data->clients_id;

        $despacho = new Dispatch;
        $despacho->ot_id = $ot_id;
        $despacho->observacion = $observacion;
        $despacho->entregar = $entregar_a;
        $despacho->status = 1;    // en despacho

        if($despacho->save())
        {
            //$n = indice, $i = array de la data
            foreach($data as $n => $i):
                if($i['inv_id']['value'] != null){
                    $itemOt = new StatusItem();
                    $itemOt->inventory_id = $i['inv_id']['value'];
                    $itemOt->quantity = $i['cant_s'];
                    $itemOt->dispatch_id = $despacho->id;
                    $itemOt->status = 1;
                    $itemOt->save();

                    $itemP = Inventory::find($i['inv_id']['value']);
                    $itemP_idclient = $itemP->clients_id;

                    //se valida si se tomo un material prestado de otro inventario
                    if($itemP_idclient != $id_cliente)
                    {
                        $prestamos = new Prestamos();
                        $prestamos->dispatch_id = $despacho->id;
                        $prestamos->invtomado_id = $itemP_idclient;
                        $prestamos->invrecibido_id = $id_cliente;
                        $prestamos->inventory_id = $i['inv_id']['value'];
                        $prestamos->status = 1;
                        $prestamos->save();
                    }
                }
                //print json_decode($i);
            endforeach;
            $url = url('dispatch');
            return $url;
        }else{
            return false; 
        }

        

        //return response()->json($id_cliente);
    }

}
