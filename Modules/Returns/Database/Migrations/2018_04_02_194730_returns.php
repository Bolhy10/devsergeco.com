<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Returns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('returns',function( Blueprint $table){
            $table->increments('id');
            $table->integer('ot_id');
            $table->integer('status');
            $table->string('observacion');
            $table->string('entregar');
            $table->integer('prints')->default(0);
            $table->timestamps();
        });

        Schema::create('returns_items',function( Blueprint $table){
            $table->increments('id');
            $table->integer('quantity');
            $table->integer('inventory_id')->unsigned();
            $table->integer('returns_id')->unsigned();
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('inventory_id')->references('id')->on('inventory');
            $table->foreign('returns_id')->references('id')->on('returns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('returns');
        Schema::drop('returns_items');
    }
}
