<?php

namespace Modules\Returns\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Dispatch\Entities\StatusItem;
use Modules\Dispatch\Entities\Dispatch;

use Modules\Returns\Entities\Returns;
use Modules\Returns\Entities\ReturnsItems;

use Modules\Ot\Entities\Ot;

use Modules\Inventory\Entities\Inventory;

use Yajra\Datatables\Datatables;

class ReturnsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $ruta_dt = "www.example.com";
        return view('returns::index',["ruta_dt" => $ruta_dt]);
    }

    /**
     * Show the form for creating a new resource.
     * $id id de la ot relacionada
     * @return Response
     */
    public function create($id)
    {
        $count_despachos = Dispatch::where(['ot_id'=>$id,'status'=>2])->count();
        if($count_despachos > 0)
        {
            $titulo = 'Devolución de Materiales';
            $items = new StatusItem;
            $materiales_despacho = $items->returns_items($id);
            return view('returns::pages.create',[ 'materiales' => $materiales_despacho, 'ot_id' => $id,'titulo' => $titulo ]);
        }
        else
            {
                $message = 'La Ot no cuenta con despachos finalizados, para crear devoluciones debe finalizar un despacho';
                return view('returns::pages.error',[ 'mensaje' => $message, 'ot_id' => $id ]);
            }
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $observacion = $request->observacion;
        $entregar = $request->entregar_a;
        $ot_id = $request->ot_id;
        $devolucion = new Returns;
        $devolucion->ot_id = $ot_id;
        $devolucion->observacion = $observacion;
        $devolucion->entregar = $entregar;
        $devolucion->status = 1;

        if( $devolucion->save() )
        {
            $ids = $request->id;
            $cant = $request->cantidad;
            //se recorren los items del formulario
            foreach ( $ids as $n => $i ):
                $item_return = new ReturnsItems();
                if( !empty( $cant[$n] ) )
                {
                    $item_return->inventory_id = $i;
                    $item_return->quantity = $cant[$n];
                    $item_return->returns_id = $devolucion->id;
                    $item_return->status = 1;
                    $item_return->save();
                }
            endforeach;

                //return route('ot.view_ot',['id'=>$ot_id]);
                return redirect()->route('ot.view_ot',$ot_id);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $devolucion = new Returns();
        $dev = Returns::find($id);
        //$ot_status =
        $ot_id = $dev->ot_id;
        //obtener datos de la devolucion
        $data = $devolucion->get_return($id);

        $ot = Ot::find($ot_id);

        //return response()->json($des);
        return view('returns::pages.viewreturn',['devolucion' => $dev, 'data_devolucion' => $data, 'ot' => $ot]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('returns::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor'))
        {
            $devolucion = Returns::find($id);
            $devolucion->status = 3;
            $devolucion->save();

            //$des_del = Dispatch::find($id);
            //$des_del->delete();

            $message=array("st"=>1,"ms"=>"La devolucion ha  sido eliminada");
            return response()->json($message);
        }
        else
        {
            return redirect('ot');
        }
    }

    public function get_data_items( Request $request)
    {
        $id_st = $request->iditem;
        $items_despacho = new StatusItem;
        $data_item = $items_despacho->st_item($id_st);

        return response()->json($data_item);
    }

    public function end_returns( Request $request)
    {
        $error_ms = array();
        $cont_error = 0;

        $id = $request->id;
        $devolucion = Returns::find($id);

        if($devolucion->status == 1){

            foreach ($devolucion->returns_item as $clave => $st):
                $inventario_id = $st->inventory_id;
                $cantidad_solicitada = $st->quantity;

                $inventario = Inventory::find($inventario_id);
                $cantidad_actual = $inventario->quantity;
                $cant_final = $cantidad_actual + $cantidad_solicitada;
                $inventario->quantity = $cant_final;
                $inventario->save();
            endforeach;

            $devolucion->status = 2;  //se actualiza el estado a despacho finalizado 2
            $devolucion->save();

            $message=array("st"=>1,"ms"=>"Los items se han acualizado en el inventario");
            return response()->json($message);
        }
    }

    public function data_table()
    {
        $datos = array();
        $materiales = '';

        $devoluciones = Returns::with(['returns_item.inventory.inventoryitems','ot'])
        ->orderBy('created_at','desc')
        ->get();

        foreach ($devoluciones as $d):
            $materiales = '';
            foreach($d->returns_item as $st):
                $materiales.= $st->inventory->inventoryitems->description." ";
            endforeach;
            array_push($datos,["id"=>$d->id,"code_ot"=>$d->ot->code_ot,"project_name" => $d->ot->project_name,
            "materiales"=>$materiales,"status"=>$d->status,"created_at"=>$d->created_at->format('d M Y - H:i:s')]);
        endforeach;
        return datatables()->of($datos)->toJson();
    }
}
