<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="panel panel-default card-view panel-refresh">
        <div class="refresh-container">
            <div class="la-anim-1"></div>
        </div>
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">{{ trans('inventory::ui.inventory.filter') }}</h6>
            </div>
            <div class="pull-right">
                <a class="pull-left inline-block mr-15" data-toggle="collapse"
                   href="#collapse_1" aria-expanded="true">
                    <i class="fa fa-arrows-v"></i>
                </a>
                <a class="pull-left inline-block close-panel" href="#" data-effect="fadeOut">
                    <i class="fa fa-close"></i>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="collapse_1" class="panel-wrapper collapse" aria-expanded="true" style="">
            <div class="panel-body">

                <div class="col-lg-6 col-xs-12">
                    <div class="form-group">
                        <select name="clients" v-model="fill_clients" class="form-control" @change="getWare(fill_clients)">
                            <option value="0">Escoja un Cliente</option>
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}">{{ $client->shortname }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="form-group">
                        <select name="warehouses" @change="fetchData()"
                                v-model="fill_warehouses"
                                class="form-control">
                            <option value="0">Escoja una Almacén</option>
                            <option v-for="ware in warehouses" :key="ware.id" :value="ware.id">@{{ ware.name }}</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>