@extends('layouts.master')

@section('pageTitle', trans('ui.menu_action.data.roles.title'))

@section('style')
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			@include('partials.message')

			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">{{ trans('auth::ui.role.names') }}</h6>
					</div>

					@if(Auth::user()->can('create-roles'))
						<a href="{{ url('auth/role/create') }}" class="btn btn-success pull-right">
							{{ trans("auth::ui.role.button_add") }}
						</a>
					@endif

					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-wrap">
							<div class="table-responsive">
								<table id="simpletable" class="table table-striped table-bordered" >
									<thead>
									<tr>
										<th>{{ trans('auth::ui.role.name_label') }}</th>
										<th>{{ trans('auth::ui.role.display') }}</th>
										<th>{{ trans('auth::ui.role.description') }}</th>
										<th>{{ trans('auth::ui.permission.names') }}</th>
										@if(Auth::user()->can(['update-roles', 'delete-roles']))
											<th>{{ trans('auth::ui.role.operation_label') }}</th>
										@endif
									</tr>
									</thead>
									<tfoot>
									<tr>
										<th>{{ trans('auth::ui.role.name_label') }}</th>
										<th>{{ trans('auth::ui.role.display') }}</th>
										<th>{{ trans('auth::ui.role.description') }}</th>
										<th>{{ trans('auth::ui.permission.names') }}</th>
										@if(Auth::user()->can(['update-roles', 'delete-roles']))
											<th>{{ trans('auth::ui.role.operation_label') }}</th>
										@endif
									</tr>
									</tfoot>
									<tbody>
									@foreach($roles as $role)
										<tr>
											<td>{{ $role->name }}</td>
											<td>{{ $role->display_name }}</td>
											<td>{{ $role->description }}</td>
											<td>
												@foreach($role->permissions as $permission)
													<span class="badge badge-primary mb-5">
													{{ $permission->display_name }}
													</span>
												@endforeach
											</td>
											@if(Auth::user()->can(['update-roles', 'delete-roles']))
												<td>
													@if(Auth::user()->can('update-roles'))
														<a href="{{ url('auth/role/' . $role->id . '/edit') }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
															<i class="fa fa-pencil"></i>
														</a>
													@endif

													@if(Auth::user()->can('delete-roles'))
														<a href="#" data-url="{{ url('auth/user/'.$role->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle"
														   v-on:click.prevent="deleteUser({{ $role }})">
															<i class="fa fa-trash"></i>
														</a>
													@endif
												</td>
											@endif
										</tr>
									@endforeach

									</tbody>
								</table>

								{!! $roles->render() !!}

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@section('script')
	{!! Html::script('assets/js/user-init.js') !!}
@stop
