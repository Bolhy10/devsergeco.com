<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>
    {!! Html::style('assets/css/bootstrap.min.css') !!}
</head>
<body>


<style>

    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }


    header {
        padding: 10px 0;
        margin-bottom: 30px;
    }

    #logo {
        text-align: center;
        margin-bottom: 10px;
    }

    #logo img {
        width: 250px;
    }

    h2 {
        color: #000;
        font-size: 2.0em;
        line-height: 1.4em;
        font-weight: normal;
        text-align: center;
        margin: 0 0 20px 0;
    }

    #project {
        float: left;
    }

    #project span {
        color: #5D6975;
        text-align: left;
        width: 52px;
        margin-right: 5px;

        font-size: 0.8em;
    }

    #company {
        float: right;
        text-align: right;
    }

    #project div,
    #company div {
        white-space: nowrap;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
    }

    table tr:nth-child(2n-1) td {
        background: #F5F5F5;
    }

    table th,
    table td {
        text-align: center;
    }

    table th {
        color: #5D6975;
        border-bottom: 1px solid #C1CED9;
        white-space: nowrap;
        font-weight: normal;
    }

    table .service,
    table .desc {
        text-align: left;
    }

    table td {
        text-align: right;
    }

    table td.service,
    table td.desc {
        vertical-align: top;
    }

    table td.unit,
    table td.qty,
    table td.total {
        font-size: 1.2em;
    }

    table td.grand {
        border-top: 1px solid #5D6975;;
    }

    #notices .notice {
        color: #5D6975;
        font-size: 1.2em;
    }


</style>

<header class="clearfix">

    <div id="logo">
        <img src="{{ asset('assets/images/elements/logo.png') }}">
    </div>

    <h2>
        {{ $title }}
    </h2>

    <div class="row">
        <div class="col-md-6">
            <div id="project">
                <div><span>GENERADO POR:</span> {{ $users->firstname.' '.$users->lastname }}</div>
            </div>
        </div>

        <div class="col-md-6">
            <div id="project" class="pull-right">
                <div><span>FECHA:</span> {{ $date }}</div>
            </div>
        </div>

    </div>

</header>

<main>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th class="service">CODIGO</th>
            <th class="desc">DESCRIPCION</th>
            <th>TIPO</th>
            <th>CLIENTE</th>
            <th>ALMACÉN</th>
            <th>DISPONIBLE</th>
            <th>ACTUALIZADO</th>
        </tr>
        </thead>
        <tbody>

        @foreach($data as $key=>$item)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td class="service">{{ $item->inventoryitems->code_sku }}</td>
                <td class="desc">{{ $item->inventoryitems->description }}</td>
                <td class="unit">{{ $item->inventoryitems->inventorytypes->type }}</td>

                <td class="desc">{{ $item->clients->fullname }}</td>
                <td class="desc">{{ $item->warehouses->name }}</td>

                <td class="qty">{{ $item->quantity }}</td>
                <td class="desc">{{ $item->updated_at }}</td>
            </tr>
        @endforeach


        </tbody>
    </table>

</main>
<footer style="color: #5D6975;width: 100%;height: 30px;position: absolute;bottom: 0;border-top: 1px solid #C1CED9;padding: 8px 0;text-align: center;">
    El listado se creó en una computadora y es válida.
</footer>
</body>
</html>