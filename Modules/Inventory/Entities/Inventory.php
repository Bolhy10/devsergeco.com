<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{


    protected $table = 'inventory';

    protected $fillable = ['inventoryitems_id', 'clients_id', 'warehouses_id', 'quantity', 'status'];


    public function inventoryitems(){

        return $this->belongsTo('Modules\Inventory\Entities\Inventoryitems');

    }


    public function inventorytypes()
    {

        return $this->hasOne('Modules\Inventory\Entities\Inventorytypes');
    }
    public function clients(){

        return $this->belongsTo('Modules\Clients\Entities\Clients');

    }

    public function warehouses(){

        return $this->belongsTo('Modules\Warehouses\Entities\Warehouses');

    }

    public function uinventary()
    {
        return $this->hasMany('Modules\InventarioUnico\Entities\Uinventary');
    }
    
}
