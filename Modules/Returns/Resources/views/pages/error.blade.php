<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 03/04/2018
 * Time: 17:15
 */
?>

@extends('layouts.master2')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="well text-center">
            <div class="well">
                <div class="row">
                    <h6>No se ha podido realizar la solicitud debido al siguiente error: </h6>
                </div>
            </div>
            <div class="well well-lg">
                <div class="alert alert-danger">
                    <p class="pull-left"> <i class="fa fa-ban pull-left"></i> {{ $mensaje }}</p>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="well txt-danger">
                <div class="row">
                    <a class="btn btn-primary" href="{{ route('ot.view_ot',['id' => $ot_id]) }}"> REGRESAR </a>
                </div>
            </div>
        </div>
    </div>
@endsection
