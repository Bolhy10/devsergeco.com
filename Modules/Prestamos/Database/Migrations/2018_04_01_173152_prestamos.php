<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Prestamos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('prestamos',function( Blueprint $table){
            $table->increments('id');
            $table->integer('dispatch_id')->unsigned();
            $table->integer('invtomado_id'); //id del cliente (inventario) de donde se tomo prestado el material
            $table->integer('invrecibido_id'); //id del cliente(inventario) quien recibio el material
            $table->integer('inventory_id')->unsigned();
            $table->integer('status');   //estados 1->prestado 2->devuelto 3->cancelado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('prestamos');
    }
}
