<?php

namespace Modules\InventarioUnico\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\InventarioUnico\Entities\Uinventary;
use Modules\InventarioUnico\Entities\Uistatus;
use Modules\Inventory\Entities\Inventory;
use Modules\Dispatch\Entities\Dispatch;

class UistatusController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('inventariounico::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create( $d_id, $id)
    {
        $data = Inventory::with('inventoryitems.inventorytypes','uinventary')->find($id);
        $despacho = Dispatch::with(['ot.clients','status_item' => function($q) use(&$id){
            $q->where('inventory_id','=',$id);
        }])->find($d_id);
        
        return view('inventariounico::pages.uist_create',["data" => $data,"despacho" => $despacho]);
        //return response()->json($despacho);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $id = $request->ui_item;
        $cantidad = $request->cantidad;
        $id_despacho = $request->id_despacho;

        $ui_new = new Uistatus();
        $ui_new->uinventary_id = $id;
        $ui_new->dispatch_id = $id_despacho;
        $ui_new->quantity = $cantidad;
        $ui_new->status = 1;
        $ui_new->save();

        return response()->json(Uistatus::find($ui_new->id));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('inventariounico::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
