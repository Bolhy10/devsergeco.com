<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>
    {!! Html::style('assets/css/bootstrap.min.css') !!}
</head>
<body>


<style>

    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }


    header {
        padding: 10px 0;
        margin-bottom: 30px;
    }

    #logo {
        text-align: center;
        margin-bottom: 10px;
    }

    #logo img {
        width: 250px;
    }

    h2 {
        color: #000;
        font-size: 2.0em;
        line-height: 1.4em;
        font-weight: normal;
        text-align: center;
        margin: 0 0 20px 0;
    }

    #project {
        float: left;
    }

    #project span {
        color: #5D6975;
        text-align: left;
        width: 52px;
        margin-right: 5px;

        font-size: 0.8em;
    }

    #company {
        float: right;
        text-align: right;
    }

    #project div,
    #company div {
        white-space: nowrap;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
    }

    table tr:nth-child(2n-1) td {
        background: #F5F5F5;
    }

    table th,
    table td {
        text-align: center;
    }

    table th {
        color: #5D6975;
        border-bottom: 1px solid #C1CED9;
        white-space: nowrap;
        font-weight: normal;
    }

    table .service,
    table .desc {
        text-align: left;
    }

    table td {
        text-align: right;
    }

    table td.service,
    table td.desc {
        vertical-align: top;
    }

    table td.unit,
    table td.qty,
    table td.total {
        font-size: 1.2em;
    }

    table td.grand {
        border-top: 1px solid #5D6975;;
    }

    #notices .notice {
        color: #5D6975;
        font-size: 1.2em;
    }

    footer {
        color: black;
        position: absolute;
        bottom: 0;
        padding: 8px 0;
        text-align: left;
    }


</style>

<header class="clearfix">

    <div id="logo">
        <img src="{{ asset('assets/images/elements/logo.png') }}">
    </div>

    <h2>
        @if($data->status == 1)
        <span>COPY -</span>
        @endif
        INGRESO IN Nº - {{ str_pad($data->id, 5,"0",STR_PAD_LEFT) }}
    </h2>

    <div class="row">
        <div class="col-md-6">
            <div id="project">
                <div><span>PROVEEDOR</span> {{ $data->provider }}</div>
                <div><span>ALMACEN</span> {{ $data->warehouses_provider }}</div>
                <div><span>#DOC</span> {{ $data->no_document }}</div>

                @if($data->no_order != "")
                <div><span>#PEDIDO</span> {{ $data->no_order }} </div>
                @endif
                @if($data->no_transfer != "")
                <div><span>#TRANSFERENCIA</span> {{ $data->no_transfer }}</div>
                @endif

                <div><span>ENTREGADO</span> {{ $data->deliver_to }}</div>


            </div>
        </div>

        <div class="col-md-6">
            <div id="project" class="pull-right">
                <div><span>FECHA</span> {{ $data->created_at }}</div>
                <div><span>INVENTARIO</span> {{ $data->clients->shortname }}</div>
                <div><span>ALMACEN</span> {{ $data->warehouses->name }}</div>


                @if($data->status == 1)
                <div><span>ESTADO</span> <strong class="text-danger">IMPRESA</strong></div>
                @endif
                <div><span>RECIBIDO</span> {{ $data->users->firstname.' '.$data->users->lastname }}</div>

            </div>
        </div>

    </div>

</header>

<main>
    <table class="table">
        <thead>
        <tr>
            <th>IT</th>
            <th class="service">CODIGO</th>
            <th class="desc">DESCRIPCION</th>
            <th>TIPO</th>
            <th>CANTIDAD</th>
            <th>OBSERVACIONES</th>
        </tr>
        </thead>
        <tbody>

        @foreach($data->ingressitems as $key=>$item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td class="service">{{ $item->inventoryitems->code_sku }}</td>
            <td class="desc">{{ $item->inventoryitems->description }}</td>
            <td class="unit">{{ $item->inventoryitems->inventorytypes->type }}</td>
            <td class="qty">{{ $item->quantity }}</td>
            <td class="total">{{ $item->observations }}</td>
        </tr>
            @endforeach


        </tbody>
    </table>
    <div id="notices">
        <div>OBSERVACIONES:</div>
        <div class="notice">{{ $data->observations }}</div>
    </div>
</main>


<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                <p style="font-size:16px">Recibido por: ..............................</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                <p style="font-size:16px">Firma: ..............................</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                <p style="font-size:16px">Fecha: ..........................</p>
            </div>
        </div>
    </div>
</footer>

</body>
</html>