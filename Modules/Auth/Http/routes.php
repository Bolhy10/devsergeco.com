<?php

Route::group(['middleware' => ['web'], 'prefix' => '/', 'namespace' => 'Modules\Auth\Http\Controllers'], function()
{
    Route::get('/', 'AuthController@index');

    Route::post('/auth/login', ['as' => 'auth.login', 'uses' => 'AuthController@postLogin']);

    Route::get('/auth/logout', ['as' => 'auth.logout', 'uses' => 'AuthController@getLogout']);

    //Resource de Users, Permissions & Role

    Route::resource('auth/user', 'UserController');

	Route::resource('auth/permission', 'PermissionController');

	Route::resource('auth/role', 'RoleController');

	Route::get('/auth/profile', ['as' => 'auth.profile', 'uses' => 'ProfileController@edit']);

	Route::put('/auth/profile/update', ['as'=> 'profile.update', 'uses'=>'ProfileController@update']);

});