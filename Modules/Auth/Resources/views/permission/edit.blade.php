@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-8 col-lg-offset-2">

			@include('partials.message')

			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">{{ trans('auth::ui.permission.edit_permission') }}</h6>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						@include('errors.form_error')
						<div class="form-wrap">

							{!! Form::model($permission, ['method' => 'PUT', 'route' => ['permission.update', $permission->id]]) !!}

							@include('auth::permission.form', ['button' => trans('auth::ui.role.button_update')])

							{!! Form::close() !!}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop
