
<div class="m-login__forget-password">
    <div class="m-login__head">
        <h3 class="m-login__title">
            {{ trans('auth::ui.login.forgot') }}
        </h3>
        <div class="m-login__desc">
            {{ trans('auth::ui.login.forgot_subtitle') }}
        </div>
    </div>
    <form class="m-login__form m-form" action="">
        <div class="form-group m-form__group">
            <input class="form-control m-input" type="text" placeholder="{{ trans('auth::ui.login.email') }}" name="email" id="m_email" autocomplete="off">
        </div>
        <div class="m-login__form-action">
            <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                {{ trans('auth::ui.login.send_forgot') }}
            </button>
            <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                {{ trans('auth::ui.login.cancel_forgot') }}
            </button>
        </div>
    </form>
</div>
