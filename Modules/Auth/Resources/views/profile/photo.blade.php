<div class="profile-box">
    <div class="profile-cover-pic">
        <div class="profile-image-overlay"></div>
    </div>
    <div class="profile-info text-center">
        <div class="profile-img-wrap">
            <img class="inline-block mb-10" src="">
            <div class="fileupload btn btn-default">
                <span class="btn-text">Editar</span>
                <input class="upload" type="file">
            </div>
        </div>
        <h5 class="block mt-10 mb-5 weight-500 capitalize-font txt-primary pb-20">{{ $profile->firstname  }} {{ $profile->lastname }}</h5>
    </div>
</div>