<div class="row" v-if="changeEdit">
    <div class="col-md-12">

        <div class="panel panel-default card-view">

            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">{{ trans('inventory::ui.ingress.title') }}</h6>
                </div>

                @if(Auth::user()->can('create-income'))
                    <a href="{{ route('ingress.create') }}" class="btn btn-success pull-right">
                        {{ trans("inventory::ui.ingress.new_ingress") }}
                    </a>
                @endif

                <div class="clearfix"></div>
            </div>

            <div class="panel-wrapper collapse in">
                <div class="panel-body">

                    <div class="alert alert-warning alert-dismissable alert-style-1" v-if="ingress.length < 1">
                        <i class="fa fa-warning"></i>{{ trans('inventory::ui.discard.error.not_items') }}
                    </div>

                    <div class="table-wrap" v-if="ingress.length > 0">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>{{ trans('inventory::ui.ingress.index') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.created_at') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.clients') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.receiver_to') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.deliver_to') }}</th>

                                    <th>{{ trans('inventory::ui.ingress.provider') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.no_document') }}</th>

                                    <th>{{ trans('inventory::ui.ingress.status') }}</th>

                                    @if(Auth::user()->can(['update-income', 'delete-income']))
                                        <th>{{ trans('inventory::ui.ingress.operation_label') }}</th>
                                    @endif
                                </tr>
                                </thead>

                                <tbody>
                                <tr v-for="(income, index) in ingress" :key="income.id" v-if="income.ingressitems.length > 0">
                                    <td> @{{ income.id }}</td>
                                    <td> @{{ income.created_at }}</td>
                                    <td> @{{ income.client.shortname }}</td>
                                    <td> @{{ income.receive_to.username }}</td>
                                    <td> @{{ income.deliver_to }}</td>
                                    <td> @{{ income.provider }}</td>
                                    <td> @{{ income.no_document }}</td>
                                    <td>
                                        <span v-if="income.status == 0" class="badge badge-primary">Guardado</span>
                                        <span v-if="income.status == 1" class="badge badge-success">Impresa</span>
                                    </td>
                                    @if(Auth::user()->can(['update-income', 'delete-income']))
                                        <td>
                                            @if(Auth::user()->can('update-income'))
                                                <a href="#" @click.prevent="editBtn(income)" v-if="income.status == 0" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @endif

                                            @if(Auth::user()->can('read-income'))
                                                <a href="#" @click.prevent="viewPrint(income)" v-if="income.status == 0" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            @endif

                                            @if(Auth::user()->can('read-income'))
                                                <a :href="'generate/ingress/2/'+income.id" v-if="income.status == 1" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            @endif

                                            @if(Auth::user()->can('delete-income'))
                                                <a href="#" @click.prevent="deleteIngress(income)" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>{{ trans('inventory::ui.ingress.index') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.created_at') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.clients') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.receiver_to') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.deliver_to') }}</th>

                                    <th>{{ trans('inventory::ui.ingress.provider') }}</th>
                                    <th>{{ trans('inventory::ui.ingress.no_document') }}</th>

                                    <th>{{ trans('inventory::ui.ingress.status') }}</th>
                                    @if(Auth::user()->can(['update-income', 'delete-income']))
                                        <th>{{ trans('inventory::ui.ingress.operation_label') }}</th>
                                    @endif
                                </tr>
                                </tfoot>

                            </table>

                        </div>
                    </div>
                </div>
            </div>


            <ul class="pagination" v-if="ingress.length > 0">
                <li v-if="meta.current_page > 1">
                    <a href="#" @click.prevent="changePage(meta.current_page - 1)" rel="prev">«</a>
                </li>

                <li v-for="page in pagesNumber" :class="[ page == isActived ? 'active': '']">
                    <a href="#" @click.prevent="changePage(page)">@{{ page }}</a>
                </li>

                <li v-if="meta.current_page < meta.last_page" disabled="">
                    <a href="#" @click.prevent="changePage(meta.current_page + 1)" rel="next">»</a>
                </li>
            </ul>

        </div>

    </div>
</div>