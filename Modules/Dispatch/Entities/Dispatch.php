<?php

namespace Modules\Dispatch\Entities;

use Illuminate\Database\Eloquent\Model;

class dispatch extends Model
{
    protected $table = 'dispatch';
    protected $fillable = [
        'ot_id',
        'status',
        'observacion',
        'entregar',
        'prints'
    ];

    public function ot()
    {
        return $this->belongsTo('Modules\Ot\Entities\Ot');
    }

    public function status_item()
    {
        return $this->hasMany('Modules\Dispatch\Entities\StatusItem');
    }

    /**
     * @param $ot_id
     * @return mixed
     * recibe como parametro el id de la ot donde se consultara los despachos creados
     */
    public function get_dispatch($id)
    {

        return $this->select('dispatch.status','dispatch.created_at','dispatch.updated_at','status_item.id AS sti_id',
            'status_item.quantity AS sti_qty','status_item.inventory_id AS sti_invid','status_item.dispatch_id AS sti_dispatch',
            'inventory.quantity AS inv_qty','inventoryitems.code_sku','inventoryitems.description','clients.fullname','clients.shortname',
            'inventorytypes.type')
            ->join('status_item','dispatch.id','=','status_item.dispatch_id')
            ->join('inventory','status_item.inventory_id','=','inventory.id')
            ->join('inventoryitems','inventory.inventoryitems_id','=','inventoryitems.id')
            ->join('inventorytypes','inventoryitems.inventorytypes_id','=','inventorytypes.id')
            ->join('clients','inventory.clients_id','=','clients.id')
            ->where(['dispatch.id' => $id,'status_item.deleted_at' => null])
            ->orderBy('inventorytypes.type', 'asc')
            ->get();
    }

    public function uistatus()
    {
        return $this->belongsToMany('Modules\InventarioUnico\Entities\Uistatus');
    }
}
