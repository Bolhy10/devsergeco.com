<?php

namespace Modules\Ot\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OtDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * clients: datos de la tabla clientes
         */
        /*$clients = [
            ['shortname' => 'ENSA','fullname' => 'ENSA PANAMA', 'have_inventary' => 1, 'phone'=>'292-5689','address'=>'Panamá, Via España','created_at' => date("Y-m-d H:i:s"), 'status' => 1]
        ];*/
        /*$type_inventary = [
            ['client_id' => 0,'nombre' => 'Inventario de SERGECO','warehouses_id'=>1,'created_at' => date("Y-m-d H:i:s"), 'status' => 1],
            ['client_id' => 1,'nombre' => 'Inventario de ENSA','warehouses_id'=>1,'created_at' => date("Y-m-d H:i:s"), 'status' => 1]
        ];*/
        $status_ot = [
            ['name'=>'Iniciado','status'=>1],
            ['name'=>'Finalizado','status'=>1]
        ];
        $register_ot = [
            ['code_ot'=>'OT23A56','clients_id'=>1,'project_name'=>'Nuevo Reparto Panamá, Cambio de postes','place'=>'Nuevo Reparto Panamá','zone'=>'Panamá','created_at' => date("Y-m-d H:i:s")]
        ];
        $stot = [
            ['ot_id'=>1,'status_id'=>1,'created_at' => date("Y-m-d H:i:s")]
        ];
        $users_ot = [
            ['name'=>'Carlos','lastname'=>'Gonzales','ot_id'=>1,'status'=>1]
        ];
        Model::unguard();

        //DB::table('clients')->insert($clients);
        //DB::table('client_inventary')->insert($type_inventary);
        //DB::table('status_ot')->insert($status_ot);
        //DB::table('ot')->insert($register_ot);
        //DB::table('stot')->insert($stot);
        //DB::table('users_ot')->insert($users_ot);
        // $this->call("OthersTableSeeder");
    }
}
