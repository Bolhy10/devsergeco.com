<div class="alert alert-warning alert-dismissable alert-style-1" v-if="allInventory.length < 1">
    <i class="fa fa-warning"></i>{{ trans('inventory::ui.inventory.not_found') }}
</div>

<table class="table table-hover table-striped display  pb-30" v-if="allInventory.length > 0">
    <thead>
    <tr>
        <th>{{ trans('inventory::ui.inventory.index') }}</th>
        <th>{{ trans('inventory::ui.inventory.code') }}</th>
        <th>{{ trans('inventory::ui.inventory.description') }}</th>
        <th>{{ trans('inventory::ui.inventory.type') }}</th>
        <th>{{ trans('inventory::ui.inventory.clients') }}</th>
        <th>{{ trans('inventory::ui.inventory.warehouse') }}</th>
        <th>{{ trans('inventory::ui.inventory.available') }}</th>
        <th>{{ trans('inventory::ui.inventory.update') }}</th>
    </tr>
    </thead>

    <tbody>
    <tr v-for="(allInv, index) in allInventory" :key="allInv.id">
        <td>@{{ allInv.id }}</td>
        <td>@{{ allInv.items.code }}</td>
        <td>@{{ allInv.items.description }}</td>
        <td>@{{ allInv.items.type.name }}</td>
        <td>@{{ allInv.client.shortname }}</td>
        <td>@{{ allInv.warehouse.name }}</td>
        <td><strong>@{{ allInv.available }}</strong></td>
        <td>@{{ allInv.updated_at }}</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <th>{{ trans('inventory::ui.inventory.index') }}</th>
        <th>{{ trans('inventory::ui.inventory.code') }}</th>
        <th>{{ trans('inventory::ui.inventory.description') }}</th>
        <th>{{ trans('inventory::ui.inventory.type') }}</th>
        <th>{{ trans('inventory::ui.inventory.clients') }}</th>
        <th>{{ trans('inventory::ui.inventory.warehouse') }}</th>
        <th>{{ trans('inventory::ui.inventory.available') }}</th>
        <th>{{ trans('inventory::ui.inventory.update') }}</th>
    </tr>
    </tfoot>

</table>

<div class="footer">

    {{----}}
{{--<ul class="pagination">--}}
        {{--<li v-if="meta.current_page > 1">--}}
            {{--<a href="#" @click.prevent="changePage(meta.current_page - 1)" rel="prev">«</a>--}}
        {{--</li>--}}

        {{--<li v-for="page in pagesNumber" :class="[ page == isActived ? 'active': '']">--}}
            {{--<a href="#" @click.prevent="changePage(page)">@{{ page }}</a>--}}
        {{--</li>--}}

        {{--<li v-if="meta.current_page < meta.last_page" disabled="">--}}
            {{--<a href="#" @click.prevent="changePage(meta.current_page + 1)" rel="next">»</a>--}}
        {{--</li>--}}
    {{--</ul>--}}

    {{--<button type="button" @click="refreshItem()" class="btn btn-primary pull-right">--}}
        {{--Refrescar--}}
    {{--</button>--}}

</div>

