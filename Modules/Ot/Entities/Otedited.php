<?php

namespace Modules\Ot\Entities;

use Illuminate\Database\Eloquent\Model;

class otedited extends Model
{
    protected $fillable = [];
    protected $table = 'ot_edited';
}
