const inventory = new Vue({
    el: '#inventory',
    data:function () {

        return {

            allInventory: [],
            warehouses: [],
            fill_clients: 0,
            fill_warehouses:0,
            code: '',
            meta: {
                'current_page': 0,
                'from': 0,
                'last_page': 0,
                'path': '',
                'per_page': 0,
                'to': 0,
                'total' : 0
            }
        }

    },

    created:function () {

        this.getInventory();

    },

    computed:{

        isActived() {
            return this.meta.current_page;
        },
        pagesNumber(){

            if(!this.meta.to){
                return [];
            }

            var from = this.meta.current_page - 3;
            if(from < 1){
                from = 1;
            }

            var to = from + (3 * 2);
            if(to >= this.meta.last_page){
                to = this.meta.last_page;
            }

            var pagesArray = [];

            while(from <= to){
                pagesArray.push(from);
                from++;
            }

            return pagesArray;
        }
    },

    methods:{

        getInventory(page){

            var url = '/inventory/api/all';
            if (this.fill_clients > 0){

                url += '/'+this.fill_clients;

                if (this.fill_warehouses > 0){
                    url += '/'+this.fill_warehouses;
                }
            }

            url += '?page='+page;

            axios.get(url).then(res => {

                this.allInventory = res.data.data;
                this.meta = res.data.meta;
                console.log(this.allInventory);
                console.log(url);

            }).catch(error => {

                console.log(error);
                $.toast('Error al obtener los datos. Intente nuevamente.');

            });
        },

        changePage:function (page) {
            this.meta.current_page = page;
            this.getInventory(page);
        },

        getWare:function (id) {

            var url = '/inventory/api/warehouse/'+id;

            if (id > 0){

                axios.get(url).then(res => {

                    this.getInventory();

                    this.warehouses = res.data;

                }).catch(err => {

                    $.toast('Error al obtener los datos.');

                });
            }
        },

        fetchData:function () {
            this.getInventory();
        },

        searchItem:function () {

            if(this.code > 0){

                this.$validator.validateAll().then((result) => {
                    if (result) {

                        var url = '/inventory/api/search/items';

                        axios.post(url, {code: this.code}).then(res=>{

                            if(res.data.status == 200){

                                this.allInventory = res.data.data;
                                this.meta = [];
                                this.code = '';

                                return;
                            }

                            if(res.data.status == 400){

                                $.toast(res.data.message);
                                return
                            }

                        }).catch(error=>{

                            $.toast('Verifique los campos requeridos.');

                            console.log(error);
                        });

                        return;
                    }

                    $.toast('Faltan campos por llenar.');

                });

                return;
            }

            $.toast('Debe agregar el Código.')

        },

        refreshItem:function () {

            this.fill_clients = 0;

            this.fill_warehouses = 0;

            this.getInventory();

        },

        printPDF:function () {

            if(confirm("Seguro que deseas descargar el inventario solicitado?.")){

                window.open('/inventory/generate/all/'+this.fill_clients+'/'+this.fill_warehouses);

                $.toast('se esta procesando para descargar el reporte.');

            }

        }

    }



});