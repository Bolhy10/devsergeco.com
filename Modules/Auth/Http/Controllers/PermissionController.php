<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Auth\Entities\Permission;
use Modules\Auth\Http\Requests\PermissionRequest;
use Nwidart\Modules\Routing\Controller;

class PermissionController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->can('read-permissions')){
        	$permissions = Permission::paginate(5);

        	return view('auth::permission.index', compact('permissions'));
		}

		return redirect('auth/logout');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
    	if (Auth::user()->can('create-permissions')){
			return view('auth::permission.create');
		}

		return redirect('auth/logout');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PermissionRequest $request)
    {
    	if (Auth::user()->can('create-permissions')){

    		$data = Permission::create($request->all());

    		$permission = Permission::findOrFail($data->id);

    		Session::flash('message', trans('auth::ui.permission.message_create',
				array('name'=> $permission->name )));

    		return redirect('auth/permission/create');
		}

		return redirect('auth/logout');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
	{
		if(Auth::user()->can('update-permissions')) {

			$permission = Permission::findOrFail($id);

			return view('auth::permission.edit', compact('permission'));

		}

		return redirect('auth/logout');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, PermissionRequest $request)
    {
		if(Auth::user()->can('update-permissions')) {

			$permission = Permission::findOrFail($id);

			$permission->update($request->all());

			Session::flash('message', trans('auth::ui.permission.message_update', array('name' => $permission->name)));

			return redirect('auth/permission');
		}

		return redirect('auth/logout');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
