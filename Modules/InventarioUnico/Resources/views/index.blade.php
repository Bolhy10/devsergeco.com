@extends('layouts.master')
@section('pageTitle', 'Inventario Especial')

@section('content')

    <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="panel panel-default card-view">
                        <!-- panel heading -->
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6>Transformadores</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- end panel heading -->
                        <!-- panel body -->
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nombre</th>
                                                    <th>Código</th>
                                                    <th>Cantidad</th>
                                                    <th>Cliente</th>
                                                    <th>Bodega</th>
                                                    <th>Creado</th>
                                                </tr>
                                            </thead>
                                        </table>
                                </div>
                            </div>
                        </div>
                        <!-- end panel body -->
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="row">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6>Carretes de cables</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- panel body -->
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">

                                    <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Nombre</th>
                                                        <th>Código</th>
                                                        <th>Cantidad</th>
                                                        <th>Cliente</th>
                                                        <th>Bodega</th>
                                                        <th>Creado</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($i_cables as $ic)
                                                        @if($ic->inventory->inventoryitems->inventorytypes->type == "Cables")
                                                            <tr>
                                                                <td> {{ $ic->id }} </td>
                                                                <td> {{ $ic->nombre }} </td>
                                                                <td> {{ $ic->codigounico }} </td>
                                                                <td> {{ $ic->cantidad }} mt </td>
                                                                <td> {{ $ic->inventory->clients->shortname }} </td>
                                                                <td> {{ $ic->inventory->warehouses->name }} </td>
                                                                <td> {{ $ic->created_at }} </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $i_cables->links() }}
                                    </div>

                            </div>
                        </div>
                        <!-- end panel body -->
                    </div>
                </div>
            </div>
    </div>


@endsection
