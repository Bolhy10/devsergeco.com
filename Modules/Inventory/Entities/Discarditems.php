<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discarditems extends Model
{
    use SoftDeletes;

    protected $table ='discarditems';

    protected $fillable = [
        'inventoryitems_id',
        'discard_id',
        'quantity',
        'observations'
    ];

    protected $dates = ['deleted_at'];

    public function discard(){

        return $this->belongsTo('Modules\Inventory\Entities\Discard');
    }

    public function inventoryitems(){

        return $this->belongsTo('Modules\Inventory\Entities\Inventoryitems');
    }


}
