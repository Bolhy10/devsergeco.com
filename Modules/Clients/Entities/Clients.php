<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clients extends Model
{
    use SoftDeletes;

    protected $fillable = ['shortname', 'fullname', 'phone', 'address', 'status'];

    protected $dates = ['deleted_at'];

    public function warehouses()
    {
        return $this->belongsToMany('Modules\Warehouses\Entities\Warehouses', 'warehouses_clients');
    }

    public function ingress(){

        return $this->hasMany('Modules\Inventory\Entities\Ingress');

    }

    public function discard(){

        return $this->hasMany('Modules\Inventory\Entities\Discard');

    }

    public function inventory(){

        return $this->hasMany('Modules\Inventory\Entities\Inventory','clients_id');

    }


}
