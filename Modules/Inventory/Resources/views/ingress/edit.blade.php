<div class="row" v-if="!changeEdit">
    <div class="col-md-12">

        <form method="post" v-on:submit.prevent="updateIngress(fillIngress)">

            <div class="row mb-10">
                <div class="col-lg-12">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">
                            Actualizar Ingreso
                        </button>

                        <a href="{{ route('inventory.ingress') }}" class="btn btn-warning">
                            Volver
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">

                @include('inventory::ingress.form')
                @include('inventory::ingress.table')

            </div>

        </form>

    </div>
</div>


