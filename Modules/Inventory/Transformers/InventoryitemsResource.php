<?php

namespace Modules\Inventory\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class InventoryitemsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code_sku,
            'description' => $this->description,
            'types' => new InventorytypesResource($this->whenLoaded('inventorytypes')),
            'type' => new InventorytypesResource($this->inventorytypes),
            'inventory' => new InventoryResource($this->inventory)
        ];
    }
}
