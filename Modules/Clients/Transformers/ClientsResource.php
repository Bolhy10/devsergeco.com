<?php

namespace Modules\Clients\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ClientsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shortname' => $this->shortname,
            'fullname' => $this->fullname
        ];
    }
}
