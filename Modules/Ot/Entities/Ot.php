<?php

namespace Modules\Ot\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ot extends Model
{
    protected $fillable = [
        'code_ot',
        'clients_id',
        'project_name',
        'place',
        'zone'
    ];
    protected $table = 'ot';

    public function clients(){
        return $this->belongsTo('Modules\Clients\Entities\Clients');
    }

    public function stot()
    {
        return $this->hasOne('Modules\Ot\Entities\Stot');
    }

    //relacion inversa con la tabla zonas
    public function zones()
    {
        return $this->belongsTo('Modules\Warehouses\Entities\Zones','zone');
    }

    /**
     * @return Todos los datos de la tabla register_ot relacionadas con el id de cliente
     */

    public function ot_all()
    {
        return $this->select('ot.id','ot.code_ot','ot.created_at','ot.project_name','ot.place','ot.zone',
            'clients.fullname','stot.status')
            ->join('clients','ot.clients_id','=','clients.id')
            ->join('stot','ot.id','=','stot.ot_id')
            ->get();
    }

    public function ot_by_warehouses($id)
    {
        return $this->select('ot.id','ot.code_ot','ot.created_at','ot.project_name','ot.place','ot.zone',
            'clients.fullname','stot.status')
            ->join('clients','ot.clients_id','=','clients.id')
            ->join('stot','ot.id','=','stot.ot_id')
            ->where('ot.zone',$id)
            ->get();
    }

    /**
     * @param $id_user
     * @return mixed Devuelve todos los datos de una ot relacionados a un usuario
     */
    public function ot_by_user($id_user)
    {
        return $this->select('ot.id','ot.code_ot','ot.created_at','ot.project_name','ot.place','ot.zone',
            'clients.fullname','stot.status')
            ->join('clients','ot.clients_id','=','clients.id')
            ->join('stot','ot.id','=','stot.ot_id')
            ->join('users_ot','ot.id','=','users_ot.ot_id')
            ->where('users_ot.user_id',$id_user)
            ->get();
    }

    /**
     * @param $id ($id de la ot) extrae datos de una Ot especifica
     */
    public function ot_all_unique($id){
        return $this->select('ot.id','ot.code_ot','ot.created_at','ot.project_name','ot.place',
            'zones.name','ot.clients_id','clients.shortname','clients.fullname','stot.status','warehouses.id AS w_id')
            ->join('clients','ot.clients_id','=','clients.id')
            ->join('zones','ot.zone','=','zones.id')
            ->join('stot','ot.id','=','stot.ot_id')
            ->join('warehouses','zones.id','=','warehouses.zones_id')
            ->where('ot.id',$id)
            ->get();
    }

    public function users()
    {
        return $this->hasManyThrough('Modules\Auth\Entities\User','Modules\Ot\Entities\UsersOt','ot_id','user_id');
    }

    public function users_ot($id) {
        return $this->select('users.id','users.firstname','users.lastname','roles.display_name')
            ->join('users_ot','ot.id','=','users_ot.ot_id')
            ->join('users','users_ot.user_id','=','users.id')
            ->join('role_user','users.id','=','role_user.user_id')
            ->join('roles','role_user.role_id','=','roles.id')
            ->where('ot.id',$id)
            ->get();
    }

    /**
     * @param $client_id id del cliente a extraer el inventario
     * @param $w_id
     * @return mixed
     */
    public function inventory_clients($client_id, $w_id)
    {
        return $this->select('inventory.id','inventory.quantity','inventoryitems.description','inventoryitems.code_sku',
        'inventorytypes.type')
            ->join('inventory','ot.clients_id','=','inventory.clients_id')
            ->join('inventoryitems','inventory.inventoryitems_id','=','inventoryitems.id')
            ->join('inventorytypes','inventoryitems.inventorytypes_id','=','inventorytypes.id')
            ->where([
                ['ot.clients_id','=',$client_id],
                ["inventory.status","=",1],
                ["inventory.warehouses_id","=",$w_id]
            ])
            ->get();
    }

    //datos que se muestran en el select de agregar items al despacho
    public function item_data($id)
    {
        return DB::table('inventory')->select('inventory.id','inventory.quantity','inventoryitems.description','inventoryitems.code_sku',
            'inventorytypes.type','inventorytypes.id AS type_id','clients.fullname')
            //->join('inventory','ot.clients_id','=','inventory.clients_id')
            ->join('inventoryitems','inventory.inventoryitems_id','=','inventoryitems.id')
            ->join('inventorytypes','inventoryitems.inventorytypes_id','=','inventorytypes.id')
            ->join('clients','inventory.clients_id','=','clients.id')
            ->where('inventory.id',$id)
            ->get();
    }
}
