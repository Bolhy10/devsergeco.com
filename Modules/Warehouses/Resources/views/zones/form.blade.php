<div class="form-group">
    <label class="control-label mb-10">{{ trans('warehouses::ui.zones.name') }}</label>
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=> trans('warehouses::ui.zones.name')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('warehouses::ui.zones.address') }}</label>
    {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder'=> trans('warehouses::ui.zones.address')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('warehouses::ui.zones.provinces_id') }}</label>
    {!! Form::select('provinces_id', $provinces, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una provincia']) !!}
</div>


<div class="form-group">
    <label class="control-label mb-10">{{ trans('warehouses::ui.zones.status') }}</label>
    {!! Form::select('status', ['1' => 'Activo', '0' => 'Desactivado'], null, ['class'=> 'form-control','placeholder' => 'Seleccione el estado']) !!}
</div>

<div class="form-group mb-0">

    {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}

    <a href="{{ route('zones.index') }}" class="btn btn-warning">{{ trans('auth::ui.general.back') }}</a>

</div>