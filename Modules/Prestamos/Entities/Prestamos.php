<?php

namespace Modules\Prestamos\Entities;

use Illuminate\Database\Eloquent\Model;

class Prestamos extends Model
{
    protected $fillable = [
        'dispatch_id',
        'invtomado_id',
        'invrecibido_id',
        'inventory_id',
        'status'
    ];
    protected $table = 'prestamos';

    public function inventory(){
        return $this->belongsTo('Modules\Inventory\Entities\Inventory');
    }

    public function dispatch()
    {
        return $this->belongsTo('Modules\Dispatch\Entities\Dispatch');
    }

    public function warehouses()
    {
        return $this->belongsTo('Modules\Warehouses\Entities\Warehouses');
    }


}
