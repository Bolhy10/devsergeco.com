<?php

namespace Modules\Inventory\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Auth\Transformers\UsersResource;
use Modules\Clients\Transformers\ClientsResource;
use Modules\Warehouses\Transformers\WarehousesResource;

class DiscardResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,

            'no_document' => $this->no_document,

            'receive_to' => $this->receive_to,

            'observations' => $this->observations,

            'status' => $this->status,

            'created_at' => $this->created_at->format('Y/m/d'),

            'updated_at' => $this->updated_at->format('Y/m/d'),

            'client' => new ClientsResource($this->whenLoaded('clients')),

            'warehouse' => new WarehousesResource($this->whenLoaded('warehouses')),

            'delivered' => new UsersResource($this->whenLoaded('users')),

            'discarditems' => DiscarditemsResource::collection($this->whenLoaded('discarditems'))

        ];

    }
}
