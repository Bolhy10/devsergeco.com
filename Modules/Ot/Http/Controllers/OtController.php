<?php

namespace Modules\Ot\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Barryvdh\DomPDF\Facade as PDF;

use Modules\Auth\Entities\User;
//use Modules\Inventary\Entities\ItemGeneral;
//use Modules\Inventary\Entities\ItemsInventary;

use Modules\Inventory\Entities\Inventory;
use Modules\Warehouses\Entities\Warehouses;
use Modules\Warehouses\Entities\Zones;
use Validator;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Modules\Auth\Entities\Role;

use Modules\Dispatch\Entities\Dispatch;

use Modules\Returns\Entities\Returns;

use Modules\Prestamos\Entities\Prestamos;

use Modules\Ot\Entities\Ot;
use Modules\Clients\Entities\Clients;
use Modules\Ot\Entities\UsersOt;
use Modules\Ot\Entities\Stot;
use Modules\Dispatch\Entities\StatusItem;
use Modules\Ot\Entities\Otedited;

class OtController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id_bodega = 0)
    {
        if (Auth::user()->can('read-ot'))
        {
            $title = 'Proyectos';
            $bodegas = Warehouses::all()->where('status',1);
            $register = new Ot();
            $id_user = Auth::id();
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor') ){
                //if(Role::users())
                //$ots = RegisterOt::all();
                $ots = $register->ot_all();
                $ots_i = Stot::where('status', 1)->count();   //iniciado
                $ots_p = Stot::where('status',2)->count();    //pausado
                $ots_c = Stot::where('status',3)->count();    //cerrado
                $ots_e = Stot::where('status',4)->count();   //eliminada
                $ots_tot = Ot::count();

                return view('ot::index',[ 'ots' => $ots, 'ot_i' => $ots_i, 'ot_c' => $ots_c,'ot_p'=>$ots_p,
                    'ot_e' =>$ots_e, 'ot_tot' => $ots_tot, 'bodegas' => $bodegas, 'titulo' => $title ]);
            }
            else if(Auth::user()->hasRole('inspector'))
            {
                $ots = $register->ot_by_user( $id_user );
                $ots_i = Stot::with(['users_ot' => function ($query) {
                    $query->where('user_id', '=', Auth::id());
                }])->where('status',1)->count();
                $ots_p = Stot::with(['users_ot' => function ($query) {
                    $query->where('user_id', '=', Auth::id());
                }])->where('status',2)->count();
                $ots_c = Stot::with(['users_ot' => function ($query) {
                    $query->where('user_id', '=', Auth::id());
                }])->where('status',3)->count();
                $ots_e = Stot::with(['users_ot' => function ($query) {
                    $query->where('user_id', '=', Auth::id());
                }])->where('status',4)->count();
                return view('ot::inspector.index',[ 'ots' => $ots, 'ot_i' => $ots_i, 'ot_c' => $ots_c,'ot_p'=>$ots_p,
                    'ot_e' =>$ots_e, 'bodegas' => $bodegas,'titulo' => $title ]);
            }
            else if( Auth::user()->hasRole('warehouseman') || Auth::user()->hasRole('warehouse-chief') )
            {
                if ($id_bodega == 0) 
                {
                    $titulo_i = 'Seleccionar Bodega.';
                    return view('ot::almacenista.seleccionar_bodega',['bodegas' => $bodegas, 'titulo' => $titulo_i ]);
                }
                else 
                {
                    //print $id_bodega;
                    $ots = $register->ot_by_warehouses($id_bodega);
                    //var_dump($ots);
                    //return response()->json($ots);
                    return view('ot::almacenista.index',['ots' => $ots, 'titulo' => $title]);
                }
            }
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if (Auth::user()->can('create-ot'))
        {
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor'))
            {
                $b = $request->bodega;
                $bodega = Warehouses::find($b);
                $zona = $bodega->zones->name;
                $id_zona = $bodega->zones->id;
                $count_clients = $bodega->clients->where('status',1)->count();
                    if($count_clients > 0)
                    {
                        $clients = $bodega->clients->where('status',1);

                        $rol_supervisor = Role::where('name','supervisor')->with('users')->first();
                        $rol_inspector = Role::where('name','inspector')->with('users')->first();
                        $supervisores = $rol_supervisor->load('users');
                        $inspectores = $rol_inspector->load('users');

                        return view('ot::pages/create', ['clients' => $clients, 'super' => $supervisores, 'inspect' => $inspectores,'zona' => $zona,'id_zona'=>$id_zona]);
                    }
                    else
                        {
                            $array_error = 'La Bodega que ha elegido no cuenta con un inventario creado';
                            return view('ot::pages.erroronchangestatus',['error_change' => $array_error, 'ot_id' => 0]);
                        }
                }
                else
                {
                    return redirect()->route('ot.index');
                }
            }
            else
            {
                return redirect()->route('ot.index');
            }
        }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code_ot' => 'required',
            'cliente' => 'required',
            'project_name' => 'required',
            'place' => 'required',
            'user' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/ot/crear-ot')
                ->withErrors($validator)
                ->withInput();
        }else{
            $id_ot = '';
            $o_t = new Ot();
            //$users_ot = new  UsersOt();

            $o_t->code_ot = $request->code_ot;
            $o_t->clients_id = $request->cliente;
            $o_t->project_name = $request->project_name;
            $o_t->place = $request->place;
            $o_t->zone = $request->zone;
            /**
             * Si se almacenan los datos de la Ot se procede a almacenar los datos de los ususarios relacionados a esa ot
             **/
            if($o_t->save())
            {
                /**
                 * id_s = id almacenado de la ot recien creada
                 **/
                $id_ot = $o_t->id;

                /**
                 * Se almacena el estado de la OT
                 **/
                $stot = new Stot();
                $stot->ot_id = $id_ot;
                $stot->status = 1;
                $stot->save();
                /**
                 * id_users id's de los usuarios
                 **/
                $id_users = $request->user;
                foreach ($id_users as $i):
                    $users_ot = new  UsersOt();
                    $users_ot->user_id = $i;
                    $users_ot->ot_id = $id_ot;
                    $users_ot->save();
                endforeach;

            }

            return redirect()->route('ot.view_ot',$id_ot);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('ot::show');
    }

    /**
     * Panel principal de las Ot
     */
    public function status_ot($id)
    {
        $ot = new Ot;
        $stot = Stot::all()->where('ot_id','=',$id)->first();

        $ot_d = $ot->ot_all_unique($id);
        $ot_u = $ot->users_ot($id);

        $status = $stot->status;

        //despachos
        $cant_despachos = Dispatch::where('ot_id',$id)->count();
        $despachos = Dispatch::with('status_item.inventory.inventoryitems')->where('ot_id',$id)->paginate(4);

        //devoluciones
        $cant_devolucion = Returns::where('ot_id',$id)->count();
        $devoluciones = Returns::with('returns_item.inventory.inventoryitems')->where('ot_id',$id)->paginate(4);

        return view('ot::pages.view-ot',['ot_data' => $ot_d,'u_ot' => $ot_u,'status'=>$status,'despachos'=>$despachos,
            'cant_desp' => $cant_despachos, 'id_ot' => $id,'cant_dev' => $cant_devolucion, 'devoluciones' => $devoluciones]);
    }

    public function change_status_ot(Request $request)
    {
        $ot_id = $request->ot_id;
        $status = $request->status;
        //$despachos = Dispatch::where('ot_id',$ot_id)->firstOrFail();

        //si el estado es 1 significa que se esta reactivando o 2 pausar
        if($status == 1 || $status == 2)
        {
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor'))
            {
                $ot =Stot::all()->where('ot_id',$ot_id)->first();
                $ot->status = $status;
                $ot->save();
            }
        }
        //si el estado es cerrar
        elseif($status ==  3)
        {
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor'))
            {
                //cantidad total de despachos
                $cant_desp = Dispatch::where('ot_id', $ot_id)->count();
                //cantidad de despachos finalizados
                $cant_fin = Dispatch::where(['ot_id' => $ot_id, 'status' => 2])->count();

                $cant_dev = Returns::where('ot_id', $ot_id)->count();
                $dev_fin = Returns::where(['ot_id' => $ot_id, 'status' => 2])->count();
                //debe haber por lo menos un despacho para poder cerrar la Ot
                if ($cant_desp > 0)
                {
                    //los despachos existentes deben haber sido finalizados
                    if($cant_fin == $cant_desp && $dev_fin == $cant_dev)
                    {
                        $ot =Stot::all()->where('ot_id',$ot_id)->first();
                        $ot->status = $status;
                        $ot->save();
                        //return back();
                    }
                    //de no haber sido finalizado
                    else
                        {
                            $array_error = 'Los despachos relacionados a la Ot deben haber sido finalizados';
                            return view('ot::pages.erroronchangestatus',['error_change' => $array_error,'ot_id' => $ot_id]);
                        }
                }
                //si no existe ningun despacho creado
                else
                    {
                        $array_error = 'No puede cerrar una Ot si no has creado un despacho';
                        return view('ot::pages.erroronchangestatus',['error_change' => $array_error,'ot_id' => $ot_id]);
                    }
            }
            //de no contar con los permisos
            else
                {
                    $array_error = 'Disculpe no cuenta con los permisos necesarios para realizar esta acción';
                    return view('ot::pages.erroronchangestatus',['error_change' => $array_error,'ot_id' => $ot_id]);
                }
        }
        //status de eliminar la Ot
        elseif ($status == 4)
        {
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor'))
            {
                //cantidad de despachos asignadas a la OT
                $cant_desp = Dispatch::where('ot_id', $ot_id)->count();

                //si no existe un despacho asignado a la Ot, se puede eliminar la OT
                if($cant_desp == 0)
                {
                    $ot =Stot::all()->where('ot_id',$ot_id)->first();
                    $ot->status = $status;
                    $ot->save();
                    //return back();
                }
                else
                {
                    $array_error = 'No se puede eliminar la Ot, ya que existen despachos creados.';
                    return view('ot::pages.erroronchangestatus',['error_change' => $array_error,'ot_id' => $ot_id]);
                }

            }
            else
            {
                $array_error = 'Disculpe no cuenta con los permisos necesarios para realizar esta acción';
                return view('ot::pages.erroronchangestatus',['error_change' => $array_error,'ot_id' => $ot_id]);
            }
        }

        return redirect()->route('ot.view_ot',$ot_id);
        //return $this->status_ot($ot_id);
        //return view()
    }

    /**
     * dispatch_item: función que muestra los recursos para agregar items a un despacho
     */
    public function dispatch_item($id)
    {

        $o_t = new Ot;
        $dat_ot = $o_t->ot_all_unique($id)->first();
        $id_bodega = $dat_ot->w_id;
        $bodega = Warehouses::find($id_bodega);
        $clients_bodega = $bodega->clients->where('status',1);

        //si el estado es iniciado se procede a crear los despachos
        if($dat_ot->status == 1)
        {
            //$cli_id = $dat_ot->clients_id;
            $cant_inventario = $o_t->inventory_clients($dat_ot->clients_id,$id_bodega)->count();

            //si el cliente cuenta con inventario
            if($cant_inventario > 0){
                $inventario = $o_t->inventory_clients($dat_ot->clients_id,$id_bodega);
                $cliente = Clients::find($dat_ot->clients_id);
                $id_cliente = $dat_ot->clients_id;
                $nombre_inventario = $cliente->shortname;
            }
            //de lo contrario se extraen los de SERGECO
            else
            {
                $inventario = $o_t->inventory_clients(1,$id_bodega);
                $cliente = Clients::find(1);
                $id_cliente = 1;
                $nombre_inventario = $cliente->shortname;
            }

            return view('ot::pages.dispatch_ot',[ 'cant_inv' => $cant_inventario, 'inventario' => $inventario,
                'ot_data' => $dat_ot,'nombre_inv' => $nombre_inventario,'clientsbodega' => $clients_bodega, 'cliente_id' => $id_cliente ]);
        }

        elseif ($dat_ot->status == 2 )
        {
            return back();
        }
    }

    //función usada al momento de realizar una solicitud de prestamo de inventario
    public function obtener_inventario( $w_id, $c_id )
    {
        $o_t = new Ot();
        //$datos = Inventory::all()->where(['clients_id' => $c_id, 'warehouses_id'=>$w_id]);
        //$datos = $o_t->inventory_clients( $c_id , $w_id );
        $datos = DB::table('inventory')->select('inventory.id','inventory.quantity','inventoryitems.description','inventoryitems.code_sku',
            'inventorytypes.type')
            ->join('inventoryitems','inventory.inventoryitems_id','=','inventoryitems.id')
            ->join('inventorytypes','inventoryitems.inventorytypes_id','=','inventorytypes.id')
            ->where([
                ['inventory.clients_id','=',$c_id],
                ["inventory.status","=",1],
                ["inventory.warehouses_id","=",$w_id]
            ])
            ->get();
        return response()->json( $datos );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getdata_inventary( $id )
    {
        $o_t = new Ot;
        $dat_ot = $o_t->ot_all_unique($id)->first();
        $id_bodega = $dat_ot->w_id;

        $cant_inventario = $o_t->inventory_clients( $id, $id_bodega )->count();

        //si el cliente cuenta con inventario
        if($cant_inventario > 0){
            $inventario = $o_t->inventory_clients( $id, $id_bodega );
        }
        //de lo contrario se extraen los de SERGECO
        else
        {
            $inventario = $o_t->inventory_clients(1, $id_bodega);
        }

        return response()->json($inventario);
    }

    /**
     * @param Request $request
     * @return response Devuelve los items relacionados a la categoria
     */
    public function get_items(Request $request){
        $id_item = $request->iditem;
        $cant = $request->qnt;

        $inventary =new Ot;
        $res = $inventary->item_data($id_item)->first();

        return response()->json( $res );

    }


    /**
     * @param Request $request
     */
    public function save_dispatch(Request $request){

        /**
         * Se guarda el nuevo despacho relacionado a la Ot
         */
        $observacion = $request->observacion;
        $entregar = $request->entregar_a;
        $id_cliente = $request->id_clients;  //id del cliente, (id del inventario)
        $dispatch = new Dispatch;
        $dispatch->ot_id = $request->ot_id;
        $dispatch->observacion = $observacion;
        $dispatch->entregar = $entregar;
        $dispatch->status = 1;     //i: En despacho

        if( $dispatch->save() )
        {
            $id = $request->id;  // array que contiene los id de los materiales
            $cant = $request->cantidad; //array que contiene las cantidades

            foreach ( $id as $n => $i ):
                $itemOt = new StatusItem();
                $itemOt->inventory_id = $i;
                $itemOt->quantity = $cant[$n];
                $itemOt->dispatch_id = $dispatch->id;
                $itemOt->status = 1;
                $itemOt->save();

                $itemP = Inventory::find($i);
                $itemP_idclient = $itemP->clients_id;

                //se valida si los items pertenecen al mismo inventario
                if($itemP_idclient != $id_cliente)
                {
                    //Se procede almacenar los items que se han tomado prestado de otro inventario
                    $prestamos = new Prestamos();
                    $prestamos->dispatch_id = $dispatch->id;
                    $prestamos->invtomado_id = $itemP_idclient;
                    $prestamos->invrecibido_id = $id_cliente;
                    $prestamos->inventory_id = $i;
                    $prestamos->status = 1;
                    $prestamos->save();
                }
            endforeach;
        }

        return redirect()->route('ot.view_ot', $request->ot_id);

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('ot::edit_ot');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * @param $id
     * funcion que se encarga de imprimir los despachos y las devoluciones
     */
    public function prints(Request $request){
        $id = $request->id;
        $option = $request->option;

        //opcion 1 = despacho
        if($option == 1) {
            $despacho = new Dispatch;

            $desp = Dispatch::find($id);
            $n_print = $desp->prints;

            $ot_id = $desp->ot_id;
            $data = $despacho->get_dispatch($id);

            $o_t = new Ot;
            $dat_ot = $o_t->ot_all_unique($ot_id)->first();
            $id_bodega = $dat_ot->w_id;

            $cliente = Clients::find($dat_ot->clients_id);
            $nombre_inventario = $cliente->shortname;

            $ot_u = $o_t->users_ot($ot_id);
            $titulo = 'Despacho';
            $alias = 'DS';
            $hoy = Carbon::parse('now')->format('d/m/Y');

            $desp->prints = $n_print + 1;

            $desp->save();


            /**
             *proceso puede ser despacho o devolucion
             *data_proceso almacena los datos de los materiales relacionados al proceso
             *ot datos de la Ot
             **/

            $pdf = PDF::loadView('ot::pages.print', ['proceso' => $desp, 'data_proceso' => $data, 'ot' => $dat_ot,
                'titulo' => $titulo ,'users' => $ot_u, 'alias' => $alias, 'hoy' => $hoy,'name_inventario' => $nombre_inventario, 'no_impresion' => $n_print ] );
            return $pdf->stream('factura.pdf');

            /*return view('ot::pages.print',['proceso' => $desp, 'data_proceso' => $data, 'ot' => $dat_ot,
                'titulo' => $titulo ,'users' => $ot_u, 'alias' => $alias, 'hoy' => $hoy,'name_inventario' => $nombre_inventario, 'no_impresion' => $n_print ]);*/
        }
        //opcion devolucion
        elseif($option == 2)
        {
            $devolucion = new Returns;
            $data_devolucion = Returns::find($id);
            $n_print = $data_devolucion->prints;

            $ot_id = $data_devolucion->ot_id;
            $descripcion_devolucion = $devolucion->get_return($id);

            $o_t = new Ot;
            $dat_ot = $o_t->ot_all_unique($ot_id)->first();
            $id_bodega = $dat_ot->w_id;

            $cliente = Clients::find($dat_ot->clients_id);
            $nombre_inventario = $cliente->shortname;

            $ot_u = $o_t->users_ot($ot_id);
            $titulo = 'Devolución';
            $alias = 'DV';
            $hoy = Carbon::parse('now')->format('d/m/Y');

            $data_devolucion->prints = $n_print + 1;

            $data_devolucion->save();

            //$pdf = PDF::loadView('ot::pages.print', ['proceso' => $devolucion, 'data_proceso' => $descripcion_devolucion, 'ot' => $dat_ot,
                //'titulo' => $titulo ,'users' => $ot_u, 'alias' => $alias, 'hoy' => $hoy,'name_inventario' => $nombre_inventario, 'no_impresion' => $n_print ] );
            //return $pdf->stream('factura.pdf');
            return view('ot::pages.print');

        }
    }
}
