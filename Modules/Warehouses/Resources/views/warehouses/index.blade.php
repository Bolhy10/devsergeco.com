@extends('layouts.master')

@section('pageTitle', trans('warehouses::ui.title'))

@section('content')

    <div class="row">
        <div class="col-md-12">

            @include('partials.message')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('warehouses::ui.warehouses.title') }}</h6>
                    </div>

                    @if(Auth::user()->can('create-warehouses'))
                        <a href="{{ route('warehouses.create') }}" class="btn btn-success pull-right">
                            {{ trans("warehouses::ui.warehouses.new_warehouse") }}
                        </a>
                    @endif

                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if(count($warehouses) < 1)
                            <div class="alert alert-warning alert-dismissable alert-style-1">
                                <i class="fa fa-warning"></i>{{ trans('inventory::ui.items.error.not_items') }}
                            </div>
                        @endif

                        @if(count($warehouses) > 0)
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="simpletable" class="table table-striped table-bordered" >
                                    <thead>
                                    <tr>
                                        <th>{{ trans('warehouses::ui.warehouses.code') }}</th>
                                        <th>{{ trans('warehouses::ui.warehouses.name') }}</th>
                                        <th>{{ trans('warehouses::ui.warehouses.zone') }}</th>
                                        <th>{{ trans('warehouses::ui.warehouses.status') }}</th>

                                        @if(Auth::user()->can(['update-warehouses', 'delete-warehouses']))
                                            <th>{{ trans('warehouses::ui.warehouses.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>{{ trans('warehouses::ui.warehouses.code') }}</th>
                                        <th>{{ trans('warehouses::ui.warehouses.name') }}</th>
                                        <th>{{ trans('warehouses::ui.warehouses.zone') }}</th>
                                        <th>{{ trans('warehouses::ui.warehouses.status') }}</th>

                                        @if(Auth::user()->can(['update-warehouses', 'delete-warehouses']))
                                            <th>{{ trans('warehouses::ui.warehouses.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($warehouses as $ware)
                                        <tr>
                                            <td>{{ $ware->code }}</td>
                                            <td>{{ $ware->name }}</td>
                                            <td><span class="badge badge-primary">
                                                    {{ $ware->zones->name }}
                                                </span>
                                            </td>
                                            <td>
                                                @if($ware->status == 1)
                                                    <span class="badge badge-success">
                                                    {{ "Activo" }}
                                                    </span>
                                                    @endif
                                                @if($ware->status == 0)
                                                        <span class="badge badge-danger">
                                                    {{ "Desactivado" }}
                                                    </span>
                                                    @endif
                                            </td>

                                            @if(Auth::user()->can(['update-warehouses', 'delete-warehouses']))
                                                <td>
                                                    @if(Auth::user()->can('update-warehouses'))
                                                        <a href="{{ url('warehouses/' . $ware->id . '/edit') }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif

                                                    @if(Auth::user()->can('delete-warehouses'))
                                                        <a href="#" data-url="{{ url('auth/user/'.$ware->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                {!! $warehouses->render() !!}

                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    @stop