<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 04/03/2018
 * Time: 16:27
 */
?>
@extends('layouts.master')

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div class="pull-right">
                    <h6>OT: {{ $ot_data->code_ot }}</h6>
                    <button id="ver-ayuda" class="btn btn-primary btn-circle btn-sm" type="button" data-toggle="modal" data-trigger="hover" data-target="#modal-ayuda">
                        <i class="fa fa-question" ></i>
                    </button>
                </div>
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Agregar Materiales para despacho.</h6>
                    <p class="inline-block">
                        <span class="txt-primary mr-5 weight-400">El cliente @php $cant_inv > 0 ? $mensaje = 'cuenta' : $mensaje = 'no cuenta'; print $mensaje; @endphp con inventario propio. </span>
                    </p>
                    <p>
                        <span class="txt-primary mr-5 weight-400">Los materiales se tomaran del </span><span class="label label-primary"> Inventario de {{ $nombre_inv }}</span>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                            <div class="form-wrap">
                                <div class="well">
                                    <div class="row">
                                        {!! Form::open(['route'=>'ot.save_dispatch' ]) !!}
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col" style="width: 10px;">#</th>
                                                <th scope="col" style="width: 100px;">Código</th>
                                                <th scope="col" style="width: 300px;">Descripción</th>
                                                <th scope="col" style="width: 30px;">Cantidad</th>
                                                <th scope="col" style="width: 30px;">Cant. disponible</th>
                                                <th scope="col">Categoría</th>
                                                <th scope="col">Inventario</th>
                                                <th></th>
                                                <input type="hidden" value="{{ $cliente_id }}" id="id_clients" name="id_clients">
                                            </tr>
                                            </thead>
                                            <tbody id="body-items">
                                            <tr id="row-inv-1">
                                                <td>1</td>
                                                <td>
                                                    <div class="col-md-3">
                                                        <div class="input-group my-group">
                                                            <select id="code1" name="id[]" class="form-control form-control-sm selectpicker code" data-width="120px" data-style=" btn-default btn-outline" data-live-search="true">
                                                                <option value="">Inserte</option>
                                                                @foreach($inventario as $ti)
                                                                    <option value="{{ $ti->id }}">{{$ti->code_sku}} {{$ti->description}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="input-group-btn">
                                                            <button id="presta1" class="btn btn-primary btn-xs prestamo" type="button" data-toggle="popover" data-trigger="hover" data-content="Tomar material prestado de otro inventario">
                                                                <i class="fa fa-exchange" ></i>
                                                            </button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="description1"></span></p>
                                                </td>
                                                <td>
                                                    <div class="form-group text-center" id="form-group-cantidad1">
                                                        <input id="cantidad1" class="form-control form-control-sm cantidad" type="number"  value="" name="cantidad[]">
                                                        <span id="medida1"></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="disp1"></span></p>
                                                </td>
                                                <td>
                                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="categoria1"></span></p>
                                                </td>
                                                <td>
                                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="inventario1"></span></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="col-md-12 col-lg-12 col-sm-12 text-center">
                                            <button class="btn btn-primary" type="button" id="add-material"><i class="fa fa-plus"></i></button>
                                        </div>
                                        <div class="col-md-8 col-sm-12 col-xs-12 col-lg-8">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Observación:</label>
                                                <textarea name="observacion" class="form-control" rows="2"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Entregar a</label>
                                                <input type="text" class="form-control" name="entregar_a" />
                                            </div>
                                        </div>
                                        <input type="hidden" value="{{ $ot_data->id }}" name="ot_id">
                                        <div class="row text-center">
                                            <div class="col-md-12 col-sm-12 col-lg-12">
                                                <button type="submit" class="btn btn-success" id="save_dispatch">Guardar para despacho</button>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    var get_inventary = '{{ route('ot.get_inventario',$ot_data->clients_id) }}';
                                </script>
                            </div>
                        </div>
                        <!--end COL-MD-12 -->
                    </div>
                    <!-- end ROW -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de pedir pretado a otro inventario -->
    <div class="modal fade" id="modalinv" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label mb-10" for="inv_prestamo">Seleccione otro inventario: </label>
                        <select class="form-control" name="inventario_id" id="inventario_id">
                            <option value="">Seleccione... </option>
                            @foreach($clientsbodega as $z)
                                <option value="{{ $z->id }}">{{ $z->shortname }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="bodega_id" name="bodega_id" value="{{ $ot_data->w_id }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary" id="add-prestamo">Continuar</button>
                    </div>
                    <div class="colmd-6">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de ayuda -->
    <div class="modal fade" id="modal-ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Para agregar un material al despacho haz lo siguiente:</h5>
                        </div>
                    </div>
                    <div class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <ol>
                                    <li>Haz click en el campo de código: </li>
                                    <img src="{{asset('assets/images/ayuda-despacho/1.png')}}">
                                    <li>Presiona el boton de tab para ir al campo cantidad</li>
                                    <li>Agrega la cantidad y presiona enter</li>
                                    <li>Automaticamente se agregará otro campo de producto.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary" id="add-prestamo">Ok</button>
                    </div>
                    <div class="colmd-6">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('inventariounico::layouts.info')
@stop

@section('script')
    @include('ot::layouts.script-ot')
@endsection
