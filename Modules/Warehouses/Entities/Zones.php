<?php

namespace Modules\Warehouses\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zones extends Model
{

    use SoftDeletes;

    protected $fillable = ['name', 'address', 'provinces_id', 'status', 'created_at', 'updated_at'];

    protected $dates = ['deleted_at'];


    public function provinces(){

        return $this->belongsTo('Modules\Warehouses\Entities\Provinces', 'provinces_id');

    }

    public function warehouses(){

        return $this->hasOne('Modules\Warehouses\Entities\Warehouses', 'zones_id');

    }

}
