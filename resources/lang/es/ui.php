<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 02/06/18
 * Time: 05:13 PM
 */

return [
    'title'             =>  'Sergeco',

    'option_user'       => [
        'title'   => 'Menu Usuario',
        'data' =>[
            'profile'         =>  [
                'title'     => 'Perfil',
                'icon'     => 'fa fa-user',
                'url'     => 'auth.profile',

            ],
            'logout'         =>  [
                'title'     => 'Salir',
                'icon'     => 'fa fa-sign-out ',
                'url'     => 'auth.logout',
            ]
        ]
    ],

    'sidebar'       => [
        'title'   => 'Sidebar',
        'dashboard'         =>  [
            'title'     => 'Dashboard',
            'icon'     => 'fa fa-delicious',
            'url'     => 'dashboard',
        ],
        'data' => [
			'Main' => [
				'dashboard'         =>  [
					'title'     => 'Dashboard',
					'icon'     => 'fa fa-home',
					'url'     => 'dashboard',
					'permission'     => true,
				],

                'clients'         =>  [
                    'title'     => 'Clientes',
                    'icon'     => 'fa fa-users',
                    'url'     => 'clients.index',
                    'permission'     => 'read-clients',
                ],

                'warehouses'         =>  [
                    'title'     => 'Almacén',
                    'icon'     => 'fa fa-building',
                    'url'     => 'warehouses.index',
                    'permission'     => 'read-warehouses',
                ],

                'zones'         =>  [
                    'title'     => 'Zonas',
                    'icon'     => 'fa fa-location-arrow',
                    'url'     => 'zones.index',
                    'permission'     => 'read-zones',
                ],
			],


            'Inventarios' => [

                'general'         =>  [
                    'title'     => 'General',
                    'icon'     => 'fa fa-shopping-basket',
                    'url'     => 'all.index',
                    'permission'     => 'read-inventory',
                ],
                'items'         =>  [
                    'title'     => 'Artículos',
                    'icon'     => 'fa fa-product-hunt',
                    'url'     => 'items.index',
                    'permission'     => 'read-clients',
                ],
                'types'         =>  [
                    'title'     => 'Tipos',
                    'icon'     => 'fa fa-retweet',
                    'url'     => 'types.index',
                    'permission'     => 'read-inventory',
                ],
                'ingress'         =>  [
                    'title'     => 'Ingresos',
                    'icon'     => 'fa fa-arrow-right',
                    'url'     => 'inventory.ingress',
                    'permission'     => 'read-income',
                ],
                'discard'         =>  [
                    'title'     => 'Descarte',
                    'icon'     => 'fa fa-arrow-left',
                    'url'     => 'inventory.discard',
                    'permission'     => 'read-discard',
                ]

            ],

            'Proyectos' => [
                'items'         =>  [
                    'title'     => 'Ot\'s',
                    'icon'     => 'fa fa-briefcase',
                    'url'     => 'ot.index',
                    'permission'     => 'read-ot',
                ],
                'Prestamos' => [
                    'title' => 'Prestamos',
                    'icon'     => 'fa fa-exchange',
                    'url'     => 'prestamos.index',
                    'permission'     => 'read-loan',
                ],
                'Despachos' => [
                    'title' => 'Despachos',
                    'icon' => 'fa fa-clipboard',
                    'url'     => 'dispatch.index',
                    'permission'     => 'read-dispatch',
                    'submenu' => [
                        'Crear' => [
                            'title' => 'Generar',
                            'url' => 'dispatch.create'
                        ],
                        'Mostrar' => [
                            'title' => 'Ver Tabla',
                            'url' => 'dispatch.index'
                        ]
                    ]
                ],
                'Devoluciones' => [
                    'title' => 'Devoluciones',
                    'icon' => 'fa fa-angle-right',
                    'url'     => 'returns.index',
                    'permission'     => 'read-return',
                ],
            ]

        ]

    ],

    'menu_action'       => [
        'title'   => 'Administrar Usuarios',
        'data' =>[
            'users'         =>  [
                'title'     => 'Usuarios',
                'icon'     => 'fa fa-users',
                'url'     => 'auth/user',
            ],
            'roles'         =>  [
                'title'     => 'Roles',
                'icon'     => 'fa fa-user',
                'url'     => 'auth/role',
            ],
            'permissions'         =>  [
                'title'     => 'Permisos',
                'icon'     => 'fa fa-key',
                'url'     => 'auth/permission',
            ]
        ]
    ]
];