<?php

namespace Modules\Inventory\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Entities\Role;
use Illuminate\Support\Facades\Session;
use Modules\Clients\Entities\Clients;
use Modules\Inventory\Entities\Ingress;
use Modules\Inventory\Entities\Ingressitems;
use Modules\Inventory\Entities\Inventory;
use Modules\Inventory\Entities\Inventoryitems;
use Modules\Inventory\Http\Requests\IngressRequest;
use Modules\Inventory\Transformers\IngressResource;
use Modules\Inventory\Transformers\InventoryitemsResource;


class IngressController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        if (Auth::user()->can('read-income')){

            $ingress = Ingress::orderBy('created_at', 'desc')
                ->with('warehouses', 'clients', 'users', 'ingressitems')
                ->paginate(5);

            return IngressResource::collection($ingress);

        }

        return $this->response_error('No autorizado para esta vista.');
    }

    public function viewIngress(){

        if (Auth::user()->can('read-income')){

            if(Auth::user()->hasRole('admin')
                || Auth::user()->hasRole('supervisor')
                || Auth::user()->hasRole('inspector'))
            {

                $users = Role::where('name', 'warehouseman')
                    ->orWhere('name', '=', 'warehouse-chief')->with('users')->get();

            }else if (Auth::user()->hasRole('warehouse-chief')){
                $users = Role::where('name', 'warehouseman')
                    ->with('users')
                    ->first();
            }else{

                $users = Auth::user();
            }

            $clients = Clients::all()->sortBy("shortname");

            return view('inventory::ingress.index', compact('users', 'clients'));

        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        if (Auth::user()->can('create-income')){

            if(Auth::user()->hasRole('admin')
                || Auth::user()->hasRole('supervisor')
                || Auth::user()->hasRole('inspector'))
            {

                $users = Role::where('name', 'warehouseman')
                    ->orWhere('name', '=', 'warehouse-chief')->with('users')->get();

            }else if (Auth::user()->hasRole('warehouse-chief')){
                $users = Role::where('name', 'warehouseman')
                    ->with('users')
                    ->first();
            }else{

                $users = Auth::user();
            }

            $clients = Clients::all()->sortBy("shortname");

            return view('inventory::ingress.create', compact('users', 'clients'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(IngressRequest $request)
    {

        if(Auth::user()->can('create-income')){

            $data = Ingress::create([
                'clients_id' => $request->clients,
                'warehouses_id' => $request->warehouses_clients,
                'no_document' => $request->no_document,
                'provider' => $request->provider,
                'warehouses_provider' => $request->warehouses_provider,
                'deliver_to' => $request->deliver_to,
                'no_order' => $request->no_order,
                'no_transfer' => $request->no_transfer,
                'users_id' => $request->receiver_to,
                'observations' => $request->observations,
                'status' => $request->status,
            ]);

            $ingress = Ingress::findOrFail($data->id);

            if ($ingress->id){

                $items = $request->items;

                $arrayItems = [];

                for($m = 0; $m < count($items); $m++){

                    $dataItem = Ingressitems::create([
                        'inventoryitems_id' => $items[$m]['items']['id'],
                        'ingress_id' => $ingress->id,
                        'quantity' => $items[$m]['quantity'],
                        'observations' => $items[$m]['observations']
                    ]);

                    array_push($arrayItems, $dataItem->id);
                }

                if (count($arrayItems) > 0){

                    return json_encode(['status' => 200, 'message' => 'Se proceso correctamente los datos.', 'id' => $data->id]);
                }


                Ingress::destroy($data->id);

                return $this->response_error('Error al almacenar los Items del ingreso. Intente nuevamente');


            }else{

                return $this->response_error('Error al almacenar los datos de ingresos. Intente nuevamente.');

            }

        }

        return $this->response_error('No esta autorizado para crear ingresos.');
    }


    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(IngressRequest $request, $id)
    {
        if (Auth::user()->can('update-income')){

            $ingress = Ingress::find($id);

            $ingress->update([
               'clients_id' => $request->clients,
               'warehouses_id' => $request->warehouses_clients,
               'no_document' => $request->no_document,
               'provider' => $request->provider,
               'warehouses_provider' => $request->warehouses_provider,
               'deliver_to' => $request->deliver_to,
               'no_order' => $request->no_order,
               'no_transfer' => $request->no_transfer,
               'users_id' => $request->receiver_to,
               'observations' => $request->observations
           ]);

           if (count($ingress) > 0){

               $items = $request->items;

               for($m = 0; $m < count($items); $m++){

                   $itemsIngress = Ingressitems::find($items[$m]['id']);

                   if (count($itemsIngress) > 0){

                       $itemsIngress->update([
                           'inventoryitems_id' => $items[$m]['items']['id'],
                           'quantity' => $items[$m]['quantity'],
                           'observations' => $items[$m]['observations']
                       ]);
                   }

                   if (count($itemsIngress) < 1){

                       Ingressitems::create([
                           'inventoryitems_id' => $items[$m]['items']['id'],
                           'ingress_id' => $id,
                           'quantity' => $items[$m]['quantity'],
                           'observations' => $items[$m]['observations']
                       ]);
                   }

               }

               return json_encode(['status' => 200, 'message' => 'Se actualizo el registro correctamente.']);

           }

            return $this->response_error('No se encontro ningún registro con el ID seleccionado.');

        }

        return $this->response_error('No tiene acceso para actualizar este registro.');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->can('delete-income')){

           Ingress::destroy($id);

           return json_encode([
               'status' => 200,
               'message' => 'Registro eliminado correctamente.'
           ]);
        }

        return $this->response_error('No esta autorizado para eliminar.');
    }

    public function ware($id){

        if (Auth::user()->can('read-income') && Auth::user()->can('create-income')){

            $ware_client = Clients::find($id)->warehouses;

            return $ware_client;
        }

        return $this->response_error('No esta autorizado para listar los almacenes.');
    }

    public function items(){

        if (Auth::user()->can('read-income') && Auth::user()->can('create-income')){

            $items = Inventoryitems::orderBy('code_sku', 'desc')->where('status', '=', 1)->with('inventorytypes')->get();

            return InventoryitemsResource::collection($items);
        }

        return $this->response_error('No esta autorizado para listar los items.');

    }


    public function createPDF($data, $view_url, $type){

        if (Auth::user()->can('read-income')){

            $date = Carbon::now();

            $title = 'ingreso_#doc'.$data->no_document.'_'.$date;

            $view = \View::make($view_url, compact('data', 'date', 'title'))->render();

            $pdf = \App::make('dompdf.wrapper');

            $pdf->loadHTML($view);

            if ($type == 1){

                if ($data->status == 0){

                    $ingress = Ingress::find($data->id);

                    if (count($ingress) > 0){

                        $items = $data->ingressitems;

                        for ($k = 0; $k < count($items); $k++){

                            $inventory = Inventory::where('inventoryitems_id', $items[$k]['inventoryitems_id'])->first();

                            if (count($inventory) > 0){

                                $quantity = ($inventory->quantity) + ($items[$k]['quantity']);

                                $inventory->quantity = $quantity;

                                $inventory->save();

                                $ingress->status = true;

                                $ingress->save();

                                if (count($inventory) < 1){

                                    $ingress->status = false;
                                    $ingress->save();

                                }


                            }elseif (count($inventory) < 1){

                                $pushItems = Inventory::create([
                                    'inventoryitems_id' => $items[$k]['inventoryitems_id'],
                                    'clients_id' => $data['clients']['id'],
                                    'warehouses_id' => $data['warehouses']['id'],
                                    'quantity' => $items[$k]['quantity'],
                                    'status' => true
                                ]);


                                if (count($pushItems) < 1){
                                    $ingress->status = false;
                                    $ingress->save();
                                }

                                $ingress->status = true;
                                $ingress->save();
                            }

                        }

                        return $pdf->stream('original_'.$title.'.pdf');

                    }

                    return $this->response_error('Error al crear el formulario de ingreso. Intente nuevamente');

                }

                return $pdf->stream($title.'.pdf');

            }

            if ($type == 2){

                return $pdf->download($title.'.pdf');

            }

        }

        return $this->response_error('No tiene acceso para generar el documento.');

    }


    public function generateIngress($type, $id){

        if (Auth::user()->can('read-income')){

            $ingress = Ingress::with('warehouses', 'clients', 'users', 'ingressitems')->find($id);


            if (count($ingress) > 0){

                $view_url = 'inventory::ingress.print';

                $data = new IngressResource($ingress);

                return $this->createPDF($data, $view_url, $type);
            }


            return $this->response_error('El ID seleccionado no se encuentra en la base de datos.');

        }

        return $this->response_error('No tiene acceso para generar el documento.');
    }


    public function response_error($message){
        return json_encode(
            [
                'status' => 400,
                'message' => $message
            ]
        );
    }

    public function notAuthenticate(){
        Session::flash('message_error', trans('auth::ui.user.message_not'));
    }

}
