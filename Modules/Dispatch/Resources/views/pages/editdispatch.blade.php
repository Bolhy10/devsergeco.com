<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 20/03/2018
 * Time: 1:17
 */
?>
@extends('layouts.master2')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Editar Item</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">Código</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Cantidad Solicitada</th>
                                <th scope="col">Cant. disponible</th>
                                <th scope="col">Categoría</th>
                                <th scope="col">Inventario</th>
                            </tr>
                            </thead>
                            {!! Form::open(['route'=>'statusitem.update']) !!}
                            <tbody id="body-items">
                            @foreach($item as $d_d)
                                <tr>
                                    <td>{{$d_d->code_sku}}</td>
                                    <input type="hidden" name="id" value="{{ $d_d->sti_id }}">
                                    <td>{{ $d_d->description}}</td>
                                    <td>
                                        <input type="number" name="cantidad" value="{{ $d_d->sti_qty }}">
                                    </td>
                                    <td>{{ $d_d->inv_qty }}</td>
                                    <td>{{ $d_d->type }}</td>
                                    <td>{{ $d_d->fullname }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success btn-anim pull-right">GUARDAR</button>
                        </div>
                        {!! Form::close() !!}
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <a class="btn btn-danger btn-anim pull-left" href="{{ URL::previous() }}"> CANCELAR </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
