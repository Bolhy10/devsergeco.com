<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventorytypes extends Model
{
    use SoftDeletes;

    protected $table = 'inventorytypes';

    protected $fillable = ['type', 'status'];

    public function inventoryitems(){

        return $this->hasMany('Modules\Inventory\Entities\Inventoryitems');

    }

}
