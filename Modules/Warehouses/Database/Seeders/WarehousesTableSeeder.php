<?php

namespace Modules\Warehouses\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Warehouses\Entities\Warehouses;

class WarehousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $warehouses = [
            [
                'code' => 'WPA08',
                'name' => 'Bodega de Panamá',
                'zones_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1
            ],
            [
                'code' => 'WCO03',
                'name' => 'Bodega de Colón',
                'zones_id' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1
            ],
        ];

        foreach ($warehouses as $key=>$value){
            Warehouses::create($value);
        }

        for($i = 1; $i < 8; $i++){
            DB::table('warehouses_clients')->insert(array(
                'warehouses_id' => 1,
                'clients_id' => $i
            ));
        }

    }

}
