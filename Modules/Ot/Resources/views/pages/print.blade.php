<?php
/**
 * Created by PhpStorm.
 * User: yvargas
 * Date: 04/04/2018
 * Time: 10:26 AM
 */
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>{{ $titulo }} de Materiales</title>
    {!! Html::style('assets/css/bootstrap.min.css') !!}
</head>
<body>


<style>

    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }

    a {
        color: #5D6975;
        text-decoration: underline;
    }


    header {
        padding: 10px 0;
        margin-bottom: 30px;
    }

    #logo {
        text-align: left;
        margin-bottom: 10px;
    }

    #logo img {
        width: 250px;
    }

    .titulo {
        font-size: 12px;
        font-weight: Arial;
        text-align: center;
        text-transform: uppercase;
    }

    table {
        width: 100%;
        font-size: 9px;
    }

    table th,
    table td {
        text-align: center;
    }

   

    

    table td {
        text-align: center;
    }

    #notices .notice {
        bottom: 0;
        color: #5D6975;
        font-size: 1.0em;
    }

    footer {
        color: black;
        width: 100%;
        height: 75px;
        position: absolute;
        bottom: 0;
        border-top: 1px solid #C1CED9;
        padding: 8px 0;
        text-align: center;
    }

</style>


    <div class='container'>
        <!-- row -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="logo">
                    <img src="{{ asset('assets/images/elements/logo.png') }}">
                </div>
            </div>
        </div>
        <!-- end row -->
       

        <!-- row -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <p class="titulo">{{ $titulo }} de Material - Bodega <span class="pull-right text-danger">{{ strtoupper($alias)}} N° {{ $proceso->id }}</span></p>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-6 col-xs-6 col-lg-6">
                    <table class="table-borderless">
                            <thead></thead>
                            <tbody>
                            <tr>
                                <td style="width:15%;text-align:right;">OT N°:</td>
                                <td style="width:3%;"></td>
                                <td style="width:82%;text-align:left;">{{ $ot->code_ot }}</td>
                            </tr>
                            <tr>
                                <td style="width:15%;text-align:right;">PROYECTO:</td>
                                <td style="width:3%;"></td>
                                <td style="width:82%;text-align:left;">{{ $ot->project_name }}</td>
                            </tr>
                            <tr>
                                <td style="width:15%;text-align:right;">LUGAR:</td>
                                <td style="width:3%;"></td>
                                <td style="width:82%;text-align:left;">{{ $ot->place }}</td>
                            </tr>
                            <tr>
                                <td style="width:15%;text-align:right;">AREA:</td>
                                <td style="width:3%;"></td>
                                <td style="width:82%;text-align:left;">{{ $ot->zone }}</td>
                            </tr>
                            @foreach($users as $uo)
                                <tr>
                                    <td style="width:15%;text-align:right;">{{ strtoupper($uo->display_name) }}:</td>
                                    <td style="width:3%;"></td>
                                    <td style="width:82%;text-align:left;">{{ $uo->firstname }} {{ $uo->lastname }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                    </table>
            </div>
            <div class="col-md-6 col-xs-6 col-lg-6 pull-right">
                    <table class="table-borderless">
                        <thead></thead>
                            <tbody>
                                    <tr>
                                        <td style="width:77%;text-align:right;">FECHA:</td>
                                        <td style="width:3%;"></td>
                                    <td style="width:20%;text-align:left;">{{ $hoy }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width:77%;text-align:right;">INVENTARIO</td>
                                        <td style="width:3%;"></td>
                                        <td style="width:20%;text-align:left;">{{ $name_inventario }}</td>
                                    </tr>
                            </tbody>
                    </table>
            </div>
        </div>

        @if($no_impresion > 0)
            <div class="row text-center">
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                    <p style="font-size:10px;" class="text-danger">-COPIA DEL ORIGINAL-</p>
                </div>
            </div>
        @endif

        <!-- div entregar a -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p style="font-size:9px;text-align:left;">ENTREGAR A: {{ $proceso->entregar }}</p>
            </div>
        </div>

        <!-- space -->
        <div style="height:3%;"> </div>
        <!-- end space -->

        <!-- table start -->
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th>IT</th>
                    <th>CODIGO</th>
                    <th>DESCRIPCION DEL MATERIAL</th>
                    <th>TIPO</th>
                    <th>QTY</th>
                </tr>
                </thead>
                <tbody>
                @php $i = 1 @endphp
                @foreach($data_proceso as $data)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $data->code_sku }}</td>
                        <td>{{ $data->description }}</td>
                        <td>{{ $data->type }}</td>
                        <td>{{ $data->sti_qty }}</td>
                    </tr>
                    @php $i ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- end table -->
    </div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                <div style="text-align:left;"><p style="font-size:9px">OBSERVACIONES:</p></div>
                @if($proceso->observacion == null)
                    <div style="height: 40px;"></div>
                @else
                    <p style="font-size:9px">{{ $proceso->observacion }}</p>
                @endif
            </div>

        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                <p style="font-size:9px">Recibido por: ..............................</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                <p style="font-size:9px">Firma: ............................. </p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                <p style="font-size:9px">Fecha: .............................</p>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
