<div class="col-lg-4 col-xs-12">
    <div class="panel panel-default card-view">

        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">{{ trans('inventory::ui.discard.create.subtitle') }}</h6>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">

                        <div class="form-wrap">
                            @if(Auth::user()->hasRole('warehouseman'))
                                <div class="form-group" :class="{'has-error': errors.has('delivered')}">
                                    <label class="control-label">{{ trans('inventory::ui.discard.delivered') }}</label>
                                    <select name="delivered"
                                            v-model="formDiscard.delivered"
                                            v-validate="'required'"
                                            class="form-control">
                                        <option :value="'{{ $users->id  }}'">{{ $users->username }}</option>
                                    </select>
                                </div>

                            @endif

                            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor') || Auth::user()->hasRole('inspector') || Auth::user()->hasRole('warehouse-chief'))
                                <div class="form-group" :class="{'has-error': errors.has('delivered')}">
                                    <label class="control-label">{{ trans('inventory::ui.discard.delivered') }}</label>
                                    <select name="delivered"
                                            v-model="formDiscard.delivered"
                                            v-validate="'required'"
                                            class="form-control">
                                        @foreach($users as $rol)
                                            @foreach($rol->users as $user)
                                                <option :value="'{{ $user->id }}'">{{ $user->username }} - {{ $rol->display_name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>

                                </div>
                            @endif


                            <h6 class="txt-dark capitalize-font">
                                <i class="fa fa-arrow-right"></i>
                                {{ trans('inventory::ui.discard.create.hr') }}
                            </h6>
                            <hr class="light-grey-hr">


                            <div class="form-group" :class="{'has-error': errors.has('deliver_to')}">
                                <label class="control-label">{{ trans('inventory::ui.discard.receive_to') }}</label>
                                <input type="text"
                                       name="deliver_to"
                                       v-model="formDiscard.receive_to"
                                       v-validate="'required'"
                                       class="form-control"
                                       placeholder="{{ trans('inventory::ui.discard.receive_to') }}">
                            </div>

                            <div class="form-group" :class="{'has-error': errors.has('no_document')}">
                                <label class="control-label">{{ trans('inventory::ui.discard.no_document') }}</label>
                                <input type="text"
                                       name="no_document"
                                       v-model="formDiscard.no_document"
                                       v-validate="'required'"
                                       class="form-control"
                                       placeholder="{{ trans('inventory::ui.discard.no_document') }}">
                            </div>

                            <div class="form-group" :class="{'has-error': errors.has('observations')}">
                                <label class="control-label">{{ trans('inventory::ui.discard.observations') }}</label>
                                <textarea
                                        name="observations"
                                        v-model="formDiscard.observations"
                                        class="form-control"
                                        placeholder="{{ trans('inventory::ui.discard.observations') }}"></textarea>
                            </div>


                            <h6 class="txt-dark capitalize-font">
                                <i class="fa fa-product-hunt"></i>
                                {{ trans('inventory::ui.discard.create.title_inv') }}
                            </h6>

                            <hr class="light-grey-hr">

                            <div class="form-group" :class="{'has-error': errors.has('clients')}">
                                <label class="control-label">{{ trans('inventory::ui.discard.clients') }}</label>
                                <select class="form-control" @change="getWare(formDiscard.clients)"
                                        name="clients"
                                        v-model="formDiscard.clients"
                                        v-validate="'required'"
                                        >
                                    @foreach($clients as $client)
                                        <option :value="'{{ $client->id }}'">{{ $client->shortname }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group" :class="{'has-error': errors.has('warehouses_clients')}">
                                <label class="control-label">{{ trans('inventory::ui.discard.warehouse_client') }}</label>
                                <select name="warehouses_clients" @change="getItems(formDiscard.clients, formDiscard.warehouses_clients)"
                                        v-model="formDiscard.warehouses_clients"
                                        v-validate="'required'"
                                        class="form-control">
                                    <option value="">Escoja una opción</option>
                                    <option v-for="ware in warehouses" :key="ware.id" :value="ware.id">@{{ ware.name }}</option>
                                </select>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>