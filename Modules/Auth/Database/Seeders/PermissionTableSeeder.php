<?php

namespace Modules\Auth\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            [
                'name' => 'create-users',
                'display_name' => 'Create Users',
                'description' => 'Create users',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'read-users',
                'display_name' => 'Read Users',
                'description' => 'List Users',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-users',
                'display_name' => 'Update Users',
                'description' => 'Update Users',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-users',
                'display_name' => 'Delete Users',
                'description' => 'Delete Users',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'create-roles',
                'display_name' => 'Create Roles',
                'description' => 'Create Roles',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-roles',
                'display_name' => 'Read Roles',
                'description' => 'List Roles',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-roles',
                'display_name' => 'Update Roles',
                'description' => 'Update Roles',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-roles',
                'display_name' => 'Delete Roles',
                'description' => 'Delete Roles',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'create-permissions',
                'display_name' => 'Create Permissions',
                'description' => 'Create Permissions',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-permissions',
                'display_name' => 'Read Permissions',
                'description' => 'List Permissions',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-permissions',
                'display_name' => 'Update Permissions',
                'description' => 'Update Permissions',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-permissions',
                'display_name' => 'Delete Permissions',
                'description' => 'Delete Permissions',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


            [
                'name' => 'create-clients',
                'display_name' => 'Create Clients',
                'description' => 'Crear nuevos clientes',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-clients',
                'display_name' => 'Read Clients',
                'description' => 'Lista de todos los clientes',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-clients',
                'display_name' => 'Update Permissions',
                'description' => 'Update Permissions',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-clients',
                'display_name' => 'Delete Clients',
                'description' => 'Eliminar clientes del sistema',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'create-warehouses',
                'display_name' => 'Create Warehouses',
                'description' => 'Crear Almacenes',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-warehouses',
                'display_name' => 'Read warehouses',
                'description' => 'Lista de Almacenes',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-warehouses',
                'display_name' => 'Update Warehouses',
                'description' => 'Actualizar almacenes',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-warehouses',
                'display_name' => 'Delete Warehouses',
                'description' => 'Eliminar almacenes',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


            [
                'name' => 'create-zones',
                'display_name' => 'Create Zones',
                'description' => 'Crear zonas',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-zones',
                'display_name' => 'Read Zones',
                'description' => 'Listar zonas',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-zones',
                'display_name' => 'Update Zones',
                'description' => 'Actualizar zonas',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-zones',
                'display_name' => 'Delete Zones',
                'description' => 'Eliminar zonas',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'create-inventory',
                'display_name' => 'Create Inventory',
                'description' => 'Crear inventario al sistema',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-inventory',
                'display_name' => 'Read Inventory',
                'description' => 'Lista de inventarios',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-inventory',
                'display_name' => 'Update Inventory',
                'description' => 'Actualizar inventario',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-inventory',
                'display_name' => 'Delete Inventory',
                'description' => 'Eliminar inventario del sistema',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'create-income',
                'display_name' => 'Create Income',
                'description' => 'Crear Ingresos al sistema',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'read-income',
                'display_name' => 'Read Income',
                'description' => 'Lista de Ingresos',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-income',
                'display_name' => 'Update Income',
                'description' => 'Actualizar datos de Ingresos',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-income',
                'display_name' => 'Delete Income',
                'description' => 'Eliminar ingresos',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'create-discard',
                'display_name' => 'Create Discard',
                'description' => 'Crear descarte al sistema',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'read-discard',
                'display_name' => 'Read Discard',
                'description' => 'Lista de Descarte',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-discard',
                'display_name' => 'Update Discard',
                'description' => 'Actualizar datos de Descarte',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-discard',
                'display_name' => 'Delete Discard',
                'description' => 'Eliminar descarte',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-dispatch',
                'display_name' => 'Read Dispatch',
                'description' => 'Leer Despacho',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'create-dispatch',
                'display_name' => 'Create Dispatch',
                'description' => 'Crear Despacho',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-dispatch',
                'display_name' => 'Update Dispatch',
                'description' => 'Actualizar Despacho',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-dispatch',
                'display_name' => 'Delete Dispatch',
                'description' => 'Eliminar Despacho',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-return',
                'display_name' => 'Read Return',
                'description' => 'Leer Devolución',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'create-return',
                'display_name' => 'Read Return',
                'description' => 'Leer Devolución',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-return',
                'display_name' => 'Update-return',
                'description' => 'Actualizar devolución',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-return',
                'display_name' => 'Delete Return',
                'description' => 'Eliminar devolución',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'view-ot',
                'display_name' => 'View Ot',
                'description' => 'Ver Ot',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-ot',
                'display_name' => 'Read Ot',
                'description' => 'Leer Ot',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'create-ot',
                'display_name' => 'Create Ot',
                'description' => 'Crear Ot',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'update-ot',
                'display_name' => 'Update Ot',
                'description' => 'Actualizar Ot',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'delete-ot',
                'display_name' => 'Delete Ot',
                'description' => 'Borrar Ot',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'read-loan',   //leer prestamos
                'display-name' => 'Read Loan',
                'description' => 'Leer Prestamos',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        foreach ($permission as $key=>$value){
            Permission::create($value);
        }
    }
}
