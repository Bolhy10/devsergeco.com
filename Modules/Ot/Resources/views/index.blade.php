@extends('layouts.master2')

@section('content')
    @section('pageTitle',$titulo)
            <!-- Row -->
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Status</h6>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="pull-left inline-block refresh">
                                    <i class="fa fa-line-chart"></i>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body row">
                                <div class="">
                                    <div class="pl-15 pr-15 mb-15">
                                        <div class="pull-left">
                                            <i class="zmdi zmdi-collection-folder-image inline-block mr-10 font-16"></i>
                                            <span class="inline-block txt-dark">Ot's iniciadas</span>
                                        </div>
                                        <span class="inline-block txt-warning pull-right weight-500">{{ $ot_i }}</span>
                                        <div class="clearfix"></div>
                                    </div>
                                    <hr class="light-grey-hr mt-0 mb-15"/>
                                    <div class="pl-15 pr-15 mb-15">
                                        <div class="pull-left">
                                            <i class="zmdi zmdi-format-list-bulleted inline-block mr-10 font-16"></i>
                                            <span class="inline-block txt-dark">Ot's Pausadas</span>
                                        </div>
                                        <span class="inline-block txt-danger pull-right weight-500">{{ $ot_p }}</span>
                                        <div class="clearfix"></div>
                                    </div>
                                    <hr class="light-grey-hr mt-0 mb-15"/>
                                    <div class="pl-15 pr-15 mb-15">
                                        <div class="pull-left">
                                            <i class="zmdi zmdi-format-list-bulleted inline-block mr-10 font-16"></i>
                                            <span class="inline-block txt-dark">Ot's eliminadas</span>
                                        </div>
                                        <span class="inline-block txt-danger pull-right weight-500">{{ $ot_e }}</span>
                                        <div class="clearfix"></div>
                                    </div>
                                    <hr class="light-grey-hr mt-0 mb-15"/>
                                    <div class="pl-15 pr-15 mb-15">
                                        <div class="pull-left">
                                            <i class="zmdi zmdi-format-list-bulleted inline-block mr-10 font-16"></i>
                                            <span class="inline-block txt-dark">Ot's Cerradas</span>
                                        </div>
                                        <span class="inline-block txt-danger pull-right weight-500">{{ $ot_c }}</span>
                                        <div class="clearfix"></div>
                                    </div>
                                    <hr class="light-grey-hr mt-0 mb-15"/>
                                    <div class="pl-15 pr-15 mb-15">
                                        <div class="pull-left">
                                            <i class="zmdi zmdi-ticket-star inline-block mr-10 font-16"></i>
                                            <span class="inline-block txt-dark">Total de Ot's creadas</span>
                                        </div>
                                        <span class="inline-block txt-primary pull-right weight-500">{{ $ot_tot }}</span>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view panel-refresh">
                        <div class="refresh-container">
                            <div class="la-anim-1"></div>
                        </div>
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Crear orden de trabajo</h6>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="pull-left inline-block refresh">
                                    <i class="fa fa-briefcase"></i>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body text-center">
                                <div style="height: 140px;">
                                    <div class="btn-group mt-30 mr-10">
                                        <!--a href="ot/crear-ot" class="btn btn-primary btn-icon-anim">Crear Ot <i class="fa fa-plus"></i></a-->
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#modalzone">CREAR OT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view panel-refresh">
                        <div class="refresh-container">
                            <div class="la-anim-1"></div>
                        </div>
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark"></h6>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="pull-left inline-block refresh">
                                    <i class="fa fa-building"></i>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body text-center">
                                <div style="height: 140px;">
                                    <!--div class="btn-group mt-30 mr-10">
                                        <button class="btn btn-primary btn-icon-anim">Agregar Bodega <i class="fa fa-plus"></i></button>
                                        <button class="btn btn-default btn-icon-anim">Ver Bodega <i class="fa fa-eye"></i></button>
                                    </div-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Ordenes de Trabajos (OT)</h6>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="pull-left inline-block full-screen">
                                    <i class="zmdi zmdi-fullscreen"></i>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body row pa-0">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        @if(count($ots) > 0)
                                        <table class="table display product-overview border-none" id="support_table">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Fecha de creación</th>
                                                <th>Código de Ot</th>
                                                <th>Proyecto</th>
                                                <th>Lugar</th>
                                                <th>Area</th>
                                                <th>Cliente</th>
                                                <th>Estado</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $i = 1; @endphp
                                            @foreach($ots as $ot)
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $ot->created_at }}</td>
                                                    <td>{{ $ot->code_ot }}</td>
                                                    <td>{{ $ot->project_name }}</td>
                                                    <td>{{ $ot->place }}</td>
                                                    <td>{{ $ot->zone }}</td>
                                                    <td>{{ $ot->fullname }}</td>
                                                    @if($ot->status == 1)
                                                        @php $tag ='label-primary'; $estado = 'INICIADO'; @endphp
                                                    @elseif($ot->status == 2)
                                                        @php $tag ='label-warning'; $estado = 'PAUSADO'; @endphp
                                                    @elseif($ot->status == 3 || $ot->status == 4)
                                                        @php $tag ='label-danger'; $ot->status == 3 ? $estado = 'CERRADO' : $estado = 'ELIMINADO'; @endphp
                                                    @endif
                                                    <td><span class="label {{ $tag }}">  {{ $estado }}</span></td>
                                                    <td><!--a href="{{ route('ot.edit', $ot->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle" id="{{  $ot->id }}"><i class="fa fa-pencil"></i></a--></td>
                                                    <td><a href="{{ route('ot.view_ot', $ot->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle" id="{{  $ot->id }}"><i class="fa fa-eye"></i></a></td>
                                                </tr>
                                                @php $i++ @endphp
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @else
                                        <div class="col-md-12 text-center">
                                            <h5 class="text-danger">Aún no se han creado Proyectos (Ot).</h5>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            <!-- Modal -->
            <div class="modal fade" id="modalzone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['route'=>'ot.create']) !!}
                            <div class="form-group">
                                <label class="control-label mb-10" for="bodega">Seleccionar Bodega: </label>
                                <select class="form-control" name="bodega">
                                    <option value="">Seleccione... </option>
                                    @foreach($bodegas as $z)
                                        <option value="{{ $z->id }}">{{ $z->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Continuar</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
@stop

