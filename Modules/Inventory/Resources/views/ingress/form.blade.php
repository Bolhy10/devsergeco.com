<div class="col-lg-4 col-xs-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">{{ trans('inventory::ui.ingress.create.subtitle') }}</h6>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">





                        <div class="form-wrap">
                                @if(Auth::user()->hasRole('warehouseman'))
                                    <div class="form-group" :class="{'has-error': errors.has('receiver_to')}">
                                        <label class="control-label">{{ trans('inventory::ui.ingress.receiver_to') }}</label>
                                        <select name="receiver_to"
                                                v-model="formIncome.receiver_to"
                                                v-validate="'required'"
                                                class="form-control">
                                            <option :value="'{{ $users->id  }}'">{{ $users->username }}</option>
                                        </select>
                                    </div>

                                @endif

                                @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor') || Auth::user()->hasRole('inspector') || Auth::user()->hasRole('warehouse-chief'))
                                    <div class="form-group" :class="{'has-error': errors.has('receiver_to')}">
                                        <label class="control-label">{{ trans('inventory::ui.ingress.receiver_to') }}</label>

                                        <select name="receiver_to"
                                                v-model="formIncome.receiver_to"
                                                v-validate="'required'"
                                                class="form-control">
                                            @foreach($users as $rol)
                                                @foreach($rol->users as $user)
                                                    <option :value="'{{ $user->id }}'">{{ $user->username }} - {{ $rol->display_name }}</option>
                                                @endforeach
                                            @endforeach
                                        </select>

                                    </div>
                                @endif


                                <h6 class="txt-dark capitalize-font">
                                    <i class="fa fa-arrow-right"></i>
                                    {{ trans('inventory::ui.ingress.create.hr_ingress') }}
                                </h6>
                                <hr class="light-grey-hr">


                                <div class="form-group" :class="{'has-error': errors.has('deliver_to')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.deliver_to') }}</label>
                                    <input type="text"
                                           name="deliver_to"
                                           v-model="formIncome.deliver_to"
                                           v-validate="'required'"
                                           class="form-control"
                                           placeholder="{{ trans('inventory::ui.ingress.deliver_to') }}">
                                </div>


                                <div class="form-group" :class="{'has-error': errors.has('provider')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.provider') }}</label>
                                    <input type="text"
                                           name="provider"
                                           v-model="formIncome.provider"
                                           v-validate="'required'"
                                           class="form-control"
                                           placeholder="{{ trans('inventory::ui.ingress.provider') }}">
                                </div>
                                <div class="form-group" :class="{'has-error': errors.has('warehouses_provider')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.ware_provider') }}</label>
                                    <input type="text"
                                           name="warehouses_provider"
                                           v-model="formIncome.warehouses_provider"
                                           v-validate="'required'"
                                           class="form-control"
                                           placeholder="{{ trans('inventory::ui.ingress.ware_provider') }}">
                                </div>
                                <div class="form-group" :class="{'has-error': errors.has('no_document')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.no_document') }}</label>
                                    <input type="text"
                                           name="no_document"
                                           v-model="formIncome.no_document"
                                           v-validate="'required'"
                                           class="form-control"
                                           placeholder="{{ trans('inventory::ui.ingress.no_document') }}">
                                </div>
                                <div class="form-group" :class="{'has-error': errors.has('no_order')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.no_order') }}</label>
                                    <input type="text"
                                           name="no_order"
                                           v-model="formIncome.no_order"
                                           class="form-control"
                                           placeholder="{{ trans('inventory::ui.ingress.no_order') }}">
                                </div>
                                <div class="form-group" :class="{'has-error': errors.has('no_transfer')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.no_transfer') }}</label>
                                    <input type="text"
                                           name="no_transfer"
                                           v-model="formIncome.no_transfer"
                                           class="form-control"
                                           placeholder="{{ trans('inventory::ui.ingress.no_transfer') }}">
                                </div>
                                <div class="form-group" :class="{'has-error': errors.has('observations')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.observations') }}</label>
                                    <textarea
                                            name="observations"
                                            v-model="formIncome.observations"
                                            class="form-control"
                                            placeholder="{{ trans('inventory::ui.ingress.observations') }}"></textarea>
                                </div>


                                <h6 class="txt-dark capitalize-font">
                                    <i class="fa fa-product-hunt"></i>
                                    {{ trans('inventory::ui.ingress.create.title_inv') }}
                                </h6>
                                <hr class="light-grey-hr">

                                <div class="form-group" :class="{'has-error': errors.has('clients')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.clients') }}</label>
                                    <select class="form-control" @change="getWare(formIncome.clients)"
                                            name="clients"
                                            v-model="formIncome.clients"
                                            v-validate="'required'"
                                    >
                                        @foreach($clients as $client)
                                            <option :value="'{{ $client->id }}'">{{ $client->shortname }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group" :class="{'has-error': errors.has('warehouses_clients')}">
                                    <label class="control-label">{{ trans('inventory::ui.ingress.warehouse_client') }}</label>
                                    <select name="warehouses_clients"
                                            v-model="formIncome.warehouses_clients"
                                            v-validate="'required'"
                                            class="form-control">
                                        <option v-for="ware in warehouses" :key="ware.id" :value="ware.id">@{{ ware.name }}</option>
                                    </select>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>