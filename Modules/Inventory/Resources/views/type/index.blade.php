@extends('layouts.master')

@section('pageTitle', trans('inventory::ui.title'))

@section('content')

    <div class="row">
        <div class="col-md-12">

            @include('partials.message')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('inventory::ui.types.title') }}</h6>
                    </div>

                    @if(Auth::user()->can('create-inventory'))
                        <a href="{{ route('types.create') }}" class="btn btn-success pull-right">
                            {{ trans("inventory::ui.types.new_types") }}
                        </a>
                    @endif

                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="simpletable" class="table table-striped table-bordered" >
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ trans('inventory::ui.types.type') }}</th>
                                        <th>{{ trans('inventory::ui.types.status') }}</th>
                                        <th>{{ trans('inventory::ui.types.created_at') }}</th>
                                        <th>{{ trans('inventory::ui.types.updated_at') }}</th>

                                        @if(Auth::user()->can(['update-inventory', 'delete-inventory']))
                                            <th>{{ trans('inventory::ui.types.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ trans('inventory::ui.types.type') }}</th>
                                        <th>{{ trans('inventory::ui.types.status') }}</th>
                                        <th>{{ trans('inventory::ui.types.created_at') }}</th>
                                        <th>{{ trans('inventory::ui.types.updated_at') }}</th>

                                        @if(Auth::user()->can(['update-inventory', 'delete-inventory']))
                                            <th>{{ trans('inventory::ui.types.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($types as $key => $type)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $type->type }}</td>
                                            <td>
                                                @if($type->status == 1)
                                                    <span class="badge badge-success">
                                                    {{ "Activo" }}
                                                    </span>
                                                @endif
                                                @if($type->status == 0)
                                                    <span class="badge badge-danger">
                                                    {{ "Desactivado" }}
                                                    </span>
                                                @endif
                                            </td>

                                            <td>{{ $type->created_at }}</td>
                                            <td>{{ $type->updated_at }}</td>

                                            @if(Auth::user()->can(['update-inventory', 'delete-inventory']))
                                                <td>
                                                    @if(Auth::user()->can('update-inventory'))
                                                        <a href="{{ url('inventory/types/' . $type->id . '/edit') }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif

                                                    @if(Auth::user()->can('delete-inventory'))
                                                        <a href="#" data-url="{{ url('auth/user/'.$type->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                {!! $types->render() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @stop