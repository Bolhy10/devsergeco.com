<?php

namespace Modules\Dispatch\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Dispatch\Entities\StatusItem;
use Modules\Prestamos\Entities\Prestamos;

class StatusItemController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     *
     * ESTADOS DE LOS MATERAILES DE UN DESPACHO:
     * 0 ELIMINADO
     * 1 EN PROCESO
     * 2 INGRESADO
     */
    public function index()
    {
        return view('dispatch::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dispatch::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('dispatch::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $st_i = new StatusItem;
        $item = $st_i->st_item($id);
        return view('dispatch::pages.editdispatch',['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $item = StatusItem::find($id);
        $despacho = $item->dispatch_id;
        $item->quantity = $request->cantidad;
        $item->save();

        return redirect()->route('dispatch.view_dispatch',$despacho);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     * actualizar status a 0, ya que se elimina del despacho
     */
    public function destroy($id)
    {
        $it_st = StatusItem::find($id);
        $inventory_id = $it_st->inventory_id;
        $dispatch_id = $it_st->dispatch_id;
        $it_st->status = 0;
        $it_st->save();

        $cont_prestamo = Prestamos::where(['inventory_id'=>$inventory_id,'dispatch_id'=>$dispatch_id])->count();
        if( $cont_prestamo > 0)
        {
            Prestamos::where(['inventory_id'=>$inventory_id,'dispatch_id'=>$dispatch_id])
                ->update(['status' => 3]);
        }

        $item = StatusItem::find($id);
        $item->delete();

        return back();
    }
}
