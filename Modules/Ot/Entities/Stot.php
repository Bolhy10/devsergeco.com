<?php

namespace Modules\Ot\Entities;

use Illuminate\Database\Eloquent\Model;

class Stot extends Model
{
    protected $fillable = [
        'ot_id',
        'status'
    ];
    protected $primaryKey = 'ot_id';
    protected $table = 'stot';

    public function users_ot()
    {
        return $this->hasMany('\Modules\Ot\Entities\UsersOt');
    }

    public function ot()
    {
        return $this->belongsTo('Modules\Ot\Entities\Ot');
    }
}
