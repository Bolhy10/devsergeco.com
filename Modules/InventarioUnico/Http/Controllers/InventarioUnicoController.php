<?php

namespace Modules\InventarioUnico\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\InventarioUnico\Entities\Uinventary;

class InventarioUnicoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        /*$elementos = Uinventary::with(['inventory'=>function($q){
            $q->where('clients_id',1);
        }])->get();*/
        //$items_general = Uinventary::orderBy('created_at')->paginate(10);
        $items_c = Uinventary::with(['inventory.inventoryitems.inventorytypes' => function($q){
            $q->where('type',"Cables");
        },'inventory.clients','inventory.warehouses'])->paginate(3);

        $items_t = Uinventary::with(['inventory.inventoryitems.inventorytypes' => function($q){
            $q->where('type',"Transformador");
        },'inventory.clients','inventory.warehouses'])->paginate(3);

        return view('inventariounico::index',[ "i_cables" => $items_c, "i_th" => $items_t]);
        //return response()->json($items_c);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('inventariounico::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('inventariounico::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('inventariounico::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Devuelve datos relacionados al item que se solicita en base al id del inventario general
     */
    public function info( Request $request )
    {
        $id = $request->id;
        $data = Uinventary::where('inventory_id',$id)->get();
        return response()->json($data);
    }
}
