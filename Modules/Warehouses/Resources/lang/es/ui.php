<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 03/06/18
 * Time: 11:04 PM
 */

return [

    'title' => 'Control de Almacén',
    'count' => [
        'panel_1'=>[
            'title' => 'Estado',
            'warehouse' => 'Almacenes Activos',
            'zone' => 'Cantidad de Zonas',
            'province' => 'Total de provincias'
        ]
    ],

    'warehouses' => [
        'title' => 'Listado de Almacenes',
        'code'=> 'Código',
        'name' => 'Nombre',
        'zone' => 'Zona',
        'status' => 'Estado',
        'operation_label' => 'Acciones',
        'new_warehouse' => 'Nuevo Almacén',
        'button_add'    =>  'Agregar',
        'create' => [
            'title' => 'Nuevo Almacén',
            'btn_add' => 'Guardar',
        ],

        'update' => [
            'title' => 'Actualizar Almacén',
            'btn_update' => 'Actualizar',
        ],


        'message_delete'        =>  'El Almacén :name ha sido eliminado satisfactoriamente',
        'message_create'        =>  'El Almacén :name ha sido creado satisfactoriamente',
        'message_update'        =>  'El Almacén :name ha sido actualizado satisfactoriamente'

    ],

    'zones' => [
        'subtitle' => 'Listado de Zonas',
        'title'=> 'Control de Zonas',
        'name' => 'Nombre',
        'address' => 'Dirección',
        'provinces_id' => 'Provincia',
        'status' => 'Estado',
        'latitude' => 'Latitud',
        'longitude' => 'Longitud',
        'operation_label' => 'Acciones',
        'new_zones' => 'Nueva Zona',
        'button_add'    =>  'Agregar Zona',
        'create' => [
            'title' => 'Nueva Zona',
            'btn_add' => 'Guardar',
        ],

        'update' => [
            'title' => 'Actualizar Zona',
            'btn_update' => 'Actualizar',
        ],


        'message_delete'        =>  'La zona :name ha sido eliminada satisfactoriamente',
        'message_create'        =>  'La zona :name ha sido creada satisfactoriamente',
        'message_update'        =>  'La zona :name ha sido actualizada satisfactoriamente'

    ]

];