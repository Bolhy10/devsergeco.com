@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-8 col-lg-offset-2">

            @include('partials.message')

            @include('errors.form_error')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('warehouses::ui.warehouses.update.title') }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">

                            {!! Form::model($warehouses, ['method' => 'PUT', 'route' => ['warehouses.update', $warehouses->id]]) !!}

                            @include('warehouses::warehouses.form', array('warehouses' => $warehouses) + compact('zones'), ['button' => trans('warehouses::ui.warehouses.update.btn_update')])

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
