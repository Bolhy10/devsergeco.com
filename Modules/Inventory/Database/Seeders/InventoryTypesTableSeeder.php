<?php

namespace Modules\Inventory\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\InventoryType;
use Modules\Inventory\Entities\Inventorytypes;

class InventoryTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'type' => 'Herraje',
                'status' => 1,
                'created_at' => Carbon::now()
            ],
            [
                'type' => 'Cables',
                'status' => 1,
                'created_at' => Carbon::now()
            ],
            [
                'type' => 'Transformadores',
                'status' => 1,
                'created_at' => Carbon::now()
            ]
        ];

        foreach ($types as $key=>$value){
            Inventorytypes::create($value);
        }

    }
}
