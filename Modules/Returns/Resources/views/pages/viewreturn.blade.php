<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 04/04/2018
 * Time: 0:57
 */
?>

@extends('layouts.master2')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-right">
                        <h6>OT #{{ $ot->code_ot }}</h6>
                    </div>
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Devolución #{{ $devolucion->id }}</h6>
                        @if ($devolucion->status == 2 && $devolucion->prints == 0)
                            <p class="text-danger">Devolución finalizada.</p>
                        @elseif($devolucion->status == 2 && $devolucion->prints > 0)
                            <p class="text-danger">Devolución finalizada, el documento ya ha sido impreso. </p>
                        @endif
                        <p></p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Código</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Cant. a devolver</th>
                                <th scope="col">Categoría</th>
                                <th scope="col">Inventario</th>
                                @if($devolucion->status == 1)
                                    <!--th></th>
                                    <th></th-->
                                @endif
                            </tr>
                            </thead>
                            <tbody id="body-items">
                            @php $cont = 1; @endphp
                            @foreach( $data_devolucion as $d_d )
                                <tr>
                                    <td>{{ $cont }}</td>
                                    <td>{{$d_d->code_sku}}</td>
                                    <td>{{ $d_d->description}}</td>
                                    <td id="cant{{ $d_d->sti_id }}">{{ $d_d->sti_qty }}</td>
                                    <td>{{ $d_d->type }}</td>
                                    <td>{{ $d_d->fullname }}</td>
                                    @if($devolucion->status == 1)
                                        <!--td>
                                            <a @php $ot->stot->status != 1 ? print 'disabled' : print ' '; @endphp
                                               href="{{ route('statusitem.edit_item',$d_d->sti_id) }}" class="btn btn-success btn-sm btn-icon-anim btn-circle"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td>
                                            <a @php $ot->stot->status != 1 ? print 'disabled' : print ' '; @endphp
                                               href="{{ route('dispatch.delete_item', $d_d->sti_id) }}" class="btn btn-danger btn-sm btn-icon-anim btn-circle"><i class="fa fa-trash"></i></a>
                                        </td-->
                                    @endif
                                    <input type="hidden" value="{{ $d_d->sti_invid }}" name="id_inv">
                                </tr>
                                @php $cont++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <input type="hidden" id="id-devolucion" value="{{ $devolucion->id }}">
                                    <a class="btn btn-primary btn-outline" href="{{ route('ot.view_ot',$ot->id) }}"> REGRESAR </a>
                                    @if($devolucion->status == 1)
                                        <button @php $ot->stot->status != 1 ? print 'disabled' : print ' '; @endphp class="btn btn-success" id="end_return">FINALIZAR E IMPRIMIR</button>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                @php //se valida si el estado del despacho es 'en despacho' => 1 @endphp
                                @if($devolucion->status === 1)
                                    @php //si el estado de la ot es distinta a 1   @endphp
                                    <button @php $ot->stot->status != 1 ? print 'disabled' : print ' '; @endphp class="btn btn-danger pull-right" id="borrar-devolucion">ELIMINAR DEVOLUCIÓN</button>
                                @elseif($devolucion->status === 2)
                                    <a class="btn btn-success btn-outline" href="{{ route('ot.prints',[ 'option' => 2, 'id' => $devolucion->id ]) }}" target="_blank">IMPRIMIR</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    @include('returns::layouts.script-return2')
@endsection


