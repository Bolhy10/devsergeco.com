<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //tabla opcional
        Schema::create('status_ot', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('status');
        });

        /**
         * register_ot: Tabla que almacenara las Ot's que se vayan registrando en el sistema
         * register_ot
         */
        Schema::create('ot', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_ot');
            $table->integer('clients_id')->unsigned();
            $table->string('project_name');
            $table->string('place');
            $table->string('zone');
            $table->timestamps();

            $table->foreign('clients_id')->references('id')->on('clients');
        });

        /**
         * stot = tabla que almacena los posibles estados en los cuales puede estar un Ot,
         * status es el estado en el cual se encuentra.
         */
        Schema::create('stot', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('ot_id')->unsigned();
            $table->integer('status');
            $table->timestamps();

            $table->foreign('ot_id')->references('id')->on('ot');
            //$table->foreign('status_id')->references('id')->on('status_ot');
        });

        /**
         * users_ot: id de los usuarios asignados a las OT's.
         */
        Schema::create('users_ot', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('ot_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('ot_id')->references('id')->on('ot');
        });

        /**
         * ot_edited: tabla que almacena las ediciones que se realizan en las ot's
         */

        Schema::create('ot_edited', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('ot_id')->unsigned();
            $table->string('action');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('ot_id')->references('id')->on('ot');

        });

        /**
         * ot_status_process: tabla que almacena el estado del proceso en el que se encuentra un ot
         * (Solicitado, agregar item, despacho,  registrado, copia)
         */
        /*Schema::create('ot_status_process', function (Blueprint $table) {
            $table->integer('ot_id')->unsigned();
            $table->integer('st_proc_inv_id')->unsigned();

            $table->foreign('ot_id')->references('id')->on('register_ot');
            $table->foreign('st_proc_inv_id')->references('id')->on('status_process_inventary');

        });*/

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ot_edited');
        Schema::drop('users_ot');
        Schema::drop('stot');
        Schema::drop('ot');
        Schema::drop('status_ot');
    }
}
