<?php

namespace Modules\Inventory\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Modules\Clients\Entities\Clients;
use Modules\Inventory\Entities\Inventory;
use Modules\Inventory\Transformers\AllInventoryResource;
use Modules\Warehouses\Entities\Warehouses;
use Symfony\Component\HttpKernel\Client;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(Auth::user()->can('read-inventory')){

            $clients = Clients::all();

            return view('inventory::index', compact('clients'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    public function inventoryAll($clients, $warehouses = 0){

        if(Auth::user()->can('read-inventory')){

            $inventory = Inventory::orderBy('id' , 'desc')->with("inventoryitems", "clients", "warehouses")->paginate(10);

            if ($clients > 0){

                $inventory = Inventory::orderBy('id' , 'desc')->with("inventoryitems", "clients", "warehouses")
                    ->where("clients_id", '=', $clients)->paginate(10);


                if ($warehouses > 0){
                    $inventory = Inventory::orderBy('id' , 'desc')
                        ->with("inventoryitems", "clients", "warehouses")
                        ->where("clients_id", '=', $clients)
                        ->where("warehouses_id", '=',$warehouses)->paginate(10);
                }


                return AllInventoryResource::collection($inventory);

            }

            return AllInventoryResource::collection($inventory);

        }

        return $this->response_error('No tiene permiso para consultar datos.');

    }

    public function searchItems(Request $request){

        if (Auth::user()->can('read-inventory')){

            $inventory = Inventory::orderBy('id', 'desc')->with('inventoryitems', 'clients', 'warehouses')->get();

            for ($v = 0; $v < count($inventory); $v++){

                if ($inventory[$v]['inventoryitems']['code_sku'] == $request->code){

                    return json_encode(['status' => 200, 'message' => 'Se encontro el registro.', 'data'=> [new AllInventoryResource($inventory[$v])] ]);

                }

                return $this->response_error('No se encontro registro con el Código enviado: '.$request->code.'.');
            }

        }

        return $this->response_error('No esta autorizado para esta consulta.');
    }

    public function createPDF(Request $request){

        if (Auth::user()->can('read-inventory')){

            $view = 'inventory::layouts.print';

            $inventory = Inventory::orderBy('id' , 'desc')->with("inventoryitems", "clients", "warehouses")->get();

            $date = Carbon::now();

            $title = 'Inventario General';

            $down = 'inventario_general_'.$date;

            if ($request->clients > 0){

                $clients = Clients::find($request->clients);

                $title = 'Inventario de '.$clients->fullname;

                $down = 'inventario_'.$clients->fullname.'_'.$date;

                $inventory = Inventory::orderBy('id' , 'desc')->with("inventoryitems", "clients", "warehouses")
                    ->where("clients_id", '=', $request->clients)->get();


                if ($request->warehouses > 0){

                    $ware = Warehouses::find($request->warehouses);

                    $title = 'Inventario de '.$clients->fullname.' - '.$ware->name;

                    $down = 'inventario_'.$clients->fullname.'_'.$ware->name.'_'.$date;

                    $inventory = Inventory::orderBy('id' , 'desc')
                        ->with("inventoryitems", "clients", "warehouses")
                        ->where("clients_id", '=', $request->clients)
                        ->where("warehouses_id", '=',$request->warehouses)->get();
                }

                return $this->generatePDF($title, $down, $inventory, $view);
            }

            return $this->generatePDF($title, $down, $inventory, $view);

        }

        return $this->response_error('No tiene permiso para descargar este documento.');
    }


    public function generatePDF($title, $down, $data, $view){

        if (Auth::user()->can('read-inventory')){

            $date = Carbon::now();

            $users = Auth::user();

            $vista = \View::make($view, compact('data', 'title', 'date', 'users'))->render();

            $pdf = \App::make('dompdf.wrapper');

            $pdf->loadHTML($vista);

            return $pdf->download($down.'.pdf');

        }

        return $this->response_error('No tiene permisos para generar el PDF.');

    }


//    ERROR THE AUTHENTICATION
    public function response_error($message){
        return json_encode(
            [
                'status' => 400,
                'message' => $message
            ]
        );
    }

    public function notAuthenticate(){
        Session::flash('message_error', trans('auth::ui.user.message_not'));
    }
}
