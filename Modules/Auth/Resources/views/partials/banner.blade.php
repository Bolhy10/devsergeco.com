<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url({{ asset('assets/images/elements/bg_profile.jpg') }})">
    <div class="m-grid__item m-grid__item--middle">
        <h3 class="m-login__welcome">
            <img src="{{ asset('assets/images/elements/logo_vm.png') }}">
        </h3>
        <p class="m-login__msg text-center">
            Panel administrativo para el control de inventario.
            <br>

        </p>
    </div>
</div>