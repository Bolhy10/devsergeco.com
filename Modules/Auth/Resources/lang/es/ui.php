<?php

return [

	'general'                  => [
		'back'                 => 'Volver'
	],

    'permission'                =>  [
        'name'                  =>  'Nombre',
        'names'                 =>  'Listado de Permisos',
        'display'               =>  'Nombre a mostrar',
        'description'           =>  'Descripción',
        'name_label'            =>  'Permiso',
        'operation_label'       =>  'Operaciones',
        'new_permission'        =>  'Nuevo Permiso',
        'edit_permission'       =>  'Editar Permiso',
        'button_update'         =>  'Actualizar',
        'button_delete'         =>  'Eliminar',
        'button_add'            =>  'Agregar',
        'message_delete'        =>  'El permiso :name ha sido eliminado satisfactoriamente',
        'message_create'        =>  'El permiso :name ha sido creado satisfactoriamente',
        'message_update'        =>  'El permiso :name ha sido actualizado satisfactoriamente'
    ],

    'role'                =>  [
        'name'                  =>  'Nombre',
        'names'                 =>  'Listado de Roles',
        'display'               =>  'Nombre a mostrar',
        'description'           =>  'Descripción',
        'name_label'            =>  'Rol',
        'operation_label'       =>  'Operaciones',
        'new_role'              =>  'Nuevo Rol',
        'edit_role'             =>  'Editar Rol',
        'button_update'         =>  'Actualizar',
        'button_delete'         =>  'Eliminar',
        'button_add'            =>  'Agregar',
        'message_delete'        =>  'El rol :name ha sido eliminado satisfactoriamente',
        'message_create'        =>  'El rol :name ha sido creado satisfactoriamente',
        'message_update'        =>  'El rol :name ha sido actualizado satisfactoriamente'
    ],
    'user'                      =>  [
        'title'                 =>  'Listado de Usuarios',
        'name'                  =>  'Nombre',
        'names'                 =>  'Usuarios',
        'firstname'             =>  'Nombre',
        'lastname'              =>  'Apellido',
        'username'              =>  'Usuario',
        'email'                 =>  'Correo',
        'phone'               =>  'Número De Contacto',
        'password'              =>  'Contraseña',
        'password_confirmation' =>  'Confirmar Contraseña',
        'password_new'          =>  'Contraseña Nueva',
        'password_old'          =>  'Contraseña Anterior',
        'name_label'            =>  'Usuario',
        'change_password'       =>  'Cambiar Contraseña',
        'operation_label'       =>  'Acciones',
        'new_user'              =>  'Nuevo Usuario',
        'edit_user'             =>  'Editar Usuario',
        'button_update'         =>  'Actualizar',
        'button_delete'         =>  'Eliminar',
        'button_add'            =>  'Agregar',
        'message_change_password' =>  'La contraseña ha sido cambiada satisfactoriamente',
        'message_delete'        =>  'El usuario :name ha sido eliminado satisfactoriamente',
        'message_create'        =>  'El usuario :name ha sido creado satisfactoriamente',
        'message_update'        =>  'El usuario :name ha sido actualizado satisfactoriamente',
        'message_not'           =>  'No contiene los permisos necesarios para acceder.',
    ],

    'login'                     =>  [
        'title'                 =>  'Iniciar Sesión en Administrador',
        'signin'                =>  'Iniciar Sesión',
        'rememberme'            =>  'Recuérdame',
        'email'                 =>  'Correo',
        'username'              =>  'Usuario',
        'password'              =>  'Contraseña',
        'forgot'                =>  '¿Contraseña olvidada?',
        'forgot_subtitle'       =>  'Ingrese su correo electrónico para restablecer su contraseña:',
        'credentials_error'     =>  'El :field o la contraseña no son correctos',
        'send_forgot'           =>  'Enviar',
        'cancel_forgot'         => 'Cancelar'
    ],

    'profile'                   =>[
        'message_update'        => 'Su cuenta :username ha sido actualizada',
        'message_error'         => 'Error al actualizar su cuenta :username. Intente nuevament',
    ]
];
