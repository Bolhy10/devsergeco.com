<div class="col-lg-12 col-md-6 col-xs-12">

    <!-- Row -->
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('warehouses::ui.count.panel_1.title') }}</h6>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block refresh">
                            <i class="fa fa-star"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body row">
                        <div class="">
                            <div class="pl-15 pr-15 mb-15">
                                <div class="pull-left">
                                    <i class="zmdi zmdi-collection-folder-image inline-block mr-10 font-16"></i>
                                    <span class="inline-block txt-dark">{{ trans('warehouses::ui.count.panel_1.warehouse') }}</span>
                                </div>
                                <span class="inline-block txt-warning pull-right weight-500">2</span>
                                <div class="clearfix"></div>
                            </div>
                            <hr class="light-grey-hr mt-0 mb-15"/>
                            <div class="pl-15 pr-15 mb-15">
                                <div class="pull-left">
                                    <i class="zmdi zmdi-format-list-bulleted inline-block mr-10 font-16"></i>
                                    <span class="inline-block txt-dark">{{ trans('warehouses::ui.count.panel_1.zone') }}</span>
                                </div>
                                <span class="inline-block txt-danger pull-right weight-500">5</span>
                                <div class="clearfix"></div>
                            </div>
                            <hr class="light-grey-hr mt-0 mb-15"/>
                            <div class="pl-15 pr-15 mb-15">
                                <div class="pull-left">
                                    <i class="zmdi zmdi-ticket-star inline-block mr-10 font-16"></i>
                                    <span class="inline-block txt-dark">{{ trans('warehouses::ui.count.panel_1.province') }}</span>
                                </div>
                                <span class="inline-block txt-primary pull-right weight-500">33</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view panel-refresh">
                <div class="refresh-container">
                    <div class="la-anim-1"></div>
                </div>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Items de Inventario</h6>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block refresh">
                            <i class="fa fa-briefcase"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body text-center">
                        <div style="height: 140px;">
                            <div class="btn-group mt-30 mr-10">
                                <button class="btn btn-primary btn-icon-anim" data-toggle="modal" data-target="#make-item">Agregar Item <i class="fa fa-plus"></i></button>
                                <a class="btn btn-default btn-icon-anim" href="inventary/descripcion-item">Ver Items <i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view panel-refresh">
                <div class="refresh-container">
                    <div class="la-anim-1"></div>
                </div>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Bodegas</h6>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block refresh">
                            <i class="fa fa-building"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body text-center">
                        <div style="height: 140px;">
                            <div class="btn-group mt-30 mr-10">
                                <button class="btn btn-primary btn-icon-anim">Agregar Bodega <i class="fa fa-plus"></i></button>
                                <button class="btn btn-default btn-icon-anim">Ver Bodega <i class="fa fa-eye"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Row -->


</div>