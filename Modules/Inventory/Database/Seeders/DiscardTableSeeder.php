<?php

namespace Modules\Inventory\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class DiscardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        for ($i = 0; $i < 10; $i++){

            DB::table('discard')->insert(array(

                'users_id' => 1,

                'clients_id' => $faker->numberBetween($min = 1, $max = 5),

                'warehouses_id' => $faker->randomElement($array = array (1, 2)),

                'no_document' => $faker->randomNumber(4),

                'receive_to' => $faker->name,

                'observations' => $faker->text($maxNbChars = 100),

                'status' => $faker->randomElement($array = array (true, false)),

                'created_at' => Carbon::now(),

                'updated_at' => Carbon::now()

            ));
        }
    }
}
