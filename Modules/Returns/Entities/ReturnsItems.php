<?php

namespace Modules\Returns\Entities;

use Illuminate\Database\Eloquent\Model;

class ReturnsItems extends Model
{
    protected $fillable = [
        'quantity',
        'inventory_id',
        'returns_id',
        'status'
    ];
    protected $table = 'returns_items';

    public function inventory()
    {
        return $this->belongsTo('Modules\Inventory\Entities\Inventory');
    }
}
