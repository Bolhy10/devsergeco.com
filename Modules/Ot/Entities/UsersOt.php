<?php

namespace Modules\Ot\Entities;

use Illuminate\Database\Eloquent\Model;

class UsersOt extends Model
{
    protected $fillable = [
        'name',
        'lastname',
        'ot_id',
        'status'
    ];
    public $timestamps = false;
    protected $table = 'users_ot';
}
