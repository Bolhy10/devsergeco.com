@include('layouts.elements.button_sidebar')

<div class="fixed-sidebar-left main-sidebar">

    <div class="slimScrollDiv" style="position: relative;
    overflow: hidden; width: auto; height: 100%;">

        <ul class="nav navbar-nav side-nav nicescroll-bar"
            style="overflow: auto; width: auto; height: 100%;">
            @foreach(trans('ui.sidebar.data') as $sideTitle=>$value)

                <li class="navigation-header">
                    <span>{{ $sideTitle }}</span>
                    <i class="fa -fa-ellipsis-h"></i>
                </li>
                @foreach($value as $list=>$val)

                @if(Auth::user()->can($val['permission']) )
                    @if(!empty($val['submenu']))
                    <li class="dropdown">
                        <!--a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $val['title'] }}<span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a-->
                        <a href="{{ route($val['url']) }}" class="dropdown-toggle" data-toggle="dropdown">
                            <div class="pull-left"><i class="{{ $val['icon'] }} mr-20"></i>
                                <span class="right-nav-text">{{ $val['title'] }}</span>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                        <ul class="dropdown-menu forAnimate" role="menu">
                            @foreach($val['submenu'] as $sub)
                          <li><a href="{{ route($sub['url']) }}">{{ $sub['title']}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ route($val['url']) }}" data-toggle="collapse" data-target="#dashboard_dr">
                            <div class="pull-left"><i class="{{ $val['icon'] }} mr-20"></i>
                                <span class="right-nav-text">{{ $val['title'] }}</span>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </li>
                    @endif
                @endif

                @endforeach

                <li><hr class="light-grey-hr mb-10"></li>

            @endforeach

        </ul>

        <div class="slimScrollBar" style="background: rgb(135, 135, 135);
        width: 4px; position: absolute; top: 0px; opacity: 0.4;
        display: none; border-radius: 0px; z-index: 99; right: 1px; height: 734px;">
        </div>

        <div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute;
        top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51);
         opacity: 0.2; z-index: 90; right: 1px;">
        </div>
    </div>
</div>