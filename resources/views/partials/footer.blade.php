<footer class="footer container-fluid pl-30 pr-30">
	<div class="row">
		<div class="col-sm-12">
			<p>{{ date('Y') }} © Sergeco, S.A - V&M. Powerby CompilerSoft</p>
		</div>
	</div>
</footer>