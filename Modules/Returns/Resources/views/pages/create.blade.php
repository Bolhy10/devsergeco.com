<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 03/04/2018
 * Time: 8:38
 */

?>

@extends('layouts.master2')

@section('content')
    @section('pageTitle',$titulo)

    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Seleccione el material y agregue la cantidad</h6>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper">
                <div class="panel-body row ">
                        {!! Form::open(['route'=>'returns.store' ]) !!}
                        <table class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th style="width: 10px;">#</th>
                                <th style="width: 100px;">Código</th>
                                <th style="width: 300px;">Descripción</th>
                                <th style="width: 30px;">Cantidad Despachada</th>
                                <th>Categoría</th>
                                <th>Cantidad a devolver</th>
                                <th>Inventario</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="body-items">
                            <tr id="row-inv-1">
                                <td>1</td>
                                <td>
                                    <div class="col-md-3">
                                        <div class="input-group my-group">
                                            <select id="code1" name="id[]" class="form-control form-control-sm selectpicker code" data-width="120px" data-style=" btn-default btn-outline" data-live-search="true">
                                                <option value="">Seleccione el item a devolver</option>
                                                @foreach($materiales as $p)
                                                    <option value="{{ $p->sti_id }}">{{$p->code_sku}} {{$p->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="description1"></span></p>
                                </td>
                                <td>
                                    <div>
                                        <p class="inline-block">
                                            <span class="capitalize-font txt-primary mr-5 weight-600" id="cant_d1"></span>
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="categoria1"></span></p>
                                </td>
                                <td>
                                    <div class="form-group text-center" id="form-group-cantidad1">
                                        <input id="cantidad1" class="form-control form-control-sm cantidad" type="number"  value="" name="cantidad[]">
                                        <span id="medida1"></span>
                                    </div>
                                </td>
                                <td>
                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="inventario1"></span></p>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-danger btn-outline borrar" id="1"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="col-md-8 col-sm-12 col-xs-12 col-lg-8">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left">Observación:</label>
                                <textarea name="observacion" class="form-control" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left">Entregar a</label>
                                <input type="text" class="form-control" name="entregar_a" />
                            </div>
                        </div>
                        <input type="hidden" value="{{ $ot_id }}" name="ot_id">
                        <div class="row text-center">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <a class="btn btn-primary pull-left" href="{{ route('ot.view_ot',['id'=>$ot_id]) }}">CANCELAR</a>
                            </div>
                            <div class="col-md-6 col-sm-12 col-lg-6">
                                <button type="submit" class="btn btn-success pull-left" id="save_dispatch">GUARDAR DEVOLUCIÓN</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
    @include('returns::layouts.script-return')
@endsection
