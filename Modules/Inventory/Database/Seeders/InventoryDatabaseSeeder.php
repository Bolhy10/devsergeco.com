<?php

namespace Modules\Inventory\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InventoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(InventoryTypesTableSeeder::class);

        $this->call(InventoryItemsTableSeeder::class);

        $this->call(InventoryTableSeeder::class);

        $this->call(IngressTableSeeder::class);
        $this->call(IngressItemsTableSeeder::class);

        $this->call(DiscardTableSeeder::class);
        $this->call(DiscardItemsTableSeeder::class);
    }
}
