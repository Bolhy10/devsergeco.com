const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([

    'resources/assets/js/toast.min.js',
    'resources/assets/js/app.js',
    'Modules/Inventory/Resources/assets/js/discard.js',
    'Modules/Inventory/Resources/assets/js/inventory.js',
    'Modules/Inventory/Resources/assets/js/ingress.js',
    'Modules/Dispatch/Resources/assets/js/dispatch.js'

    ], 'public/js/app.js')

.styles([
    'resources/assets/css/toast.min.css'
], 'public/css/app.css');




//.sass('resources/assets/sass/app.scss', 'public/css');
