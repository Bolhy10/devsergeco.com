<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Clients\Http\Controllers'], function()
{
    Route::resource('clients', 'ClientsController');
});
