<?php

namespace Modules\Dashboard\Http\Controllers;

use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Clients\Entities\Clients;
use Modules\Inventory\Entities\Inventoryitems;
use Modules\Warehouses\Entities\Warehouses;
use Nwidart\Modules\Routing\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $users = User::all()->count();
        $cant_items = Inventoryitems::all()->count();
        $cant_warehouse = Warehouses::all()->count();
        $cant_client = Clients::all()->count();

        return view('dashboard::index', compact('users', 'cant_items', 'cant_warehouse', 'cant_client'));

    }

}
