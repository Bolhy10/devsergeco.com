<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="mobile-only-brand pull-left">
        <div class="nav-header pull-left">
            <div class="logo-wrap">
                <a href="{{ route('dashboard')  }}">
                    <img class="brand-img hidden-md hidden-lg hidden-sm"
                         src="{{ asset('assets/images/elements/logo_icono.png') }}">
                    <img class="brand-img img-responsive hidden-xs"
                         src="{{ asset('assets/images/elements/logo.png') }}">
                </a>
            </div>
        </div>

        @include('layouts.elements.button_sidebar')

    </div>

    <div id="mobile_only_nav" class="mobile-only-nav pull-right">

        @include('layouts.elements.menu_profile')

        @include('layouts.elements.menu_top')


    </div>


</nav>