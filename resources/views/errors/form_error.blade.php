@if (count($errors) > 0)
    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible">
        <strong>Whoops!</strong> Error<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif