<?php
/**
 * Created by PhpStorm.
 * User: yvargas
 * Date: 03/26/2018
 * Time: 11:08 AM
 */
?>
<!-- Script usado en los despachos -->
<script>
    var fin_despacho = '{{ route('dispatch.end') }}';
    var borrar_despacho = '{{ route('dispatch.delete') }}'
    var token = $('meta[name=csrf-token]').attr('content');
    
    //route( 'ot.prints',[ 'option' => 1,'id' => $data->id ])

    $(document).ready(function(){
        // al hacer click en el boton de finalizar despacho
        $('#end_dispatch').click(function () {
            var id = $('#id-despacho').val();
            var pass = prompt('Introduzca la contraseña:');
            var url_imprimir = '{{ route('ot.prints',['option' => 1, 'id' => 'id' ]) }}';
            if(pass === 'sergeco2018')
            {
                $.post(fin_despacho , { id: id, _token: token })
                    .done(function( data )
                    {
                        if(data.st === 1)
                        {
                            alert(data.ms);
                            location.reload();
                            window.open(url_imprimir, '_blank');
                        }
                        else if(data.st === 0)
                        {
                            //se muestran los mensajes de error
                            alert(data.ms);
                        }
                    });
            }
        });

        //al hacer click en el botón de eliminar despacho
        $('#borrar-despacho').click(function(){
            var token = $('meta[name=csrf-token]').attr('content');
            var id_despacho = $('#id-despacho').val();
            $.post(borrar_despacho, {id:id_despacho, _token: token })
                .done(function (data)
                {
                    if(data.st === 1)
                    {
                        alert(data.ms);
                        window.location.replace('{{ route('ot.index') }}');
                    }
                });
        });
    });
</script>