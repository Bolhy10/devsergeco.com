<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableDispatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * tabla dispatch, almacena los despachos relacionados a una OT
         */
        Schema::create('dispatch',function(Blueprint $table){
           $table->increments('id');
           $table->integer('ot_id');
           $table->integer('status');
           $table->string('observacion')->nullable();
           $table->string('entregar')->nullable();
           $table->integer('prints')->default(0);
           $table->timestamps();
           $table->softDeletes();
        });

        /**
         * status_item = almacena la cantidad usada en los despachos
         */

        Schema::create('status_item',function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('quantity');
            $table->integer('inventory_id')->unsigned();
            $table->integer('dispatch_id')->unsigned();
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();

            //foreign key
            $table->foreign('inventory_id')->references('id')->on('inventory');
            $table->foreign('dispatch_id')->references('id')->on('dispatch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('status_item');
        Schema::drop('dispatch');
    }
}
