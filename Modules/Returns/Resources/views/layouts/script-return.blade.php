<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 03/04/2018
 * Time: 10:11
 */
?>

<script>

    var route_getitem = '{{ route('returns.get_data_item') }}' ;  //obtener información de un item en especifico.


    //variable global glob_row, almacena el número de filas que se van creando
    var glob_row = 1;
    var btn_pres = 0;

    $(document).ready(function(){
        var that = $(this);

        /**Al cambiar el valor del select de categoria**/
        $(document).on('change','.code',function () {
            //dato id
            var id_text = $(this).attr('id');
            //id de la fila
            var id_row = id_text.substr(4,1);
            //id del inventario
            console.log(id_row);
            var id_item = $(this).val();
            var description = '#description'+id_row;
            var categoria = '#categoria'+id_row;
            var inventario = '#inventario'+id_row;
            var cantidad = '#cantidad'+id_row;     //a devolver
            var cantidad_despachada = '#cant_d'+id_row;  //que se despacho
            var disp = '#disp'+id_row;
            var medida = '#medida'+id_row;
            var token = $('meta[name=csrf-token]').attr('content');
            var qnt = $(cantidad).val();

            $.post(route_getitem , { iditem: id_item, _token: token })
                .done(function( data ) {
                    //for (var i in data){
                    $(description).text(data[0]['description']);
                    //console.log(data[0]['description']);
                    $(categoria).text(data[0]['type']);
                    $(inventario).text(data[0]['fullname']);
                    $(cantidad_despachada).text(data[0]['sti_qty']);
                    if(data['type_id'] === 2)
                    {
                        $(medida).text('mt');
                    }
                    else
                    {
                        $(medida).text('un');
                    }
                    //}
                    //var cant_disp = +($(disp).text());
                    //validar si la cantidad solicitada es mayor a 0
                    /*if(qnt > 0){
                        //validar si la cantidad solicitada es mayor que la disponible
                        if(qnt > cant_disp)
                        {
                            alert('La cantidad solicitada excede de la disponible')
                            $(form_cantidad).addClass('has-error');
                        }
                    }*/
                });

        });

        $('form').bind("keypress", function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });

        /**
         * Se valida que las cantidades de los materiales sean mayor que 0
         **/
        $( "form" ).submit(function( event ) {
            //event.preventDefault();
            var cant_inp = $('input[name="cantidad[]"]').length;
            //alert(cant_inp);
            //for (var i = 0; i< )
            //$(this).submit();
        });
        /**
         * Al momento de enfocar el campo de cantidad se debe de validar si la cantidad es > 0
         **/
        $(document).on('input','.cantidad',function()
        {
            var id_text = $(this).attr('id');
            var id_row = parseInt(id_text.substr(8,1));
            //alert(id_row);
            var code_id = '#code'+id_row;
            var despachadas = '#cant_d'+id_row;
            var cant_d = +($(despachadas).text());
            var id_item = $(code_id).val();   //id del item despachado
            var cantidad = +($(this).val());  //cantidad ingresada para devolución
            var table = $('#body-items');
            var form_cantidad = '#form-group-cantidad'+id_row;

            console.log('cantidad: '+cantidad+' '+'cantidad disponible'+' '+cant_d);
            if(cantidad > cant_d || cantidad < 0)
            {
                alert('error en la cantidad a devolver, revise nuevamente');
            }

            $('.cantidad').keypress(function(e)
            {
                cantidad = +($(this).val());
                if(e.which === 13)
                {
                    if(cant_d > 0){
                        if(cantidad > cant_d || cantidad < 0)
                        {
                            alert('cantidad excede, disminuya o tomarlo prestado de otro inventario de la bodega');
                            $(form_cantidad).addClass('has-error');
                        }
                        else if((cantidad <= cant_d) && (cantidad > 0))
                        {
                            //si el usuario regresa a una fila creada para modificar, no se añade otro campo
                            //if(id_row >= glob_row){
                                $(form_cantidad).removeClass('has-error');
                                glob_row ++;
                                $(table).append(
                                    '<tr id="row-inv-'+glob_row+'">' +
                                    '                                <td>'+glob_row+'</td>' +
                                    '                                <td>' +
                                    '                                    <div class="col-md-3">' +
                                    '                                        <div class="input-group my-group">' +
                                    '                                            <select id="code'+glob_row+'" name="id[]" class="form-control form-control-sm selectpicker code" data-width="120px" data-style=" btn-default btn-outline" data-live-search="true">' +
                                    '                                                <option value="">Seleccione el item a devolver</option>' +
                                    '                                                @foreach($materiales as $p)' +
                                    '                                                    <option value="{{ $p->sti_id }}">{{$p->code_sku}} {{$p->description}}</option>' +
                                    '                                                @endforeach' +
                                    '                                            </select>' +
                                    '                                        </div>' +
                                    '                                    </div>' +
                                    '                                </td>' +
                                    '                                <td>' +
                                    '                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="description'+glob_row+'"></span></p>' +
                                    '                                </td>' +
                                    '                                <td>' +
                                    '                                    <div>' +
                                    '                                        <p class="inline-block">' +
                                    '                                            <span class="capitalize-font txt-primary mr-5 weight-600" id="cant_d'+glob_row+'"></span>' +
                                    '                                        </p>' +
                                    '                                    </div>' +
                                    '                                </td>' +
                                    '                                <td>' +
                                    '                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="categoria'+glob_row+'"></span></p>' +
                                    '                                </td>' +
                                    '                                <td>' +
                                    '                                    <div class="form-group text-center" id="form-group-cantidad'+glob_row+'">' +
                                    '                                        <input id="cantidad'+glob_row+'" class="form-control form-control-sm cantidad" type="number"  value="" name="cantidad[]">\n' +
                                    '                                        <span id="medida'+glob_row+'"></span>' +
                                    '                                    </div>' +
                                    '                                </td>' +
                                    '                                <td>' +
                                    '                                    <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="inventario'+glob_row+'"></span></p>' +
                                    '                                </td>' +
                                    '                                 <td>' +
                                    '                                    <button type="button" class="btn btn-sm btn-danger btn-outline borrar" id="'+glob_row+'"><i class="fa fa-trash"></i></button>' +
                                    '                                </td>'+
                                    '                            </tr>'
                                );
                                var select = $('#code'+glob_row);
                                select.selectpicker('refresh');
                            //}
                        }
                    }

                }
            });


        });
        //Pedir prestamo(click en el formulario de item)

        $(document).on('click','.borrar',function(){
            var id = $(this).attr('id');
            if(id > 1){
                var row = '#row-inv-'+id;
                $(row).html('');
            }
            else
                {
                    alert('El primer item no se puede eliminar');
                }
        });

    });



</script>
