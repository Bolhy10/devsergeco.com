

<div class="col-lg-8 col-xs-12">

    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <div class="">
                <h6 class="panel-title txt-dark">
                    {{ trans('inventory::ui.discard.table.title') }}
                </h6>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">

                        <div class="alert alert-warning alert-dismissable alert-style-1" v-if="formIncome.items.length > 1">
                            <i class="fa fa-warning"></i>{{ trans('inventory::ui.discard.error.not_items') }}
                        </div>

                        <table class="table table-striped" v-if="formIncome.items.length > 0">
                            <thead>
                            <tr>
                                <th>{{ trans('inventory::ui.discard.table.code') }}</th>
                                <th>{{ trans('inventory::ui.discard.table.description') }}</th>
                                <th>{{ trans('inventory::ui.discard.table.type') }}</th>
                                <th>{{ trans('inventory::ui.discard.table.quantity') }}</th>
                                <th>{{ trans('inventory::ui.discard.table.observations') }}</th>
                                <th>{{ trans('inventory::ui.discard.operation_label') }}</th>
                            </tr>
                            </thead>

                            <tbody>

                            <tr v-for="(inv, index) in formIncome.items" :id="'item-'+index">
                                <td>
                                    <div :class="{'has-error': errors.has('code'+inv.id) }">
                                        <select :name="'code'+inv.id"
                                                v-validate="'required'"
                                                class="form-control"
                                                v-model="inv.items.id"
                                                @change="pushDetails(inv)">
                                            <option value="">Nada</option>
                                            <option v-for="item in items" :key="item.id" :value="item.id">@{{ item.code }}</option>
                                        </select>
                                    </div>

                                </td>
                                <td>
                                    <label>@{{ inv.items.description }}</label>
                                </td>
                                <td><span>@{{ inv.items.type.name }}</span></td>
                                <td>
                                    <div :class="{'has-error': errors.has('quantity'+inv.quantity) }">
                                        <input type="number" :name="'quantity'+inv.quantity" min="1" v-validate="'required|numeric'" v-model="inv.quantity" class="form-control">
                                    </div>

                                </td>
                                <td>
                                    <input type="text" v-model="inv.observations" class="form-control">
                                </td>

                                <td>
                                    <button type="button" class="btn btn-danger btn-icon-anim btn-circle" title="Remove Item" @click="removeItem(inv)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>

                            </tr>
                            </tbody>
                        </table>

                        <div class="text-center">
                            <button type="button" class="btn  btn-primary btn-rounded" title="Add Item" @click="addItem">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>





