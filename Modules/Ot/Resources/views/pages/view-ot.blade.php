<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 15/03/2018
 * Time: 22:28
 */

?>
@extends('layouts.master2')

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default card-view">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    @php $disabled = 'disabled' @endphp
                    @if($status == 1)
                        <div class="col-md-6">
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12 text-center">
                                        <a href="{{ route('ot.dispatch',$id_ot) }}" class="btn btn-primary">Crear Despacho <i class="fa fa-share-square"></i></a>
                                    </div>
                                    @if($cant_desp >0 )
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12 text-center">
                                        <a href="{{ route('returns.create',$id_ot) }}" class="btn btn-primary">Crear Devolución <i class="fa fa-undo"></i></a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    <div class="col-md-6">
                        @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor'))
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12 text-center">
                                        {!! Form::open(['route'=>'ot.change_status']) !!}
                                        <input type="hidden" name="ot_id" value="{{ $id_ot }}">
                                        <input type="hidden" name="status" value="2">
                                        <button class="btn btn-success btn-outline" type="submit">Pausar Ot <i class="fa fa-pause"></i></button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12 text-center">
                                        {!! Form::open(['route'=>'ot.change_status','id'=>'cerrar-ot']) !!}
                                        <input type="hidden" name="ot_id" value="{{ $id_ot }}">
                                        <input type="hidden" name="status" value="3">
                                        <button class="btn btn-danger btn-outline" type="submit">Cerrar Ot <i class="fa fa-close"></i></button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-lg-4 col-xs-12 text-center">
                                        {!! Form::open(['route'=>'ot.change_status']) !!}
                                        <input type="hidden" name="ot_id" value="{{ $id_ot }}">
                                        <input type="hidden" name="status" value="4">
                                        <button class="btn btn-danger btn-outline" type="submit">Eliminar Ot <i class="fa fa-trash"></i></button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    @endif
                    @if($status == 2)
                        <div class="well">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-center">
                                    <h4>La Ot se encuentra en modo pausado</h4>
                                    {!! Form::open(['route'=>'ot.change_status']) !!}
                                    <input type="hidden" name="ot_id" value="{{ $id_ot }}">
                                    <input type="hidden" name="status" value="1">
                                    <button class="btn btn-success" type="submit">Reactivar Ot</button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    @elseif($status == 3)
                        <div class="well">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-center">
                                    <h4>La Ot ha sido cerrada</h4>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    @foreach($ot_data as $o_d)
    @section('pageTitle','Ot #'.$o_d->code_ot)
        <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">

                    </div>
                    <div class="pull-right">
                        <h6>Cliente: {{ $o_d->fullname }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <div class="form-wrap">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-lg-6">
                                                <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600">Proyecto:</span></p><br>
                                                <p class="inline-block"><span>{{ $o_d->project_name }}</span></p>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-lg-6">
                                                <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600">Creado:</span></p><br>
                                                <p class="inline-block"><span>{{ $o_d->created_at }}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                                                <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600">Zona:</span></p><br>
                                                <p class="inline-block"><span>{{ $o_d->name }}</span></p>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                                                <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600">Lugar:</span></p><br>
                                                <p class="inline-block"><span>{{ $o_d->place }}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12">
                                                <p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600">Responsables del proyecto:</span></p><br>
                                                @foreach($u_ot as $uo)
                                                    <p class="inline-block"><span>{{ $uo->firstname }} {{ $uo->lastname }}</span></p>
                                                    <p class="pull-right"><span>{{ $uo->display_name }}</span></p><br>
                                                    <hr>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
            </div>
        </div>
            <div class="col-md-8 col-lg-8 col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-heading text-center">
                            <h6>Despachos creados</h6>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body" >
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-center" style="overflow:auto;height:200px;">
                                    @if($cant_desp > 0)
                                        <table class="tablas-ots-view table table-sm table-responsive">
                                            <thead>
                                            <tr>
                                                <th scope="col">id</th>
                                                <th scope="col">Estado</th>
                                                <th scope="col">Materiales</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($despachos as $d)
                                                <tr>
                                                    <td>{{ $d->id }}</td>
                                                    <td>@php
                                                            if($d->status == 1){ print '<span class="label label-primary">EN DESPACHO</span>'; }
                                                            elseif ($d->status == 2){ print'<span class="label label-danger">FINALIZADO</span>'; }
                                                            elseif ($d->status == 3){ print'<span class="label label-danger">ELIMINADO</span>'; }
                                                        @endphp
                                                    </td>
                                                <td>
                                                    @foreach ($d->status_item as $st)
                                                        {{ $st->inventory->inventoryitems->description }}
                                                    @endforeach
                                                </td>
                                                    <td><a href="{{ route('dispatch.view_dispatch', $d->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle" id="{{  $d->id }}"><i class="fa fa-pencil"></i></a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <p>La Ot aún no cuenta con despachos creados.</p>
                                    @endif
                                </div>
                                <div class="col-md-12 text-center">
                                        {{ $despachos->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-8 col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-heading text-center">
                            <h6>Devoluciones creadas</h6>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body" style="overflow:auto;height:200px;">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-center" style="overflow:auto;height:200px;">
                                    @if($cant_dev > 0)
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">id</th>
                                                <th scope="col">creado</th>
                                                <th scope="col">Estado</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($devoluciones as $dev)
                                                <tr>
                                                    <td>{{ $dev->id }}</td>
                                                    <td>{{ $dev->created_at }}</td>
                                                    <td>@php
                                                            if($dev->status == 1){ print '<span class="label label-primary">EN DESPACHO</span>'; }
                                                            elseif ($dev->status == 2){ print'<span class="label label-danger">FINALIZADO</span>'; }
                                                            elseif ($dev->status == 3){ print'<span class="label label-danger">ELIMINADO</span>'; }
                                                        @endphp</td>
                                                    <td><a href="{{ route('returns.view_return', $dev->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle" id="{{  $d->id }}"><i class="fa fa-pencil"></i></a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <p>La Ot aún no cuenta con devoluciones creadas.</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
@section('style')
@include('ot::layouts.style-ot')
@endsection

@section('script')
    <script>
        //funcion para confirmar antes de enviar un form
        jQuery.fn.confirmSubmit = function( message ) {
            $(this).submit( function( event ) {
                if( confirm(message) ) {
                    $(this).find('button[type="submit"]').prop( 'disabled', true );
                } else {
                    event.preventDefault();
                }
            });
        };


        $(document).ready(function () {
            $('#cerrar-ot').confirmSubmit( 'Estas seguro de continuar?' );
        })
    </script>
@endsection