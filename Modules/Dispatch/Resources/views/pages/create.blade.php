@extends('layouts.master')

@section('pageTitle', 'Crear Despacho')

@section('content')
    <div class="row"  id="appdispatch">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <!-- panel-heading -->
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Crear Despacho</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- end panel-heading -->

                <!-- wrapper body -->
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label">Seleccione la zona</label>
                                    <select name="zone" v-model="zone" class="form-control" data-live-search="true">
                                        @foreach($zones as $z)
                                        <option value="{{ $z->id }}">{{ $z->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6" v-if="_.isEmpty(code_ot) != true">
                                <div class="form-group">
                                    <label class="form-control-label">Seleccione la Ot</label>
                                    <select name="ot" class="form-control" data-style=" btn-default btn-outline" v-model="ot_id">
                                        <option value="">Seleccionar</option>
                                        <option v-for="cod in code_ot" v-if="_.isEmpty(cod.ot) != true" :value="cod.ot.id">@{{ cod.ot.code_ot }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="well">
                                <div class="row">
                                        <div class="col-md-3 text-center">
                                            <p>Código SKU.</p>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <p>Descripción</p>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <p>Cant. Solicitada</p>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <p>Cant. Disponible</p>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <p>Categoría</p>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <p>Inventario</p>
                                        </div>
                                    </div>
                                    <div class="row" v-if="ot_id">
                                        <!-- col-md-3 -->
                                        <div class="col-md-3">
                                            <div class="input-group" style="min-width:200px;">

                                                <template>
                                                    <basic-select :options="options" :selected-option="item[indice].inv_id" placeholder="introduzca code sku" @select="onSelect">
                                                    </basic-select>
                                                </template>
                                            </div>
                                        </div>
                                        <!-- end col -->

                                        <!-- col-md3 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control" v-model="item[indice].desc" disabled placeholder="descripción">
                                            </div>
                                        </div>
                                        <!-- end col-md3 -->
                                        <!-- col-md2 -->
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <!--input type="number" class="form-control" v-model="item[indice].cant_s"-->
                                                <input type="number" class="form-control" v-model="cant_sol">
                                            </div>
                                        </div>
                                        <!-- col-md2 -->
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <input type="number" class="form-control" v-model="item[indice].cant_d" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control" v-model="item[indice].categ" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" name="" class="form-control" v-model="item[indice].inventario" disabled>
                                            </div>
                                        </div>
                                    </div> <!-- row -->
                                    <div class="row text-center" v-if="valot">
                                        <button class="btn btn-primary" v-on:click="btn_add" :disabled="!btn_agregar">Agregar</button>
                                    </div>
                                    <div class="row"><!-- row -->
                                        <div class="card-block">
                                            <table class="table" v-if="indice > 0">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>SKU</th>
                                                        <th>Descripción</th>
                                                        <th>Cantidad Solicitada</th>
                                                        <th>Categoría</th>
                                                        <th>Inventario</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr is="list-item" v-for="(i, index) in item" :key="i.id" v-bind:ite="i" v-bind:index="index" @dlete="deleteitem"></tr>
                                                </tbody>
                                            </table>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Observación:</label>
                                                        <textarea class="form-control" rows="2" v-model="observacion"></textarea>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-control-label">Entregar a:</label>
                                                    <input type="text" class="form-control" v-model="entregar_a">
                                                    
                                                </div>
                                            </div>
                                            <div class="row text-center"  v-if="itemlength">
                                            <button class="btn btn-primary" v-on:click="saveitem" :disabled="loading">Guardar</button>
                                            </div>
                                        </div><!-- card-block -->
                                    </div><!-- end row-->
                        </div> <!-- well -->
                    </div>
                </div>
                <!-- end wrapper body -->
            </div>
        </div>
        <div>
            <!-- modal -->
                <div class="modal fade modal-flex" id="default-Modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">Pedir Prestado</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="col-md-12">
                                        <ul class="list-group" v-if="modal_other">
                                            <li class="list-group-item" is="list-prest" v-for="(item,index) in other_inv" v-bind:index="index" v-bind:datainv="item" v-bind:cantps="cant_ps" @prestado="idprestado">
                                            </li>
                                        </ul>
                                        <h6 v-else>Disculpe, no encontramos en otro inventario la cantidad restante para prestar.</h6>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary waves-effect waves-light ">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>
            <!-- end modal -->
        </div>
    </div>
@endsection
