<?php

namespace

Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Entities\User;
use Modules\Auth\Http\Requests\UserRequest;
use Nwidart\Modules\Routing\Controller;
use Modules\Auth\Entities\Role;
use DataTables;


class UserController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->can('read-users')){

            if ($request->ajax()){

                $users = User::with('roles')->select('users.*');

                return DataTables::eloquent($users)->addColumn('display_name', function (User $user) {
                    return $user->roles->map(function($role) {
                        return str_limit($role->display_name, 30, '...');
                    })->implode(', ');
                })->addColumn('action', 'auth::partials.button')->make(true);

            }

            return view('auth::user.index');

        }

        return redirect('auth/logout');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
       if (Auth::user()->can('create-users')){
		   $roles = Role::orderBy('display_name', 'asc')->pluck('display_name', 'id');
		   return view('auth::user.create', compact('roles'));
	   }
		return redirect('auth/logout');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
		if(Auth::user()->can('create-users')) {

			$data = User::create([
				'firstname' =>  $request->input('firstname'),
				'lastname'  =>  $request->input('lastname'),
				'username'  =>  $request->input('username'),
				'email'     =>  $request->input('email'),
				'phone'     =>  $request->input('phone'),
				'password'  =>  Hash::make($request->input('password')),
			]);

			$user = User::findOrFail($data->id);

			$data->attachRoles($request->input('role_id'));

			Session::flash('message', trans('auth::ui.user.message_create', array('name' => $user->firstname)));

			return redirect('auth/user/create');
		}

		return redirect('auth/logout');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('auth::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('update-users')){

			$user = User::findOrFail($id);

			$roles_user = User::find($id)->roles()->pluck('role_id')->toArray();

			$roles = Role::orderBy('display_name', 'asc')->pluck('display_name', 'id');

			return view('auth::user.edit', compact('user', 'roles', 'roles_user'));

		}
		return redirect('auth/login');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
		if(Auth::user()->can('update-users')) {
			$this->validate($request, [
				'firstname' => 'required',
				'lastname' => 'required',
				'username' => 'required',
				'email' => 'required|email|max:255',
				'role_id' => 'required'
			]);

			if ($request->input('password') != null){
				$request->except('password');
				$this->validate($request, [
					'password' => 'confirmed|min:5',
				]);
			}

			$data = array(
				'firstname' =>  $request->input('firstname'),
				'lastname'  =>  $request->input('lastname'),
				'username'  =>  $request->input('username'),
				'email'     =>  $request->input('email'),
				'password'  =>  Hash::make($request->input('password')),
			);

			$user = User::findOrFail($id);

			$user->update($data);

			if($user->roles->count()) {

				$user->roles()->detach($user->roles()->pluck('role_id')->toArray());

			}

			$user->attachRoles($request->input('role_id'));

			Session::flash('message', trans('auth::ui.user.message_update', array('name' => $user->firstname)));

			return redirect('auth/user');
		}

		return redirect('auth/logout');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
    	if ($this->user()->can('delete-users')){

    		$user = User::findOrFail($id);

    		$user->delete();

			return Response::json(array (
				'success' => true,
				'msg'     => 'El usuario ' . $user->full_name . ' ha sido eliminado.',
				'id'      => $user->id
			));
		}

		return Redirect::route('user.index');

	}

}
