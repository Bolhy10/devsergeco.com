const discard = new Vue({
    el:'#discard',
    data: function () {
        return {

            warehouses: [],

            discard: [],

            ingress: [],

            inventory:[],

            meta: {
                'current_page': 0,
                'from': 0,
                'last_page': 0,
                'path': '',
                'per_page': 0,
                'to': 0,
                'total' : 0
            },

            formDiscard:{
                receive_to: '',
                delivered: '',
                no_document: '',
                observations: '',
                clients: null,
                warehouses_clients: '',
                status: false,
                items:[]
            },

            fillDiscard: '',

            changeEdit: true
        }
    },

    created:function () {
        this.getDiscard();
    },

    computed:{

        isActived() {
            return this.meta.current_page;
        },
        pagesNumber(){

            if(!this.meta.to){
                return [];
            }

            var from = this.meta.current_page - 3;
            if(from < 1){
                from = 1;
            }

            var to = from + (3 * 2);
            if(to >= this.meta.last_page){
                to = this.meta.last_page;
            }

            var pagesArray = [];

            while(from <= to){
                pagesArray.push(from);
                from++;
            }

            return pagesArray;
        }

    },

    methods:{

        getDiscard(page){
            var url = '/inventory/api/discard?page='+page;
            axios.get(url).then(res => {
                this.discard = res.data.data;
                //Pagination
                this.meta = res.data.meta;
            });
        },

        getWare:function (id) {
            var url = '/inventory/api/warehouse/'+id;
            if (id > 0){
                axios.get(url).then(res => {

                    this.warehouses = res.data;

                }).catch(err => {
                    alert(err);
                });
            }
        },

        getItems:function (clients, warehouses) {

            var url = '/inventory/api/discard/items/'+clients+'/'+warehouses;

            if(clients != '' && warehouses != '') {

                axios.get(url).then(res => {

                    this.inventory = res.data.data;

                }).catch(err => {

                    $.toast('Error al capturar los items. Intente nuevamente.');

                });


            }else{

                $.toast('Debe escoger el Cliente y el Almacén para descartar items.');

            }

        },

        changePage:function (page) {
            this.meta.current_page = page;
            this.getDiscard(page);
        },

        deleteDiscard:function (discard) {
            var url = '/inventory/api/discard/'+discard.id;
            if(confirm('Seguro que desea eliminar el registro seleccionado.')){
                axios.delete(url).then(res => {

                    console.log(res.data);

                    this.getDiscard();
                    $.toast({
                        heading: 'Registro con #Doc:'+discard.no_document,
                        text: res.data.message,
                        position: 'top-right',
                        icon: 'success',
                    });

                }).catch(err => {
                    $.toast({
                        heading: 'Registro con #Doc:'+discard.no_document,
                        text: 'Error al eliminar el registro seleccionado.',
                        position: 'top-right',
                        icon: 'error'
                    });
                });
            };
        },

        pushItems:function (item) {

            if(item.items.id != ""){
                var hash = {};
                this.formDiscard.items = this.formDiscard.items.filter(function(current) {
                    var exists = !hash[current.items.id] || false;
                    hash[current.items.id] = true;
                    return exists;
                });

                for(var i=0;i<this.inventory.length;i++){
                    if(item.items.id == this.inventory[i]['items'].id){
                        item.items.description = this.inventory[i]['items'].description;
                        item.items.type.name = this.inventory[i]['items'].type.name;
                        item.items.inventory.available = this.inventory[i].available;
                    }
                }

                return;
            }

            $.toast("Debe seleccionar un CÓDIGO.");
        },

        addItem: function(){
            this.formDiscard.items.push(
                {
                    id: '',
                    items: {
                        id: '',
                        code: '',
                        description: '',
                        type: {
                            id: '',
                            name: ''
                        },
                        inventory: {
                            id: '',
                            available: '',
                            status: ''
                        }
                    },
                    quantity: '',
                    observations: ''
                }
            );
        },

        removeItem:function (item) {
            var index = this.formDiscard.items.indexOf(item);
            this.formDiscard.items.splice(index, 1);
        },

        compareQuantity:function (item) {

            if(item.quantity > item.items.inventory.available){

                item.quantity = '';

                $('#item'+item.items.id).focus();

                $.toast('La cantidad debe ser menor a la cantidad disponible.');

                return;
            }

            if(item.quantity == 0){

                item.quantity = '';
                $('#item'+item.items.id).focus();
                $.toast('La cantidad debe ser mayor a cero.');

                return;
            }

        },

        createSubmit:function () {

            if(this.formDiscard.items.length > 0){

                this.$validator.validateAll().then((result) => {
                    if (result) {

                        if(confirm('¿Se encuentra seguro de guardar el descarte?')){

                            var url = '/inventory/api/discard';

                            axios.post(url, this.formDiscard).then(res=>{

                                if(res.data.status == 200){

                                    if(confirm('¿Desea imprimir el formulario de descarte?.')){

                                        window.open('/inventory/generate/discard/1/'+res.data.id);
                                    }

                                    $.toast(res.data.message);

                                    this.cleanForm();

                                    return;
                                }

                            }).catch(error=>{

                                $.toast(error.response.data.message+' Verifique los campos requeridos.');

                                console.log(error);

                            });

                        }

                        return;
                    }

                    $.toast('Faltan campos por llenar. Verifique todos los campos y ITEMS de la tabla.');

                });

                return;
            }

            $.toast('Debe agregar ITEMS a su formulario de ingreso.')

        },

        cleanForm:function () {
            this.formDiscard.receive_to = '';
            this.formDiscard.delivered  = '';
            this.formDiscard.no_document = '';
            this.formDiscard.observations = '';
            this.formDiscard.clients = null;
            this.formDiscard.warehouses_clients = '';
            this.formDiscard.items = [];
        },

        updateBtn:function (discard) {

            this.fillDiscard = discard.id;

            this.changeEdit = false;

            this.getWare(discard.client.id);

            this.getItems(discard.client.id, discard.warehouse.id);

            this.formDiscard.receive_to = discard.receive_to;

            this.formDiscard.delivered = discard.delivered.id;

            this.formDiscard.no_document = discard.no_document;

            this.formDiscard.observations = discard.observations;

            this.formDiscard.clients = discard.client.id;

            this.formDiscard.warehouses_clients = discard.warehouse.id;

            this.formDiscard.items = discard.discarditems;

        },

        updateDiscard:function (id) {

            var url = '/inventory/api/discard/'+id;

            if(confirm('Desea actualizar el registro seleccionado?')){

                axios.put(url, this.formDiscard).then(res=>{

                    console.log(res);

                    if(res.data.status == 200){

                        this.changeEdit = true;

                        this.getDiscard();

                        this.cleanForm();

                        $.toast('Se actualizo el registro correctamente.');

                        return;
                    }

                    $.toast('Error al actualizar el registro. Intente nuevamente.');

                }).catch(err=>{

                    console.log(err);

                    $.toast('Error en el servidor:', err.response.data);

                });
            }
        },

        viewPrint:function (discard) {

            if(confirm("Seguro que deseas imprimir?. \n" +
                    "Al aceptar se procesa el descarte del sistema. \n" +
                    "Las siguientes impresiones seran copias. \n" +
                    "Asegúrese de imprimir el archivo original.")){

                this.getDiscard(2);

                window.open('/inventory/generate/discard/1/'+discard.id);

            }
        }

    }

});