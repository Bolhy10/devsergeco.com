<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 04/04/2018
 * Time: 3:36
 */

?>

<script>
    //usado en la vista de las devoluciones

    <!-- Script usado en los despachos -->

    var fin_devolucion = '{{ route('returns.end') }}';
    var borrar_devolucion = '{{ route('returns.delete') }}';
    var token = $('meta[name=csrf-token]').attr('content');

    $(document).ready(function(){
        // al hacer click en el boton de finalizar despacho
        $('#end_return').click(function () {
            var id = $('#id-devolucion').val();
            var pass = prompt('Introduzca la contraseña:');
            if(pass === 'sergeco2018')
            {
                $.post(fin_devolucion , { id: id, _token: token })
                    .done(function( data )
                    {
                        if(data.st === 1)
                        {
                            alert(data.ms);
                            location.reload();
                        }
                        else if(data.st === 0)
                        {
                            //se muestran los mensajes de error
                        }
                    });
            }
        });

        //al hacer click en el botón de eliminar despacho
        $('#borrar-devolucion').click(function(){
            var token = $('meta[name=csrf-token]').attr('content');
            var id_despacho = $('#id-devolucion').val();
            $.post(borrar_devolucion, {id:id_despacho, _token: token })
                .done(function (data)
                {
                    if(data.st === 1)
                    {
                        alert(data.ms);
                        window.location.replace('{{ route('ot.index') }}');
                    }
                });
        });
    });

    //fin
</script>
