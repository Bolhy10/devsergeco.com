<?php

namespace Modules\Inventory\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class InventoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i<100; $i++){
            DB::table('inventory')->insert(array(
                'inventoryitems_id' => $faker->numberBetween($min = 1, $max = 20),
                'clients_id' => $faker->numberBetween($min = 1, $max = 6 ),
                'warehouses_id' => $faker->numberBetween($min = 1, $max = 2 ),

                'quantity' => $faker->randomDigit,
                'status' => $faker->randomElement($array = array (1, 0)),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()

            ));
        }
    }
}
