
{!! Html::script('assets/js/jquery.min.js') !!}
{!! Html::script('assets/js/bootstrap.min.js') !!}


{!! Html::script('assets/js/jquery.slimscroll.js') !!}
{!! Html::script('assets/plugins/jquery.dataTables.min.js') !!}
{!! Html::script('assets/plugins/mindmup-editabletable.js') !!}
{!! Html::script('assets/plugins/jquery.bootstrap-touchspin.min.js') !!}
{!! Html::script('assets/plugins/bootstrap-select.min.js') !!}

{!! Html::script('assets/js/init.js') !!}


@yield('script')