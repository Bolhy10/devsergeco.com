<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Inventory\Entities\Inventorytypes;
use Modules\Inventory\Http\Requests\InventorytypesRequest;

class InventorytypesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->can('read-inventory')){

            $types = Inventorytypes::paginate(5);

            return view('inventory::type.index', compact('types'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->can('create-inventory')){
            return view('inventory::type.create');
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(InventorytypesRequest $request)
    {
        if (Auth::user()->can('create-inventory')){

            Inventorytypes::create($request->all());

            Session::flash('message', trans('inventory::ui.types.message_create', array('name'=> $request->type)));

            return redirect('inventory/types/create');
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('update-inventory')){

            $types = Inventorytypes::findOrFail($id);

            return view('inventory::type.edit', compact('types'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, InventorytypesRequest $request)
    {
        if (Auth::user()->can('update-inventory')){

            $types = Inventorytypes::findOrFail($id);

            $types->update($request->all());

            Session::flash('message', trans('inventory::ui.types.message_update', array('name' => $types->type)));

            return redirect('inventory/types');
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function notAuthenticate(){

        Session::flash('message_error', trans('auth::ui.user.message_not'));

    }

}
