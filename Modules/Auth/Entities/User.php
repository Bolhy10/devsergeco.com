<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;


    use SoftDeletes, EntrustUserTrait {
     SoftDeletes::restore as sfRestore;
     EntrustUserTrait::restore as euRestore;
    }

    const USER_VERIFIED = 'true';
    const USER_NOT_VERIFIED = 'false';


    protected $table = 'users';

    protected $fillable = ['firstname', 'lastname', 'username', 'email', 'password', 'phone', 'image', 'verified', 'verification_token'];

    protected $hidden = ['password', 'remember_token', 'verification_token'];

    protected $dates = ['deleted_at'];

    public function isVerified(){
        return $this->verified == User::USER_VERIFIED;
    }

    public static function generateVerificationToken(){
        return str_random(40);
    }

    //Relations
    public function roles() {
        return $this->belongsToMany('Modules\Auth\Entities\Role', 'role_user');
    }

    public function restore()
    {
        $this->sfRestore();
        $this->euRestore();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Devuelve las id de las ot relacionadas a un usuario
     */
    public function user_ot($id)
    {
        return $this->hasMany('Modules\Ot\Entities\Ot');
    }

    public function ingress(){
        return $this->hasMany('Modules\Inventory\Entities\Ingress');
    }

    public function discard(){
        return $this->hasMany('Modules\Inventory\Entities\Discard');
    }

}
