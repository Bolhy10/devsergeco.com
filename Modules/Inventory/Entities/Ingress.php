<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingress extends Model
{

    use SoftDeletes;

    const STATUS_IMPRESS = true;
    const STATUS_NOT_IMPRESS = false;

    protected $table = 'ingress';

    protected $fillable = [
        'clients_id',
        'warehouses_id',
        'warehouses_provider',
        'provider',
        'no_document',
        'no_order',
        'no_transfer',
        'users_id',
        'deliver_to',
        'observations',
        'status'
    ];

    public $hidden = ['clients_id', 'warehouses_id'];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public function isImpress(){

        return $this->status == Ingress::STATUS_IMPRESS;

    }

    public function ingressitems(){

        return $this->hasMany('Modules\Inventory\Entities\Ingressitems');

    }

    public function clients(){

        return $this->belongsTo('Modules\Clients\Entities\Clients');

    }

    public function users(){

        return $this->belongsTo('Modules\Auth\Entities\User');

    }

    public function warehouses(){

        return $this->belongsTo('Modules\Warehouses\Entities\Warehouses');

    }


}
