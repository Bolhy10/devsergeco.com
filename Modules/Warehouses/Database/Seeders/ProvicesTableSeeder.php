<?php

namespace Modules\Warehouses\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Warehouses\Entities\Provinces;

class ProvicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = [
            [
                'name' => 'Bocas del Toro',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1
            ],
            [
                'name' => 'Coclé',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1],
            [
                'name' => 'Colón',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1],
            [
                'name' => 'Chiriquí',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1],
            [
                'name' => 'Darién',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1],
            [
                'name' => 'Herrera',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1],
            [
                'name' => 'Los Santos',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1],
            [
                'name' => 'Panamá',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1
            ],
            [
                'name' => 'Veraguas',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1
            ],
            [
                'name' => 'Panamá Oeste',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1
            ]
        ];

        foreach ($provinces as $key=>$value){

            Provinces::create($value);
        }

    }
}
