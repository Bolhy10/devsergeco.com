<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDiscard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discard', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->integer('clients_id')->unsigned();
            $table->integer('warehouses_id')->unsigned();
            $table->string("no_document");
            $table->string('receive_to');
            $table->string('observations')->nullable();
            $table->boolean('status')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('clients_id')->references('id')->on('clients');
            $table->foreign('warehouses_id')->references('id')->on('warehouses');

        });


        Schema::create('discarditems', function (Blueprint $table){

            $table->increments('id');
            $table->integer('inventoryitems_id')->unsigned();
            $table->integer('discard_id')->unsigned();
            $table->integer('quantity');
            $table->string('observations')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('inventoryitems_id')->references('id')->on('inventoryitems');
            $table->foreign('discard_id')->references('id')->on('discard');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discarditems');
        Schema::drop('discard');
    }
}
