<?php

namespace Modules\Clients\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Clients\Entities\Clients;
use Modules\Clients\Http\Requests\ClientsRequest;
use Modules\Warehouses\Entities\Warehouses;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        if (Auth::user()->can('read-clients')){

            $clients = Clients::with('warehouses')->paginate(5);

            return view('clients::index', compact('clients'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->can('create-clients')){

            $warehouses = Warehouses::pluck('name', 'id');

            return view('clients::partials.create', compact('warehouses'));
        }


        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ClientsRequest $request)
    {
        if (Auth::user()->can('create-clients')){

            $data = Clients::create($request->all());

            Clients::findOrFail($data->id);

            $data->warehouses()->attach($request->input('warehouses_id'));

            Session::flash('message', trans('clients::ui.clients.message_create', array('name'=> $request->shortname)));

            return redirect('clients/create');
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('update-clients')){

            $clients = Clients::findOrFail($id);

            $clients_warehouses = Clients::find($id)->warehouses()->pluck('warehouses_id')->toArray();

            $warehouses = Warehouses::orderBy('name', 'asc')->pluck('name', 'id');

            return view('clients::partials.edit', compact('clients', 'warehouses', 'clients_warehouses'));
        }

        $this->notAuthenticate();
        return redirect('dashboard');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, ClientsRequest $request)
    {
        if (Auth::user()->can('update-clients')){

            $clients = Clients::findOrFail($id);

            $clients->update($request->all());


            if($clients->warehouses->count()) {

                $clients->warehouses()->detach($clients->warehouses()->pluck('warehouses_id')->toArray());
            }

            $clients->warehouses()->attach($request->input('warehouses_id'));

            Session::flash('message', trans('clients::ui.clients.message_update', array('name' => $clients->shortname)));

            return redirect('clients');
        }

        $this->notAuthenticate();
        return redirect('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function notAuthenticate(){

        Session::flash('message_error', trans('auth::ui.user.message_not'));

    }

}
