@extends('layouts.master')

@section('pageTitle', trans('ui.menu_action.data.permissions.title'))

@section('style')
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			@include('partials.message')

			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">{{ trans('auth::ui.permission.names') }}</h6>
					</div>

					@if(Auth::user()->can('create-permissions'))
						<a href="{{ url('auth/permission/create') }}"
						   class="btn btn-success pull-right">
							{{ trans("auth::ui.permission.button_add") }}
						</a>
					@endif
					<div class="clearfix"></div>
				</div>

				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-wrap">
							<div class="table-responsive">
								<table id="simpletable" class="table table-striped table-bordered" >
									<thead>
									<tr>
										<th>{{ trans('auth::ui.permission.name_label') }}</th>
										<th>{{ trans('auth::ui.permission.display') }}</th>
										<th>{{ trans('auth::ui.permission.description') }}</th>
										@if(Auth::user()->can(['update-permissions', 'delete-permissions']))
											<th>{{ trans('auth::ui.permission.operation_label') }}</th>
										@endif
									</tr>
									</thead>
									<tfoot>
									<tr>
										<th>{{ trans('auth::ui.permission.name_label') }}</th>
										<th>{{ trans('auth::ui.permission.display') }}</th>
										<th>{{ trans('auth::ui.permission.description') }}</th>
										@if(Auth::user()->can(['update-permissions', 'delete-permissions']))
											<th>{{ trans('auth::ui.permission.operation_label') }}</th>
										@endif
									</tr>
									</tfoot>
									<tbody>
									@foreach($permissions as $permission)
										<tr>
											<td>{{ $permission->name }}</td>
											<td>{{ $permission->display_name }}</td>
											<td>{{ $permission->description }}</td>
											@if(Auth::user()->can(['update-permissions', 'delete-permissions']))
												<td>
													@if(Auth::user()->can('update-permissions'))
														<a href="{{ url('auth/permission/' . $permission->id . '/edit') }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
															<i class="fa fa-pencil"></i>
														</a>
													@endif

													@if(Auth::user()->can('delete-permissions'))
														<a href="#" data-url="{{ url('auth/permission/'.$permission->id) }}"
														   class="btn btn-default btn-sm btn-icon-anim btn-circle"
														   v-on:click.prevent="deleteUser({{ $permission }})">
															<i class="fa fa-trash"></i>
														</a>
													@endif
												</td>
											@endif
										</tr>
									@endforeach

									</tbody>
								</table>

								{!! $permissions->render() !!}

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@section('script')
	{!! Html::script('assets/js/user-init.js') !!}
@stop
