<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventoryitems extends Model
{
    use SoftDeletes;


    protected $fillable = ['code_sku', 'description', 'inventorytypes_id', 'check_unique', 'status'];

    public function inventorytypes(){
        return $this->belongsTo('Modules\Inventory\Entities\Inventorytypes');
    }

    public function inventory()
    {
        return $this->hasOne('Modules\Inventory\Entities\Inventory');
    }

    public function ingressitems(){

        return $this->hasMany('Modules\Inventory\Entities\Ingressitems');

    }

    public function discarditems(){

        return $this->hasMany('Modules\Inventory\Entities\Discarditems');

    }


}
