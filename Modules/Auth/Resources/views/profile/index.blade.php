@extends('layouts.master')


@section('content')

    <div class="row">

        {{--<div class="col-lg-3 col-xs-12">--}}
            {{--<div class="panel panel-default card-view  pa-0">--}}
                {{--<div class="panel-wrapper collapse in">--}}
                    {{--<div class="panel-body  pa-0">--}}

                        {{--@include('auth::profile.photo')--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="col-md-8 col-lg-offset-2 col-xs-12">

            @include('partials.message')

            @include('errors.form_error')

            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pb-0">

                        <div class="col-md-12">

                            @include('auth::profile.form')

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    @stop