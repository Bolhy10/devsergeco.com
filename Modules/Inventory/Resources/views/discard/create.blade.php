@extends('layouts.master')

@section('pageTitle', trans('inventory::ui.discard.create.title'))

@section('content')

    <form method="POST" @submit.prevent="createSubmit" id="discard">
        <div class="row mb-10">
            <div class="col-lg-12">
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">
                        Guardar Descarte
                    </button>
                    <a href="{{ route('inventory.discard') }}" class="btn btn-warning">
                        Volver
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            @include('inventory::discard.form')
            @include('inventory::discard.table')
        </div>
    </form>

@stop