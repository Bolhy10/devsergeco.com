@extends('layouts.master')

@section('pageTitle', trans('warehouses::ui.zones.title'))


@section('content')
    <div class="row">
        <div class="col-md-12">

            @include('partials.message')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('warehouses::ui.zones.subtitle') }}</h6>
                    </div>

                    @if(Auth::user()->can('create-zones'))
                        <a href="{{ route('zones.create') }}" class="btn btn-success pull-right">
                            {{ trans("warehouses::ui.zones.new_zones") }}
                        </a>
                    @endif

                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if(count($zones) < 1)
                            <div class="alert alert-warning alert-dismissable alert-style-1">
                                <i class="fa fa-warning"></i>{{ trans('inventory::ui.items.error.not_items') }}
                            </div>
                        @endif

                        @if(count($zones) > 0)
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="simpletable" class="table table-striped table-bordered" >
                                    <thead>
                                    <tr>
                                        <th>{{ trans('warehouses::ui.zones.name') }}</th>
                                        <th>{{ trans('warehouses::ui.zones.address') }}</th>
                                        <th>{{ trans('warehouses::ui.zones.provinces_id') }}</th>
                                        <th>{{ trans('warehouses::ui.zones.status') }}</th>

                                        @if(Auth::user()->can(['update-zones', 'delete-zones']))
                                            <th>{{ trans('warehouses::ui.zones.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>{{ trans('warehouses::ui.zones.name') }}</th>
                                        <th>{{ trans('warehouses::ui.zones.address') }}</th>
                                        <th>{{ trans('warehouses::ui.zones.provinces_id') }}</th>
                                        <th>{{ trans('warehouses::ui.zones.status') }}</th>

                                        @if(Auth::user()->can(['update-zones', 'delete-zones']))
                                            <th>{{ trans('warehouses::ui.zones.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($zones as $zone)
                                        <tr>
                                            <td>{{ $zone->name }}</td>
                                            <td>{{ $zone->address }}</td>
                                            <td><span class="badge badge-primary">
                                                    {{ $zone->provinces->name }}
                                                </span>
                                            </td>
                                            <td>
                                                @if($zone->status == 1)
                                                    <span class="badge badge-success">
                                                    {{ "Activo" }}
                                                    </span>
                                                @endif
                                                @if($zone->status == 0)
                                                    <span class="badge badge-danger">
                                                    {{ "Desactivado" }}
                                                    </span>
                                                @endif
                                            </td>

                                            @if(Auth::user()->can(['update-zones', 'delete-zones']))
                                                <td>
                                                    @if(Auth::user()->can('update-zones'))
                                                        <a href="{{ url('zones/' . $zone->id . '/edit') }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif

                                                    @if(Auth::user()->can('delete-zones'))
                                                        <a href="#" data-url="{{ url('auth/user/'.$zone->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                {!! $zones->render() !!}

                            </div>
                        </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    @stop