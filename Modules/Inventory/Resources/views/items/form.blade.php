<div class="form-group">
    <label class="control-label mb-10">{{ trans('inventory::ui.items.sku') }}</label>
    {!! Form::text('code_sku', null, ['class' => 'form-control', 'placeholder'=> trans('inventory::ui.items.sku')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('inventory::ui.items.description') }}</label>
    {!! Form::text('description', null, ['class' => 'form-control', 'placeholder'=> trans('inventory::ui.items.description')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('inventory::ui.items.types') }}</label>
    {!! Form::select('inventorytypes_id', $types, null, ['class' => 'form-control', 'placeholder' => 'Seleccione el tipo']) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('inventory::ui.items.status') }}</label>
    {!! Form::select('status', ['1' => 'Activo', '0' => 'Desactivado'], null, ['class'=> 'form-control','placeholder' => 'Seleccione el estado']) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('inventory::ui.items.unique') }}</label>
    {!! Form::select('check_unique', ['0' => 'No', '1' => 'Si'], null, ['class'=> 'form-control']) !!}
</div>

<div class="form-group mb-0">

    {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}

    <a href="{{ route('items.index') }}" class="btn btn-warning">{{ trans('auth::ui.general.back') }}</a>

</div>