<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 28/02/2018
 * Time: 22:22
 */
?>

<script>

    var route_getitem = '{{ route('ot.getitem') }}' ;  //obtener información de un item en especifico.

    var route_saved = '{{ route('ot.save_dispatch') }}'; //almacenar un despacho

    var route_info = '{{ route('ui.info') }}';


    //variable global glob_row, almacena el número de filas que se van creando
    var glob_row = 1;
    var btn_pres = 0;


    $(document).ready(function(){
        /* Bootstrap-TouchSpin Init*/
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'fa fa-plus',
            verticaldownclass: 'fa fa-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }

        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();

        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });

        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });

        $(document).on('click','.prestamo',function(){
            $('#modalinv').modal();
        });

        /**Al cambiar el valor del select de categoria**/
        $(document).on('change','.code',function () {
            //dato id
            var id_text = $(this).attr('id');
            //id de la fila
            var id_row = id_text.substr(4,1);
            //id del inventario
            var id_item = $(this).val();
            var description = '#description'+id_row;
            var categoria = '#categoria'+id_row;
            var inventario = '#inventario'+id_row;
            var cantidad = '#cantidad'+id_row;
            var disp = '#disp'+id_row;
            var medida = '#medida'+id_row;
            var token = $('meta[name=csrf-token]').attr('content');
            var qnt = $(cantidad).val();

            var form_cantidad = '#form-group-cantidad'+id_row;

            $.post(route_getitem , { iditem: id_item, qnt: qnt, _token: token })
                .done(function( data ) {
                    console.log(data);
                    //for (var i in data){
                        $(description).text(data['description']);
                        $(categoria).text(data['type']);
                        $(inventario).text(data['fullname']);
                        $(disp).text(data['quantity']);
                        if(data['type_id'] === 2)
                        {
                            $(medida).text('mt');
                        }
                        else
                        {
                            $(medida).text('un');
                        }
                    //}
                    var cant_disp = +($(disp).text());
                    //validar si la cantidad solicitada es mayor a 0
                    if(qnt > 0){
                        //validar si la cantidad solicitada es mayor que la disponible
                        if(qnt > cant_disp)
                        {
                            alert('La cantidad solicitada excede de la disponible')
                            $(form_cantidad).addClass('has-error');
                        }
                    }

                    /*VALIDACION PARA MOSTRAR INFORMACION DEL ITEM DEL INVENTARIO DE ITEMS UNICOS**/
                    //validar si la cantidad disponible es mayor a 0
                    if(cant_disp > 0)
                    {
                        //si es del tipo de cables o de los transformadores
                        if(data['type'] == "Cables" || data['type'] == "Tranformador")
                        {
                            var med = '';
                            $("#ui_info").html('');
                            $("#ui_type").text(data['description']);
                            if(data['type'] == "Cables"){ med="mts"; }else{ med="un"; }
                            //
                            $.post(route_info , { id: data['id'] , _token: token })
                            .done(function( data ) 
                            {
                                for(var i in data)
                                {
                                    $("#ui_info").append(
                                        '<tr>'+
                                        '<td>'+ data[i].nombre +'</td>'+
                                        '<td>'+ data[i].codigounico +'</td>'+
                                        '<td>'+ data[i].cantidad +' '+med+'</td>'+
                                        'tr'
                                    );
                                }
                                $('#modaluinv').modal();
                            });
                        }
                    }
                });

        });

        $('form').bind("keypress", function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });

        /**
         * Se valida que las cantidades de los materiales sean mayor que 0
         **/
        $( "form" ).submit(function( event ) {
            //event.preventDefault();
            var cant_inp = $('input[name="cantidad[]"]').length;
            //alert(cant_inp);
            //for (var i = 0; i< )
            //$(this).submit();
        });
        /**
         * Al momento de enfocar el campo de cantidad se debe de validar si la cantidad es > 0
         **/
        $(document).on('input','.cantidad',function()
        {
            var id_text = $(this).attr('id');
            var id_row = parseInt(id_text.substr(8,1));
            //alert(id_row);
            var code_id = '#code'+id_row;
            var disp = '#disp'+id_row;
            var cant_d = +($(disp).text());
            var id_item = $(code_id).val();
            var cantidad = +($(this).val());
            var table = $('#body-items');
            var form_cantidad = '#form-group-cantidad'+id_row;

            console.log('cantidad: '+cantidad+' '+'cantidad disponible'+' '+cant_d);
            if(cantidad > cant_d || cantidad < 0)
            {
                alert('cantidad excede, disminuya o tomarlo prestado de otro inventario de la bodega');
            }

            $('.cantidad').keypress(function(e)
            {
                cantidad = +($(this).val());
                if(e.which === 13)
                {
                    if(cant_d >= 0){
                        if(cantidad > cant_d || cantidad < 0)
                        {
                            alert('cantidad excede, disminuya o tomarlo prestado de otro inventario de la bodega');
                            $(form_cantidad).addClass('has-error');
                        }
                        else if((cantidad <= cant_d) && (cantidad > 0))
                        {
                            //si el usuario regresa a una fila creada para modificar, no se añade otro campo
                            if(id_row >= glob_row){
                                $(form_cantidad).removeClass('has-error');
                                glob_row ++;
                                $(table).append('<tr id="row-inv-'+glob_row+'">' +
                                    '<td>'+glob_row+'</td>' +
                                    '<td>' +
                                        '<div class="col-md-3">'+
                                        '<div class="input-group my-group">'+
                                    '<select id="code'+glob_row+'" name="id[]" data-width="120px" class="form-control form-control-sm selectpicker code" data-style=" btn-default btn-outline" data-live-search="true">' +
                                    '<option value="Seleccione"></option>' +
                                        @foreach($inventario as $ti)
                                            '<option value="{{ $ti->id }}">{{$ti->code_sku}} {{$ti->description}}</option>' +
                                        @endforeach
                                            '</select>' +
                                        '<span class="input-group-btn">'+
                                        '<button id="presta'+glob_row+'" class="btn btn-primary btn-xs prestamo" type="button" data-toggle="popover" data-trigger="hover" data-content="Tomar material prestado de otro inventario">' +
                                        '<i class="fa fa-exchange"></i>' +
                                        '</button>'+
                                        '</span>'+
                                        '</div>'+
                                        '</div>'+
                                    '</td>' +
                                    '<td>' +
                                    '<p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="description'+glob_row+'"></span></p>' +
                                    '</td>' +
                                    '<td>' +
                                    '<div class="form-group text-center">' +
                                    '<input id="cantidad'+glob_row+'" class="form-control form-control-sm cantidad" type="text" value="" name="cantidad[]">' +
                                    '<span id="medida'+glob_row+'"></span>'+
                                    '</div>' +
                                    '</td>' +
                                    '<td>' +
                                    '<p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="disp'+glob_row+'"></span></p>' +
                                    '</td>' +
                                    '<td>' +
                                    '<p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="categoria'+glob_row+'"></span></p>' +
                                    '</td>' +
                                    '<td>' +
                                    '<p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="inventario'+glob_row+'"></span></p>' +
                                    '</td>' +
                                    '</tr>');
                                var select = $('#code'+glob_row);
                                select.selectpicker('refresh');
                            }
                        }
                    }

                }
            });

            $(document).on('click','#add-material',function(){

                var id_row = glob_row;

                var code_id = '#code'+id_row;
                var disp = '#disp'+id_row;
                var cant_d = +($(disp).text());
                var cant = '#cantidad'+id_row;
                var cantidad = $(cant).val();


                if(cant_d >= 0){
                    if(cantidad > cant_d || cantidad < 0)
                    {
                        alert('cantidad excede, disminuya o tomarlo prestado de otro inventario de la bodega');
                        $(form_cantidad).addClass('has-error');
                    }
                    else if((cantidad <= cant_d) && (cantidad > 0))
                    {
                        //si el usuario regresa a una fila creada para modificar, no se añade otro campo
                        if(id_row >= glob_row){
                            $(form_cantidad).removeClass('has-error');
                            glob_row ++;
                            $(table).append('<tr id="row-inv-'+glob_row+'">' +
                                '<td>'+glob_row+'</td>' +
                                '<td>' +
                                '<div class="col-md-3">'+
                                '<div class="input-group my-group">'+
                                '<select id="code'+glob_row+'" name="id[]" data-width="120px" class="form-control form-control-sm selectpicker code" data-style=" btn-default btn-outline" data-live-search="true">' +
                                '<option value="Seleccione"></option>' +
                                    @foreach($inventario as $ti)
                                        '<option value="{{ $ti->id }}">{{$ti->code_sku}} {{$ti->description}}</option>' +
                                    @endforeach
                                        '</select>' +
                                '<span class="input-group-btn">'+
                                '<button id="presta'+glob_row+'" class="btn btn-primary btn-xs prestamo" type="button" data-toggle="popover" data-trigger="hover" data-content="Tomar material prestado de otro inventario">' +
                                '<i class="fa fa-exchange"></i>' +
                                '</button>'+
                                '</span>'+
                                '</div>'+
                                '</div>'+
                                '</td>' +
                                '<td>' +
                                '<p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="description'+glob_row+'"></span></p>' +
                                '</td>' +
                                '<td>' +
                                '<div class="form-group text-center">' +
                                '<input id="cantidad'+glob_row+'" class="form-control form-control-sm cantidad" type="text" value="" name="cantidad[]">' +
                                '<span id="medida'+glob_row+'"></span>'+
                                '</div>' +
                                '</td>' +
                                '<td>' +
                                '<p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="disp'+glob_row+'"></span></p>' +
                                '</td>' +
                                '<td>' +
                                '<p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="categoria'+glob_row+'"></span></p>' +
                                '</td>' +
                                '<td>' +
                                '<p class="inline-block"><span class="capitalize-font txt-primary mr-5 weight-600" id="inventario'+glob_row+'"></span></p>' +
                                '</td>' +
                                '</tr>');
                            var select = $('#code'+glob_row);
                            select.selectpicker('refresh');
                        }
                    }
                }
            });

        });
        //Pedir prestamo(click en el formulario de item)
        $(document).on('click','.prestamo',function(){
            var tag_id = $(this).attr('id');
            var row_id = tag_id.substr(6,1);
            btn_pres = row_id;
        });

        //Pedir prestado de otro inventario (click en el modal)
        $('#add-prestamo').click(function () {
            $('#modalinv').modal('hide');
            //id del inventario a tomar prestado
            var url = '{{ route('ot.obtener_inventario',[ 'w_id' => ':wid' , 'c_id' => ':cid' ]) }}';
            var id_inventario = $('#inventario_id').val();
            var id_bodega = $('#bodega_id').val();
            var url1 = url.replace(':wid', id_bodega);
            var url_f = url1.replace(':cid', id_inventario);
            var select = '#code'+btn_pres;

            $.get( url_f, function( data ) {
                console.log(data);
                $(select).html(' ');

                $(select).append(
                    '<option value=" "> Seleccione un material </option>'
                );

                for(var i = 0; i < data.length; i++ )
                {
                    $(select).append(
                        '<option value="'+ data[i]['id'] +'">'+ data[i]['code_sku'] + data[i]['description'] +'</option>'
                    )
                }
                $(select).selectpicker('refresh');
            });
        });

        $(document).on('click','#save_dispatch',function(){

        });

    });

</script>