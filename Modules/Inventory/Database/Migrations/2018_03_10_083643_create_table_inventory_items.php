<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInventoryItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventoryitems', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('inventorytypes_id')->unsigned();
            $table->string('code_sku')->unique();
            $table->boolean('check_unique');
            $table->string('description');
            $table->integer('status');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('inventoryitems', function (Blueprint $table){
            $table->foreign('inventorytypes_id')->references('id')->on('inventorytypes');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventoryitems');
    }
}
