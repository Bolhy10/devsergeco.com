<ul class="nav navbar-right top-nav pull-right">
    <li class="dropdown auth-drp">
        <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="{{ asset('assets/images/elements/avatar.png') }}" alt="user_auth" class="user-auth-img img-circle"><span class="user-online-status"></span></a>
        <ul class="dropdown-menu user-auth-dropdown">


            @foreach(trans('ui.option_user.data') as $list=>$value)

            <li>
                <a href="{{ route($value['url']) }}">
                    <i class="{{ $value['icon'] }}"></i>
                    <span>
                        {{ $value['title'] }}
                    </span>
                </a>
            </li>

            @endforeach

        </ul>
    </li>
</ul>