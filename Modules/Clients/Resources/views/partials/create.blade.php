@extends('layouts.master')

@section('style')
    {!! Html::style('assets/plugins/jquery-multi-select/css/multi-select.css') !!}
@stop

@section('pageTitle', trans('clients::ui.title'))

@section('content')

    <div class="row">
        <div class="col-md-8 col-lg-offset-2">

            @include('partials.message')

            @include('errors.form_error')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('clients::ui.clients.create.title') }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <div class="form-wrap">
                            {!! Form::open(array('route'=>'clients.store')) !!}

                            @include('clients::partials.form', ['button' => trans('clients::ui.clients.create.btn_add')])

                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
    {!! Html::script('assets/plugins/jquery-multi-select/js/jquery.multi-select.js') !!}
    {!! Html::script('assets/js/query-init.js') !!}
@stop