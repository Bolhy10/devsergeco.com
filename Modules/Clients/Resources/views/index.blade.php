@extends('layouts.master')

@section('pageTitle', trans('clients::ui.title'))

@section('content')

    <div class="row">
        <div class="col-md-12">

            @include('partials.message')



            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('clients::ui.clients.title') }}</h6>
                    </div>

                    @if(Auth::user()->can('create-clients'))
                        <a href="{{ route('clients.create') }}" class="btn btn-success pull-right">
                            {{ trans("clients::ui.clients.new_client") }}
                        </a>
                    @endif

                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if(count($clients) < 1)
                            <div class="alert alert-warning alert-dismissable alert-style-1">
                                <i class="fa fa-warning"></i>{{ trans('inventory::ui.items.error.not_items') }}
                            </div>
                        @endif

                        @if(count($clients) > 0)

                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="simpletable" class="table table-striped table-bordered" >
                                    <thead>
                                    <tr>
                                        <th>{{ trans('clients::ui.clients.shortname') }}</th>
                                        <th>{{ trans('clients::ui.clients.fullname') }}</th>
                                        <th>{{ trans('clients::ui.clients.phone') }}</th>
                                        <th>{{ trans('clients::ui.clients.warehouses') }}</th>
                                        <th>{{ trans('clients::ui.clients.status') }}</th>

                                        @if(Auth::user()->can(['update-clients', 'delete-clients']))
                                            <th>{{ trans('clients::ui.clients.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>{{ trans('clients::ui.clients.shortname') }}</th>
                                        <th>{{ trans('clients::ui.clients.fullname') }}</th>
                                        <th>{{ trans('clients::ui.clients.phone') }}</th>
                                        <th>{{ trans('clients::ui.clients.warehouses') }}</th>
                                        <th>{{ trans('clients::ui.clients.status') }}</th>

                                        @if(Auth::user()->can(['update-clients', 'delete-clients']))
                                            <th>{{ trans('clients::ui.clients.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($clients as $client)
                                        <tr>
                                            <td>{{ $client->shortname }}</td>
                                            <td>{{ $client->fullname }}</td>
                                            <td>{{ $client->phone }}</td>

                                            <td>
                                                @foreach($client->warehouses as $warehouse)
                                                    <span class="badge badge-primary mb-5">
													{{ $warehouse->name }}
													</span>
                                                @endforeach
                                            </td>

                                            <td>
                                                @if($client->status == 1)
                                                    <span class="badge badge-success">
                                                    {{ "Activo" }}
                                                    </span>
                                                @endif
                                                @if($client->status == 0)
                                                    <span class="badge badge-danger">
                                                    {{ "Desactivado" }}
                                                    </span>
                                                @endif
                                            </td>

                                            @if(Auth::user()->can(['update-clients', 'delete-clients']))
                                                <td>
                                                    @if(Auth::user()->can('update-clients'))
                                                        <a href="{{ url('clients/' . $client->id . '/edit') }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif

                                                    @if(Auth::user()->can('delete-clients'))
                                                        <a href="#" data-url="{{ url('clients/'.$client->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                {!! $clients->render() !!}

                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>

        </div>
    </div>

@stop
