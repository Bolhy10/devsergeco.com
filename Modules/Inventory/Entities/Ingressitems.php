<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingressitems extends Model
{
    use SoftDeletes;

    protected $table = "ingressitems";

    protected $fillable = [
        'inventoryitems_id',
        'ingress_id',
        'quantity',
        'observations'
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];


    public function ingress(){

        return $this->belongsTo('Modules\Inventory\Entities\Ingress');
    }

    public function inventoryitems(){

        return $this->belongsTo('Modules\Inventory\Entities\Inventoryitems');
    }


}
