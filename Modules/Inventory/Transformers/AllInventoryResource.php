<?php

namespace Modules\Inventory\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Clients\Transformers\ClientsResource;
use Modules\Warehouses\Transformers\WarehousesResource;

class AllInventoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'available' => $this->quantity,
            'status' => $this->status,
            'created_at' => $this->created_at->format('Y/M/d h:m:s'),
            'updated_at' => $this->updated_at->format('Y/M/d h:m:s'),
            'items' => new InventoryitemsResource($this->whenLoaded('inventoryitems')),
            'client' => new ClientsResource($this->whenLoaded('clients')),
            'warehouse' => new WarehousesResource($this->whenLoaded('warehouses')),
        ];
    }
}
