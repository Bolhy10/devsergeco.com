<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIngress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingress', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('clients_id')->unsigned();
            $table->integer('warehouses_id')->unsigned();

            $table->string('provider')->nullable();
            $table->string('warehouses_provider')->nullable();
            $table->integer('no_document');
            $table->string('deliver_to');

            $table->string('no_order');
            $table->string('no_transfer');

            $table->integer('users_id')->unsigned();

            $table->string('observations')->nullable();
            $table->boolean('status')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clients_id')->references('id')->on('clients');
            $table->foreign('warehouses_id')->references('id')->on('warehouses');
            $table->foreign('users_id')->references('id')->on('users');

        });

        Schema::create('ingressitems', function (Blueprint $table){

            $table->increments('id');

            $table->integer('inventoryitems_id')->unsigned();
            $table->integer('ingress_id')->unsigned();
            $table->integer('quantity');
            $table->string('observations')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('inventoryitems_id')->references('id')->on('inventoryitems');
            $table->foreign('ingress_id')->references('id')->on('ingress');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingressitems');
        Schema::drop('ingress');
    }

}
