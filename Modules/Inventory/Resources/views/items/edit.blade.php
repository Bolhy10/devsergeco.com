@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-8 col-lg-offset-2">

            @include('partials.message')

            @include('errors.form_error')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('inventory::ui.items.update.title') }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">

                            {!! Form::model($items, ['method' => 'PUT', 'route' => ['items.update', $items->id]]) !!}

                            @include('inventory::items.form', array('items' => $items) + compact('types'), ['button' => trans('inventory::ui.items.update.btn_update')])

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
