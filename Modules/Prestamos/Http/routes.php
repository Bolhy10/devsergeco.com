<?php

Route::group(['middleware' => 'web', 'prefix' => 'prestamos', 'namespace' => 'Modules\Prestamos\Http\Controllers'], function()
{
    Route::any('/', 'PrestamosController@index')->name('prestamos.index');

    Route::get('/data_table/bodega/{id}','PrestamosController@data_table')->name('prestamos.table');

    //ver el prestamo solicitado
    Route::get('/view/{id}','PrestamosController@show')->name('prestamos.view');

    //devolver material recibido
    Route::get('/devolver/{id}','PrestamosController@return')->name('prestamos.return');
    
});
