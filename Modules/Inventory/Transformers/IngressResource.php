<?php

namespace Modules\Inventory\Transformers;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;
use Modules\Auth\Transformers\UsersResource;
use Modules\Clients\Transformers\ClientsResource;
use Modules\Warehouses\Transformers\WarehousesResource;


class IngressResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'provider' => $this->provider,
            'warehouses_provider' => $this->warehouses_provider,
            'no_document' => $this->no_document,
            'deliver_to' => $this->deliver_to,
            'no_order' => $this->no_order,
            'no_transfer' => $this->no_transfer,
            'observations' => $this->observations,
            'status' => $this->status,
            'created_at' => $this->created_at->format('Y/m/d'),
            'updated_at' => $this->updated_at->format('Y/m/d'),
            'client' => new ClientsResource($this->whenLoaded('clients')),
            'receive_to' => new UsersResource($this->whenLoaded('users')),
            'warehouse' => new WarehousesResource($this->whenLoaded('warehouses')),

            'ingressitems'     =>  IngressitemsResource::collection($this->whenLoaded('ingressitems'))

        ];
    }
}