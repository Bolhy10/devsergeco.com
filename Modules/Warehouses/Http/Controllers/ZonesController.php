<?php

namespace Modules\Warehouses\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Warehouses\Entities\Zones;
use Modules\Warehouses\Entities\Provinces;
use Modules\Warehouses\Http\Requests\ZonesRequest;

class ZonesController extends Controller
{

    public function notAuthenticate(){
        Session::flash('message_error', trans('auth::ui.user.message_not'));
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->can('read-zones')){

            $zones = Zones::with('provinces')->paginate(5);

            return view('warehouses::zones.index', compact('zones'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->can('create-zones')){
            $provinces = Provinces::pluck('name', 'id');
            return view('warehouses::zones.create', compact('provinces'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ZonesRequest $request)
    {
        if (Auth::user()->can('create-zones')){

            Zones::create($request->all());

            Session::flash('message', trans('warehouses::ui.warehouses.message_create', array('name'=> $request->name)));

            return redirect('zones/create');
        }

        $this->notAuthenticate();
        return redirect('dashboard');

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('update-zones')){

            $zones = Zones::findOrFail($id);

            $provinces = Provinces::orderBy('name', 'asc')->pluck('name', 'id')->toArray();

            return view('warehouses::zones.edit', compact('zones', 'provinces'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, ZonesRequest $request)
    {

        if (Auth::user()->can('update-zones')){

            $zones = Zones::findOrFail($id);

            $zones->update($request->all());

            Session::flash('message', trans('warehouses::ui.zones.message_update', array('name' => $zones->name)));

            return redirect('zones');
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
