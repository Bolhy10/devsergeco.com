@extends('layouts.master')

@section('pageTitle', 'Inventario general')

@section('content')

    <div id="inventory">
        <div class="row">

            @include('inventory::layouts.filter')

            @include('inventory::layouts.search')

            <div class="col-lg-12 col-xs-12">
            <div class="panel panel-default card-view">

                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('inventory::ui.inventory.title') }}</h6>
                    </div>
                    <div class="pull-right">

                        <a href="#" class="pull-left inline-block mr-15" @click.prevent="printPDF()">
                            <i class="fa fa-download" v-if="allInventory.length > 1 && code == ''"></i>
                        </a>

                        <a href="#" class="pull-left inline-block full-screen mr-15" v-if="allInventory.length > 0">
                            <i class="fa fa-arrows-alt"></i>
                        </a>

                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">

                                @include('inventory::layouts.table')

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>


@stop
