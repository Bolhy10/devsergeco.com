@extends('layouts.master')


@section('pageTitle', trans('ui.menu_action.data.users.title'))

@section('style')
    {!! Html::style('assets/plugins/sweetalert/dist/sweetalert.css') !!}
    @stop

@section('content')

    <div class="row" id="">
        <div class="col-md-12">

            @include('partials.message')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('auth::ui.user.title') }}</h6>
                    </div>

                    @if(Auth::user()->can('create-users'))
                    <a href="{{ url('auth/user/create') }}" class="btn btn-success pull-right">
                        {{ trans("auth::ui.user.button_add") }}
                    </a>
                    @endif

                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">

                                <table class="table table-striped" id="user-datable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ trans('auth::ui.user.firstname') }}</th>
                                        <th>{{ trans('auth::ui.user.lastname') }}</th>
                                        <th>{{ trans('auth::ui.user.username') }}</th>
                                        <th>{{ trans('auth::ui.user.email') }}</th>
                                        <th>{{ trans('auth::ui.role.names') }}</th>
                                        @if(Auth::user()->can(['update-users', 'delete-users']))
                                            <th>{{ trans('auth::ui.user.operation_label') }}</th>
                                            @endif
                                    </tr>
                                    </thead>
                                </table>

                                <data-table :columns="['id', 'name', 'username']" :ajax="{!! route('user.index') !!}"></data-table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @stop

@section('script')
    <script>
        $(function () {
            $('#user-datable').DataTable({
                processing: true,
                serverSide:true,
                language:{
                    'url': '{!! asset('assets/plugins/datatable/spanish.json') !!}'
                },
                ajax: '{!! route('user.index') !!}',
                columns:[
                    {data: 'id', name: 'users.id'},
                    {data: 'firstname', name: 'users.firstname'},
                    {data: 'lastname', name: 'lastname'},
                    {data: 'username', name: 'username'},
                    {data: 'email', name: 'email'},
                    {data: null, render: function (data) {
                            return '<span class="badge badge-primary">'+data.display_name+'</span>'
                        }, orderable:false, searchable:false},

                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });

    </script>
@stop


{{--https://github.com/yajra/laravel-datatables/issues/1632--}}