<div class="row" v-if="changeEdit">
    <div class="col-md-12">

        <div class="panel panel-default card-view">

            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">{{ trans('inventory::ui.discard.title') }}</h6>
                </div>

                @if(Auth::user()->can('create-discard'))
                    <a href="{{ route('discard.create') }}" class="btn btn-success pull-right">
                        {{ trans("inventory::ui.discard.new_discard") }}
                    </a>
                @endif

                <div class="clearfix"></div>
            </div>

            <div class="panel-wrapper collapse in">
                <div class="panel-body">

                    <div class="alert alert-warning alert-dismissable alert-style-1" v-if="discard.length < 1">
                        <i class="fa fa-warning"></i>{{ trans('inventory::ui.items.error.not_items') }}
                    </div>

                    <div class="table-wrap" v-if="discard.length  > 0">
                        <div class="table-responsive">
                            <table id="simpletable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>{{ trans('inventory::ui.discard.index') }}</th>
                                    <th>{{ trans('inventory::ui.discard.created_at') }}</th>
                                    <th>{{ trans('inventory::ui.discard.clients') }}</th>
                                    <th>{{ trans('inventory::ui.discard.receive_to') }}</th>
                                    <th>{{ trans('inventory::ui.discard.delivered') }}</th>

                                    <th>{{ trans('inventory::ui.discard.no_document') }}</th>

                                    <th>{{ trans('inventory::ui.discard.status') }}</th>

                                    @if(Auth::user()->can(['update-discard', 'delete-discard']))
                                        <th>{{ trans('inventory::ui.discard.operation_label') }}</th>
                                    @endif
                                </tr>
                                </thead>

                                <tbody>
                                <tr v-for="(disc, index) in discard" :key="disc.id" v-if="disc.discarditems.length > 0">
                                    <td> @{{ disc.id }}</td>
                                    <td> @{{ disc.created_at }}</td>
                                    <td> @{{ disc.client.shortname }}</td>
                                    <td> @{{ disc.receive_to }}</td>
                                    <td> @{{ disc.delivered.username }}</td>
                                    <td> @{{ disc.no_document }}</td>
                                    <td>
                                        <span v-if="disc.status == 0" class="badge badge-primary">Guardado</span>
                                        <span v-if="disc.status == 1" class="badge badge-success">Impresa</span>
                                    </td>
                                    @if(Auth::user()->can(['update-discard', 'delete-discard']))
                                        <td>
                                            @if(Auth::user()->can('update-discard'))
                                                <a href="#" @click.prevent="updateBtn(disc)" v-if="disc.status == 0" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @endif

                                            @if(Auth::user()->can('read-discard'))
                                                <a href="#" @click.prevent="viewPrint(disc)" v-if="disc.status == 0" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            @endif

                                            @if(Auth::user()->can('read-discard'))
                                                <a :href="'generate/discard/2/'+disc.id" v-if="disc.status == 1" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            @endif

                                            @if(Auth::user()->can('delete-discard'))
                                                <a href="#" @click.prevent="deleteDiscard(disc)" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>{{ trans('inventory::ui.discard.index') }}</th>
                                    <th>{{ trans('inventory::ui.discard.created_at') }}</th>
                                    <th>{{ trans('inventory::ui.discard.clients') }}</th>
                                    <th>{{ trans('inventory::ui.discard.receive_to') }}</th>
                                    <th>{{ trans('inventory::ui.discard.delivered') }}</th>

                                    <th>{{ trans('inventory::ui.discard.no_document') }}</th>

                                    <th>{{ trans('inventory::ui.discard.status') }}</th>
                                    @if(Auth::user()->can(['update-discard', 'delete-discard']))
                                        <th>{{ trans('inventory::ui.discard.operation_label') }}</th>
                                    @endif
                                </tr>
                                </tfoot>

                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <ul class="pagination" v-if="discard.length > 0">
                <li v-if="meta.current_page > 1">
                    <a href="#" @click.prevent="changePage(meta.current_page - 1)" rel="prev">«</a>
                </li>

                <li v-for="page in pagesNumber" :class="[ page == isActived ? 'active': '']">
                    <a href="#" @click.prevent="changePage(page)">@{{ page }}</a>
                </li>

                <li v-if="meta.current_page < meta.last_page" disabled="">
                    <a href="#" @click.prevent="changePage(meta.current_page + 1)" rel="next">»</a>
                </li>
            </ul>

        </div>

    </div>
</div>