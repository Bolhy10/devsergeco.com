<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.user.firstname') }}</label>
	{!! Form::text('firstname', null, ['class' => 'form-control', 'placeholder'=> 'Introduzca el nombre']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.user.lastname') }}</label>
	{!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder'=> 'Introduzca el apellido']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.user.username') }}</label>
	{!! Form::text('username', null, ['class' => 'form-control', 'placeholder'=> 'Nombre del usuario']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.user.email') }}</label>
	{!! Form::text('email', null, ['class' => 'form-control', 'placeholder'=> 'Nombre el correo']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.user.phone') }}</label>
	{!! Form::text('phone', null, ['class' => 'form-control', 'placeholder'=> trans('auth::ui.user.phone')]) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.user.password') }}</label>
	{!! Form::password('password', ['class' => 'form-control', 'placeholder'=> 'Contraseña']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.user.password_confirmation') }}</label>
	{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder'=> 'Confirme la contraseña']) !!}
</div>

<div class="form-group col-md-12">
	<div class="col-md-6 col-lg-offset-3">
		<label class="control-label mb-10 text-center"><strong>{{ trans('auth::ui.role.names') }}</strong></label>
		{!! Form::select('role_id[]', $roles, isset($roles_user) ? $roles_user : null, array(
		'multiple' => true, 'class' => 'multi-select', 'id' => 'roleSelect')) !!}
	</div>

</div>

<div class="form-group mb-0">
	{!! Form::submit($button, ['class' => 'btn btn-primary']) !!}

	<a href="{{ route('user.index') }}" class="btn btn-warning">{{ trans('auth::ui.general.back') }}</a>
	
</div>
