<?php

Route::group(['middleware' => 'web', 'prefix' => 'dispatch', 'namespace' => 'Modules\Dispatch\Http\Controllers'], function()
{
    Route::any('/', 'DispatchController@index')->name('dispatch.index');

    //vista de despachos
    Route::get('/ver-despacho/{id}','DispatchController@show')->name('dispatch.view_dispatch');

    //ver recursos para editar un item
    Route::get('/editar-item/{id}','StatusItemController@edit')->name('statusitem.edit_item');

    //actualizar un item de un despacho
    Route::post('actualizar-item','StatusItemController@update')->name('statusitem.update');

    //eliminar un item de un despacho
    Route::get('/borrar-item/{id}','StatusItemController@destroy')->name('dispatch.delete_item');

    //finalizar un despacho
    Route::post('/finalizar-despacho','DispatchController@end_dispatch')->name('dispatch.end');

    //eliminar un despacho
    Route::post('/borrar-despacho','DispatchController@destroy')->name('dispatch.delete');

    //consultar data_table de despachos
    Route::any('/tabla/despacho/bodega/{id}','DispatchController@data_table')->name('dispatch.table');

    //editar un despacho
    Route::get('/{id}/editar','DispatchController@edit')->name('dispatch.edit');

    //crear un despacho
    Route::get('/create','DispatchController@create')->name('dispatch.create');

    //guardar un despacho
    Route::post('/save','DispatchController@store')->name('dispatch.store');

    //obtener las Ots en base a la zona
    Route::get('/getots/{id}','DispatchController@getOtByZone')->name('dispatch.otbyzone');

    Route::get('/getotinventory/{ot_id}', 'DispatchController@getInventoryOts')->name('dispatch.inventoryots');

    //buscar items con mas cantidad en otros inventarios
    Route::get('/searchsku/{sku}/{cantps}/{clientid}','DispatchController@searchSku')->name('dispatch.searchsku');

    //obtener la infomracion de un item del inventario por su id
    Route::get('/info_item/{id}', 'DispatchController@getDataItem')->name('dispatch.infoitem');

    Route::post('/new_dispatch','DispatchController@saveNewDispatch')->name('dispatch.new_save');

});
