<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 19/03/2018
 * Time: 23:56
 */
?>
@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-right">
                        <h6>OT #{{ $data->ot->code_ot }}</h6>
                    </div>
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Despacho #{{ $data->id }}</h6>
                        @if ($data->status == 2 && $data->prints == 0)
                            <p class="text-danger">Despacho finalizado</p>
                        @elseif($data->status == 2 && $data->prints > 0)
                            <p class="text-danger">Despacho finalizado, el documento ya ha sido impreso.</p>
                        @elseif($data->status == 3)
                            <p class="text-danger">El despacho ha sido eliminado</p>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Código</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Cantidad Solicitada</th>
                                @if($data->status == 1)
                                    <th scope="col">Cant. Disponible</th>
                                @elseif($data->status == 2)
                                    <th scope="col">Cant. Restante</th>
                                @endif
                                <th scope="col">Categoría</th>
                                <th scope="col">Inventario</th>
                                @if($data->status == 1)
                                <th></th>
                                <th></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody id="body-items">
                            @php $cont = 1; @endphp
                            @foreach($data->status_item as $d_d)
                                <tr>
                                    <td>{{ $cont }}</td>
                                    <td>{{$d_d->inventory->inventoryitems->code_sku}}</td>
                                    <td>{{ $d_d->inventory->inventoryitems->description}}</td>
                                    <td id="cant{{ $d_d->id }}">{{ $d_d->quantity }}</td>
                                    <td>{{ $d_d->inventory->quantity }}</td>
                                    <td>{{ $d_d->inventory->inventoryitems->inventorytypes->type }}</td>
                                    <td>{{ $d_d->inventory->clients->fullname }}</td>
                                    @if($data->status == 1)
                                        <td>
                                            <a @php $ot->stot->status != 1 ? print 'disabled' : print ' '; @endphp
                                               href="{{ route('statusitem.edit_item',$d_d->id) }}" class="btn btn-success btn-sm btn-icon-anim btn-circle"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td>
                                            <a @php $ot->stot->status != 1 ? print 'disabled' : print ' '; @endphp
                                               href="{{ route('dispatch.delete_item', $d_d->id) }}" class="btn btn-danger btn-sm btn-icon-anim btn-circle"><i class="fa fa-trash"></i></a>
                                        </td>
                                    @endif
                                </tr>
                                @php $cont++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-outline" href="{!! URL::previous() !!}"> REGRESAR </a>
                                    @if($despacho->status === 1)
                                        @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor') )
                                        <button @php $ot->stot->status != 1 ? print 'disabled' : print ' '; @endphp class="btn btn-success" id="end_dispatch">FINALIZAR E IMPRIMIR</button>
                                    @endif
                                        <input type="hidden" id="id-despacho" value="{{ $data->id }}">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                @php //se valida si el estado del despacho es 'en despacho' => 1 @endphp
                                @if($data->status === 1)
                                    @php //si el estado de la ot es distinta a 1   @endphp
                                    @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor') )
                                        <button @php $ot->stot->status != 1 ? print 'disabled' : print ' '; @endphp class="btn btn-danger pull-right" id="borrar-despacho">ELIMINAR DESPACHO</button>
                                    @endif
                                @elseif($data->status === 2)
                                    <a class="btn btn-success btn-outline" href="{{ route( 'ot.prints',[ 'option' => 1,'id' => $data->id ]) }}" target="_blank">IMPRIMIR</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('dispatch::layouts.script_dispatch')
@endsection

