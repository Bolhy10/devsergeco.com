<script>
$(document).ready(function() {
    //tabla de los despachos
    $('#datable_despacho').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '{{ $ruta_dt }}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'code_ot', name: 'code_ot' },
            { data: 'zone', name:'zone' },
            { data: 'project_name', name: 'project_name' },
            { data: 'materiales', name: 'materiales' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' }
        ],
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

    });

    $('#datable_despacho tbody').on( 'click', 'tr', function () {
        var id = $(this).attr('id');
        var route = '{{ route("dispatch.view_dispatch",['id' => ':id']) }}';
        var route_f = route.replace(':id', id);

        window.location.replace(route_f);
    } );

    //tabla de las devoluciones
    $('#datable_devolucion').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '{{ route('returns.table') }}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'code_ot', name: 'code_ot' },
            { data: 'project_name', name: 'project_name' },
            { data: 'materiales', name: 'materiales' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' }
        ],

        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

    });

    //DATATABLE DE LOS PRESTAMOS

    $('#datable_prestamos').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '{{ $ruta_dt }}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'code_ot', name: 'code_ot' },
            { data: 'inv_o', name:'inv_o' },
            { data: 'inv_r', name: 'inv_r' },
            { data: 'description', name: 'description'},
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' }
        ],

        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

    });

     $('#datable_prestamos tbody').on( 'click', 'tr', function () {
        var id = $(this).attr('id');
        var route = '{{ route("prestamos.view",['id' => ':id']) }}';
        var route_f = route.replace(':id', id);

        window.location.replace(route_f);
    } );

} );

</script>