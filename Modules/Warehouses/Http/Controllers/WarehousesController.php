<?php

namespace Modules\Warehouses\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Warehouses\Entities\Warehouses;
use Modules\Warehouses\Entities\Zones;
use Modules\Warehouses\Http\Requests\WarehousesRequest;

class WarehousesController extends Controller
{



    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->can('read-warehouses')){

            $warehouses = Warehouses::with('zones')->paginate(5);

            return view('warehouses::warehouses.index', compact('warehouses'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->can('create-warehouses')){
            $zones = Zones::pluck('name', 'id');
            return view('warehouses::warehouses.create', compact('zones'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(WarehousesRequest $request)
    {
        if (Auth::user()->can('create-warehouses')){

            Warehouses::create($request->all());

            Session::flash('message', trans('warehouses::ui.warehouses.message_create', array('name'=> $request->name)));

            return redirect('warehouses/create');
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {

        if (Auth::user()->can('update-warehouses')){

            $warehouses = Warehouses::findOrFail($id);

            $zones = Zones::orderBy('name', 'asc')->pluck('name', 'id')->toArray();

            return view('warehouses::warehouses.edit', compact('warehouses', 'zones'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, WarehousesRequest $request)
    {
        if (Auth::user()->can('update-warehouses')){

            $warehouses = Warehouses::findOrFail($id);

            $warehouses->update($request->all());

            Session::flash('message', trans('warehouses::ui.warehouses.message_update', array('name' => $warehouses->name)));

            return redirect('warehouses');

        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function notAuthenticate(){
        Session::flash('message_error', trans('auth::ui.user.message_not'));
    }
}
