import { BasicSelect } from 'vue-search-select'

Vue.component('list-item', {
    props: ['ite', 'index'],
    template: '<tr v-if="ite.cant_s > 0">' +
        '<th><span class="badge">{{ index + 1 }}</span></th>' +
        '<td>{{ ite.sku }}</td>' +
        '<td>{{ ite.desc }}</td>' +
        '<td>{{ ite.cant_s }}</td>' +
        '<td>{{ ite.categ }}</td>' +
        '<td>{{ ite.inventario }}</td>' +
        '<td><button @click="deleteit(index)" class="btn btn-danger btn-sm btn-icon-anim btn-circle"><i class="fa fa-close"></i></button></td>' +
        '</tr>',
        methods: {
            deleteit(index){
                this.$emit('dlete', index);
            }
        }
});

Vue.component('list-prest', {
    props: ['index', 'datainv', 'cantps'],
    template: '<li class="list-group-item text-center"><p>Inventario: {{datainv.clients.shortname}}</p><p>Cantidad disponible: {{datainv.quantity}}</p></div><div><a href="javascript:void(0)" @click="add_prestado(datainv)" class="">Agregar</a></div></li>',
    methods: {
        add_prestado(data) {
            this.$emit('click');
            this.$emit('prestado', data.id);
        }
    }
});

var appdispatch = new Vue({
    el: '#appdispatch',
    data: function () {
        return {
            csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
            zone: '',
            code_ot: [],
            ot_id: '',
            inventoryOt: [],        //almacena datos de la ot y el inventario del cliente
            options: [],            //almacena las opciones del select en codigo_sku
            inventory_id: '',
            searchText: '',
            indice: 0,             //indice que indica el número del item o la fila
            data_item: [],
            item: [
                {
                    inv_id: {         //value: id del inventario
                        value: '',
                        text: ''
                    },
                    desc: '',
                    cant_s: '',
                    cant_d: '',
                    categ: '',
                    inventario: '',
                    sku: '',
                    clients_id: ''
                }
            ],
            cant_sol: '',
            other_inv: [],
            modal_other: false,
            cantidad_ps: '',
            found_other: false,
            info_item: [],
            cant_tt: 0,
            cant_ps: '',
            observacion: '',
            entregar_a: '',
            loading: false,
            btn_agregar: false,
        };
    },
    computed: {
        itemlength: function() {
            if (this.indice == 0 ){
                return false;
            }else {
                return true;
            }
        },
        valot: function() {
            if (this.ot_id > 0){
                return true;
            }else{
                return false;
            }
        },
    },
    watch: {
        zone: function (val) {
            this.getOtCode(val);
            this.ot_id = '';
        },
        //escucha los cambios realizados en el select de las zonas(Panama, Colón)
        ot_id: function (val) {
            this.getOtInventary(val);
        },
        inventory_id: function (val) {
            this.getDescription(val.value);
        },
        cant_sol(val) {
            this.item[this.indice].cant_s = val;
            if(val > 0) {
                this.btn_agregar = true;
            }else{
                this.btn_agregar = false;
            }
        },
    },
    methods: {
        getOtCode(id_zone) {
            var url = '/dispatch/getots/' + id_zone;
            axios.get(url).then(res => {
                this.code_ot = res.data;
            });
        },
        //función que consulta los items disponibles en el inventario de la Ot y los imprime en el select.
        getOtInventary(ot_id) {
            var url = '/dispatch/getotinventory/' + ot_id;
            axios.get(url).then(res => {
                this.inventoryOt = res.data;
                Object.values(this.inventoryOt.clients.inventory).forEach(element => {
                    this.options.push({ value: element.id, text: element.inventoryitems.code_sku + ' - ' + element.quantity })
                });
            });
        },
        getDescription(inv_id) {
            Object.values(this.inventoryOt.clients.inventory).forEach(element => {
                if (element.id === inv_id) {
                    this.item[this.indice].desc = element.inventoryitems.description;
                    this.item[this.indice].cant_d = element.quantity;
                    this.item[this.indice].categ = element.inventoryitems.inventorytypes.type;
                    this.item[this.indice].sku = element.inventoryitems.code_sku;
                }
            });
            this.item[this.indice].inventario = this.inventoryOt.clients.shortname;
            this.item[this.indice].clients_id = this.inventoryOt.clients.id;
        },
        //agregar un item y sumar al indice un valor
        push_item(){
            this.item.push({
                inv_id: {
                    value: '',
                    text: ''
                },
                desc: '',
                cant_s: '',
                cant_d: '',
                categ: '',
                inventario: '',
                sku: ''
            })
            this.indice += 1;
        },
        //funciones del select de vue que se activa al seleccionar un item y muestra los datos del mismo
        onSelect(item) {
            this.item[this.indice].inv_id = item;
            this.inventory_id = this.item[this.indice].inv_id;   //id del inventario
        },
        reset() {
            this.item[this.indice].inv_id = {}
        },
        selectOption() {
            // select option from parent component
            this.item[this.indice].inv_id = this.options[0]
        },
        //funcion para agregar un item al despacho
        btn_add() {
            let existe = false;
            //-----
            axios.get('/dispatch/info_item/' + this.inventory_id.value).then(res => {
                //console.log(res);
                this.info_item = res.data;
            });

            Object.keys(this.item).forEach(element => {
                if (this.indice == element) {
                    
                } else {
                    if ((this.item[this.indice].inv_id.value === /*element.inv_id.value*/ this.item[element].inv_id.value) && (this.item[element].cant_s > 0)) {
                        let cants_a = +this.item[this.indice].cant_s;
                        let cants_e = +this.item[element].cant_s;
                        let cant_t = cants_a + cants_e;
                        if (cant_t <= this.info_item.quantity) {
                            
                            this.item[element].cant_s = cant_t;
                            Vue.set(appdispatch.item, this.indice, {
                                inv_id: {
                                    value: '',
                                    text: ''
                                },
                                desc: '',
                                cant_s: '',
                                cant_d: '',
                                categ: '',
                                inventario: '',
                                sku: ''
                            });
                            existe = true;
                        } else {
                            var code_sku = this.item[this.indice].sku;
                            this.cantidad_ps = this.item[this.indice].cant_s - this.item[this.indice].cant_d;     // cantidad prestada solicitada
                            this.searchOtherInventary(code_sku, this.cantidad_ps, this.item[this.indice].clients_id);
                        }
                    }
                }
            });
            //-----
            if (!existe) {
                if (this.item[this.indice].cant_s <= this.item[this.indice].cant_d) {
                    this.item.push({
                        inv_id: {
                            value: '',
                            text: ''
                        },
                        desc: '',
                        cant_s: '',
                        cant_d: '',
                        categ: '',
                        inventario: '',
                        sku: ''
                    });
                    this.indice += 1;
                } else {
                    var code_sku = this.item[this.indice].sku;
                    this.cantidad_ps = this.item[this.indice].cant_s - this.item[this.indice].cant_d;     // cantidad prestada solicitada
                    this.searchOtherInventary(code_sku, this.cantidad_ps, this.item[this.indice].clients_id);
                }
            }
        },

        searchOtherInventary(sku, cant_ps, client_id) {
            this.cant_ps = cant_ps;
            var url = "/dispatch/searchsku/" + sku + '/' + cant_ps + '/' + client_id;
            axios.get(url).then(res => {
                this.other_inv = res.data;
                if(this.other_inv.length > 0){
                    this.modal_other = true;
                }else{
                    this.modal_other = false;
                }
                $("#default-Modal").modal('show');
            });
        },
        idprestado: function (id) {
            let id_inv = id;  //id del inventario
            let c_p = this.cant_ps;  // cantidad prestada
            let c_disp = this.item[this.indice].cant_d;

            //si la cantidad disponible es 0, se agrega el item y uno en blanco.
            if (c_disp == 0) {

                axios.get('/dispatch/info_item/' + id_inv).then(res => {
                    //console.log(res);
                    this.info_item = res.data;
                    this.item[this.indice].desc = this.info_item.inventoryitems.description;
                    this.item[this.indice].cant_d = this.info_item.quantity;
                    this.item[this.indice].categ = this.info_item.inventoryitems.inventorytypes.type;
                    this.item[this.indice].sku = this.info_item.inventoryitems.code_sku;
                    this.item[this.indice].inventario = this.info_item.clients.shortname;
                    this.item[this.indice].clients_id = this.info_item.clients.id;
                    this.item[this.indice].cant_s = this.cant_ps;
                    this.item[this.indice].inv_id = { value: this.info_item.id, text: this.info_item.inventoryitems.code_sku + ' - ' + this.info_item.quantity };

                    //se agrega un nuevo item en blanco

                    this.push_item();

                });
            } else if(c_disp > 0){
                
                this.item[this.indice].cant_s = this.item[this.indice].cant_d;

               this.push_item();

                axios.get('/dispatch/info_item/' + id_inv).then(res => {
                    //console.log(res);
                    this.info_item = res.data;
                    this.item[this.indice].desc = this.info_item.inventoryitems.description;
                    this.item[this.indice].cant_d = this.info_item.quantity;
                    this.item[this.indice].categ = this.info_item.inventoryitems.inventorytypes.type;
                    this.item[this.indice].sku = this.info_item.inventoryitems.code_sku;
                    this.item[this.indice].inventario = this.info_item.clients.shortname;
                    this.item[this.indice].clients_id = this.info_item.clients.id;
                    this.item[this.indice].cant_s = this.cant_ps;
                    this.item[this.indice].inv_id = { value: this.info_item.id, text: this.info_item.inventoryitems.code_sku + ' - ' + this.info_item.quantity };
                    this.push_item();
                });
            }

            //luego se agrega un nuevo item en blanco al array
            $("#default-Modal").modal('hide');
        },
        saveitem: function(){
            this.loading = true;
            axios.post('/dispatch/new_dispatch', {_token: this.csrf ,data: this.item, observacion: this.observacion, entregar_a: this.entregar_a, ot_id: this.ot_id})
            .then(function (response) {
                if(response.data != false){
                    window.location.href = response.data;
                }else{
                    alert('ha ocurrido un error');
                    this.loading = false;
                }
              })
              .catch(function (error) {
                console.log(error);
              });
            
        },
        deleteitem: function(index) {
            var indx = +index;
            if(indx == 0  && this.index == 0){
                appdispatch.item.splice(indx,1);
                appdispatch.item.push({
                    inv_id: {
                        value: '',
                        text: ''
                    },
                    desc: '',
                    cant_s: '',
                    cant_d: '',
                    categ: '',
                    inventario: '',
                    sku: ''
                });
            } else {
                appdispatch.item.splice(indx,1);
                this.indice --;
            }
        }
    },
    components: {
        BasicSelect
    }
});
