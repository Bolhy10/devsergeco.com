<?php

namespace Modules\Warehouses\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provinces extends Model
{

    use SoftDeletes;

    protected $table = 'provinces';

    protected $fillable = ['name', 'status'];

    protected $dates = ['deleted_at'];

    //Relations

    public function zones(){

        return $this->hasMany('Modules\Warehouses\Entities\Zones');
    }

}