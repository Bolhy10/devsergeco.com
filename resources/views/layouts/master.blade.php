<!DOCTYPE html>
<html lang="es">
    <head>
        @include('partials.header')
        @include('partials.style')
    </head>
    <body>

    <div class="wrapper  theme-1-active primary-color-blue">
        @include('partials.header_top')
        @include('partials.sidebar')

        <div class="page-wrapper" style="min-height: 800px;">
            <div class="container-fluid">

                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">@yield('pageTitle')</h5>
                    </div>
                </div>

                @yield('content')

                @include('partials.footer')

            </div>
        </div>
    </div>

    @include('partials.script')



    </body>
</html>