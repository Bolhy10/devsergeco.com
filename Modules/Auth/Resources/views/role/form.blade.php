<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.role.name') }}</label>
	{!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=> 'Rol']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.role.display') }}</label>
	{!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder'=> 'Introduzca el nombre']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.role.description') }}</label>
	{!! Form::text('description', null, ['class' => 'form-control', 'placeholder'=> 'Introduzca una breve descripcion']) !!}
</div>

<div class="form-group col-md-12">
	<div class="col-md-6 col-lg-offset-3">
		<label class="control-label mb-10 text-center" for="permissionSelect">
			<strong>{{ trans('auth::ui.permission.names') }}</strong></label>

		{!! Form::select('permission_id[]', $permissions, isset($role_permission) ? $role_permission : null,
		array('multiple' => true, 'class' => 'multi-select', 'id' => 'permissionSelect')) !!}

	</div>
</div>

<div class="form-group mb-0">

	{!! Form::submit($button, ['class' => 'btn btn-primary']) !!}

	<a href="{{ route('role.index') }}" class="btn btn-warning">{{ trans('auth::ui.general.back') }}</a>

</div>
