<?php

namespace Modules\Inventory\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Warehouses\Transformers\WarehousesResource;

class InventoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'available' => $this->quantity,
            'warehouse' => new WarehousesResource($this->whenLoaded('warehouses')),
            'items' => new InventoryitemsResource($this->whenLoaded('inventoryitems')),
            'status' => $this->status
        ];
    }
}
