<?php

Route::group(['middleware' => 'web', 'prefix' => 'inventory', 'namespace' => 'Modules\Inventory\Http\Controllers'], function()
{

    Route::resource('types', 'InventorytypesController', ['except' => 'show']);

    Route::resource('items', 'InventoryitemsController', ['except' => 'show']);

    Route::resource('all', 'InventoryController', ['except' => 'show', 'edit', 'create', 'delete', 'post']);

    Route::get('ingress', 'IngressController@viewIngress')->name('inventory.ingress');

    Route::get('discard', 'DiscardController@viewDiscard')->name('inventory.discard');

    Route::get('generate/ingress/{type}/{id}', 'IngressController@generateIngress')->name('ingress.generate');

    Route::get('generate/discard/{type}/{id}', 'DiscardController@generateDiscard')->name('discard.generate');

    Route::get('generate/all/{clients?}/{warehouses?}', 'InventoryController@createPDF')->name('inventory.generate');


    Route::post('items-import', 'InventoryitemsController@itemsImport')->name('items.import');

    Route::group(['prefix' => 'api'],function (){

        Route::resource('ingress', 'IngressController', ['except' => 'show', 'edit']);

        Route::resource('discard', 'DiscardController', ['except' => 'show', 'edit']);

        Route::get('warehouse/{id}', 'IngressController@ware');

        Route::get('items', 'IngressController@items');

        Route::get('discard/items/{clients}/{warehouses}', 'DiscardController@items');

        Route::get('all/{clients?}/{warehouses?}', 'InventoryController@inventoryAll');

        Route::post('search/items', 'InventoryController@searchItems');

    });

});
