<?php

namespace Modules\InventarioUnico\Entities;

use Illuminate\Database\Eloquent\Model;

class uistatus extends Model
{
    protected $table = 'uistatus';
    protected $fillable = [
        'uinventary_id',
        'dispatch_id',
        'quantity',
        'status'
    ];

    public function uinventary()
    {
        return $this->belongsTo('Modules\InventarioUnico\Entities\Uinventary');
    }
}
