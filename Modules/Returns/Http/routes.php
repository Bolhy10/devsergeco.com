<?php

Route::group(['middleware' => 'web', 'prefix' => 'returns', 'namespace' => 'Modules\Returns\Http\Controllers'], function()
{
    Route::get('/', 'ReturnsController@index')->name('returns.index');

    Route::get('/crear-devolucion/{id}','ReturnsController@create')->name('returns.create');

    Route::post('/datos-item/','ReturnsController@get_data_items')->name('returns.get_data_item');

    Route::post('/guardar-devolucion','ReturnsController@store')->name('returns.store');

    Route::get('/ver-devolucion/{id}','ReturnsController@show')->name('returns.view_return');

    Route::post('/finalizar-devolucion','ReturnsController@end_returns')->name('returns.end');

    Route::post('/borrar-devolucion','ReturnsController@destroy')->name('returns.delete');

    //ruta que devuelve los valores de una tabla con datatable del lado del servifor.
    Route::any('/tabla/devoluciones','ReturnsController@data_table')->name('returns.table');
});
