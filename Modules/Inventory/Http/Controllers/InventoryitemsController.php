<?php

namespace Modules\Inventory\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Excel;
use Illuminate\Support\Facades\Redirect;
use Modules\Inventory\Entities\Inventoryitems;
use Modules\Inventory\Entities\Inventorytypes;
use Modules\Inventory\Http\Requests\InventoryitemsRequest;

class InventoryitemsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->can('read-inventory')){

            $items = Inventoryitems::with('inventorytypes')->paginate(10);

            return view('inventory::items.index', compact('items'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->can('create-inventory')){

            $types = Inventorytypes::pluck('type', 'id');

            return view('inventory::items.create', compact('types'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(InventoryitemsRequest $request)
    {
        if (Auth::user()->can('create-inventory')){

            Inventoryitems::create($request->all());

            Session::flash('message', trans('warehouses::ui.warehouses.message_create', array('name'=> $request->description)));

            return redirect('inventory/items/create');
        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('update-inventory')){

            $items = Inventoryitems::findOrFail($id);

            $types = Inventorytypes::orderBy('type', 'asc')->pluck('type', 'id')->toArray();

            return view('inventory::items.edit', compact('items', 'types'));
        }

        $this->notAuthenticate();

        return redirect('dashboard');

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, InventoryitemsRequest $request)
    {
        if (Auth::user()->can('update-inventory')){

            $items = Inventoryitems::findOrFail($id);

            $items->update($request->all());

            Session::flash('message', trans('inventory::ui.items.message_update', array('name' => $request->description)));

            return redirect('inventory/items');

        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }


    public function itemsImport(Request $request){

        if (Auth::user()->can('create-inventory')){

            if($request->hasFile('products')){

                $path = $request->file('products')->getRealPath();

                $data = Excel::load($path)->get();

                if($data->count()){

                    foreach ($data as $key => $value) {
                        //print_r($value);
                        $product_list[] = ['code_sku' => $value->code_sku, 'description' => $value->description, 'inventorytypes_id' => $value->type, 'status' => $value->status, 'created_at' => Carbon::now()];
                    }
                    if(!empty($product_list)){
                        Inventoryitems::insert($product_list);

                        \Session::flash('success','File improted successfully.');
                    }
                }

            }else{

                \Session::flash('warnning','There is no file to import');

            }
            return Redirect::back();

        }

        $this->notAuthenticate();

        return redirect('dashboard');
    }



    public function notAuthenticate(){
        Session::flash('message_error', trans('auth::ui.user.message_not'));
    }

}
