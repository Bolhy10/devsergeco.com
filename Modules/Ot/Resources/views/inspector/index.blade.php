<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 02/04/2018
 * Time: 3:01
 */
?>

@extends('layouts.master')

@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Status</h6>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block refresh">
                            <i class="fa fa-line-chart"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body row">
                        <div class="">
                            <div class="pl-15 pr-15 mb-15">
                                <div class="pull-left">
                                    <i class="zmdi zmdi-collection-folder-image inline-block mr-10 font-16"></i>
                                    <span class="inline-block txt-dark">Ot's iniciadas</span>
                                </div>
                                <span class="inline-block txt-warning pull-right weight-500">{{ $ot_i }}</span>
                                <div class="clearfix"></div>
                            </div>
                            <hr class="light-grey-hr mt-0 mb-15"/>
                            <div class="pl-15 pr-15 mb-15">
                                <div class="pull-left">
                                    <i class="zmdi zmdi-format-list-bulleted inline-block mr-10 font-16"></i>
                                    <span class="inline-block txt-dark">Ot's Pausadas</span>
                                </div>
                                <span class="inline-block txt-danger pull-right weight-500">{{ $ot_p }}</span>
                                <div class="clearfix"></div>
                            </div>
                            <hr class="light-grey-hr mt-0 mb-15"/>
                            <div class="pl-15 pr-15 mb-15">
                                <div class="pull-left">
                                    <i class="zmdi zmdi-format-list-bulleted inline-block mr-10 font-16"></i>
                                    <span class="inline-block txt-dark">Ot's eliminadas</span>
                                </div>
                                <span class="inline-block txt-danger pull-right weight-500">{{ $ot_e }}</span>
                                <div class="clearfix"></div>
                            </div>
                            <hr class="light-grey-hr mt-0 mb-15"/>
                            <div class="pl-15 pr-15 mb-15">
                                <div class="pull-left">
                                    <i class="zmdi zmdi-format-list-bulleted inline-block mr-10 font-16"></i>
                                    <span class="inline-block txt-dark">Ot's Cerradas</span>
                                </div>
                                <span class="inline-block txt-danger pull-right weight-500">{{ $ot_c }}</span>
                                <div class="clearfix"></div>
                            </div>
                            <hr class="light-grey-hr mt-0 mb-15"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Proyectos Asignados(OT's)</h6>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body row pa-0">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table display product-overview border-none" id="support_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Fecha de creación</th>
                                        <th>Código de Ot</th>
                                        <th>Proyecto</th>
                                        <th>Lugar</th>
                                        <th>Area</th>
                                        <th>Cliente</th>
                                        <th>Estado</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $i = 1; @endphp
                                    @foreach($ots as $ot)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $ot->created_at }}</td>
                                            <td>{{ $ot->code_ot }}</td>
                                            <td>{{ $ot->project_name }}</td>
                                            <td>{{ $ot->place }}</td>
                                            <td>{{ $ot->zone }}</td>
                                            <td>{{ $ot->fullname }}</td>
                                            @if($ot->status == 1)
                                                @php $tag ='label-primary'; $estado = 'INICIADO'; @endphp
                                            @elseif($ot->status == 2)
                                                @php $tag ='label-warning'; $estado = 'PAUSADO'; @endphp
                                            @elseif($ot->status == 3 || $ot->status == 4)
                                                @php $tag ='label-danger'; $ot->status == 3 ? $estado = 'CERRADO' : $estado = 'ELIMINADO'; @endphp
                                            @endif
                                            <td><span class="label {{ $tag }}">  {{ $estado }}</span></td>
                                            <td><!--a href="{{ route('ot.edit', $ot->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle" id="{{  $ot->id }}"><i class="fa fa-pencil"></i></a--></td>
                                            <td><a href="{{ route('ot.view_ot', $ot->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle" id="{{  $ot->id }}"><i class="fa fa-eye"></i></a></td>
                                        </tr>
                                        @php $i++ @endphp
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@stop

