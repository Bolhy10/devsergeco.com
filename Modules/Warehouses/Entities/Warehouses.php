<?php

namespace Modules\Warehouses\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouses extends Model
{

    use SoftDeletes;

    protected $fillable = ['code', 'name', 'zones_id', 'status'];

    protected $dates = ['deleted_at'];

    public function zones()
    {
        return $this->belongsTo('Modules\Warehouses\Entities\Zones');
    }

    public function clients()
    {
        return $this->belongsToMany('Modules\Clients\Entities\Clients', 'warehouses_clients');
    }

    public function inventory()
    {
        return $this->hasOne('Modules\Inventory\Entities\Inventory');
    }

//    CORREGIR LAS RELACIONES YA QUE SON INVERSAS
    public function ingress(){

        return $this->hasMany('Modules\Inventory\Entities\Ingress');

    }

    public function discard(){

        return $this->hasMany('Modules\Inventory\Entities\Discard');

    }

}
