<?php

Route::group(['middleware' => 'web', 'prefix' => 'inventariou', 'namespace' => 'Modules\InventarioUnico\Http\Controllers'], function()
{
    //index
    Route::get('/', 'InventarioUnicoController@index');

    //ver información del item unico
    Route::post('/info','InventarioUnicoController@info')->name('ui.info');

    //agregar cantidad que se ha tomado de los items unicos
    Route::get('/agregar-seleccion/{d_id}/{id}','UistatusController@create')->name('uist.create');

    Route::post('/guardar-seleccion','UistatusController@store')->name('uist.store');

});
