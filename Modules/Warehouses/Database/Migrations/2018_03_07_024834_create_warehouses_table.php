<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');
            $table->string('name');
            $table->integer('zones_id')->unsigned();
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('zones_id')
                ->references('id')->on('zones');
        });


        Schema::create('warehouses_clients', function (Blueprint $table){

            $table->integer('warehouses_id')->unsigned();
            $table->integer('clients_id')->unsigned();

            $table->foreign('warehouses_id')->references('id')->on('warehouses')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('clients_id')->references('id')->on('clients')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['warehouses_id', 'clients_id']);

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses_clients');
        Schema::dropIfExists('warehouses');
    }

}
