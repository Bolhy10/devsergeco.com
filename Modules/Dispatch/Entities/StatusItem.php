<?php

namespace Modules\Dispatch\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusItem extends Model
{
    use SoftDeletes;
    protected $table = 'status_item';
    protected $fillable = [
        'quantity',
        'inventory_id',
        'dispatch_id',
        'status'
    ];

    public function inventory()
    {
        return $this->belongsTo('Modules\Inventory\Entities\Inventory');
    }

    /**
     * @param $id
     * @return mixed
     * función usada en la sección de editar un despacho
     */
    public function st_item($id){
        return $this->select('status_item.id AS sti_id', 'status_item.quantity AS sti_qty','status_item.inventory_id AS sti_invid',
            'status_item.dispatch_id AS sti_dispatch', 'inventory.quantity AS inv_qty','inventoryitems.code_sku',
            'inventoryitems.description','clients.fullname','inventorytypes.type')
            ->join('inventory','status_item.inventory_id','=','inventory.id')
            ->join('inventoryitems','inventory.inventoryitems_id','=','inventoryitems.id')
            ->join('inventorytypes','inventoryitems.inventorytypes_id','=','inventorytypes.id')
            ->join('clients','inventory.clients_id','=','clients.id')
            ->where('status_item.id',$id)
            ->get();
    }

    public function returns_items($id)
    {
        return $this->select('status_item.id AS sti_id', 'status_item.quantity AS sti_qty','status_item.inventory_id AS sti_invid',
            'status_item.dispatch_id AS sti_dispatch', 'inventory.quantity AS inv_qty','inventoryitems.code_sku',
            'inventoryitems.description','inventorytypes.type')
            ->join('inventory','status_item.inventory_id','=','inventory.id')
            ->join('inventoryitems','inventory.inventoryitems_id','=','inventoryitems.id')
            ->join('inventorytypes','inventoryitems.inventorytypes_id','=','inventorytypes.id')
            ->join('dispatch','status_item.dispatch_id','=','dispatch.id')
            ->where(['dispatch.ot_id'=>$id, 'dispatch.status' => 2])
            ->get();
    }

    /**
     * @param $id
     * @return mixed
     * funcion usada en la sección de devolución de items
     */
    public function st_item_r($id){
        return $this->select('status_item.id AS sti_id', 'status_item.quantity AS sti_qty','status_item.inventory_id AS sti_invid',
            'status_item.dispatch_id AS sti_dispatch', 'inventory.quantity AS inv_qty','inventoryitems.code_sku',
            'inventoryitems.description','clients.fullname','inventorytypes.type')
            ->join('inventory','status_item.inventory_id','=','inventory.id')
            ->join('inventoryitems','inventory.inventoryitems_id','=','inventoryitems.id')
            ->join('inventorytypes','inventoryitems.inventorytypes_id','=','inventorytypes.id')
            ->join('clients','inventory.clients_id','=','clients.id')
            ->where(['status_item.id' => $id, 'status_item.status' => 2])
            ->get();
    }
}
