@extends('layouts.master')

@section('pageTitle', trans('inventory::ui.ingress.create.title'))

@section('content')

    <form method="POST" @submit.prevent="validateBeforeSubmit" id="ingress">
            <div class="row mb-10">
                <div class="col-lg-12">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">
                            Guardar Datos
                        </button>
                        <a href="{{ route('inventory.ingress') }}" class="btn btn-warning">
                            Volver
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                @include('inventory::ingress.form')
                @include('inventory::ingress.table')
            </div>
    </form>

    @stop