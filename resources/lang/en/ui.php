<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 02/06/18
 * Time: 05:11 PM
 */

return [

    'title'             =>  'Sergeco',
    'header_top'        =>  [
        'change_password'       =>  'Cambiar Contraseña',
        'logout'        =>  'Salir'
    ],

    'sidebar'           =>  [
        'dashboard'     =>  'Dashboard',
        'admin_users'   =>  'Administrar Usuarios',
        'permissions'   =>  'Permisos',
        'roles'         =>  'Roles',
        'users'         =>  'Usuarios'
    ]
];