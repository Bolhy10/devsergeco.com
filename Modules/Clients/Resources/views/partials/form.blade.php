<div class="form-group">
    <label class="control-label mb-10">{{ trans('clients::ui.clients.shortname') }}</label>
    {!! Form::text('shortname', null, ['class' => 'form-control', 'placeholder'=> trans('clients::ui.clients.shortname')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('clients::ui.clients.fullname') }}</label>
    {!! Form::text('fullname', null, ['class' => 'form-control', 'placeholder'=> trans('clients::ui.clients.fullname')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('clients::ui.clients.phone') }}</label>
    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder'=> trans('clients::ui.clients.phone')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('clients::ui.clients.address') }}</label>
    {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder'=> trans('clients::ui.clients.address')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('clients::ui.clients.status') }}</label>
    {!! Form::select('status', ['1' => 'Activo', '0' => 'Desactivado'], null, ['class'=> 'form-control','placeholder' => 'Seleccione el estado']) !!}
</div>

<div class="form-group col-md-12">
    <div class="col-md-6 col-lg-offset-3">
        <label class="control-label mb-10 text-center" for="allSelect"><strong>{{ trans('clients::ui.clients.warehouses') }}</strong></label>

        {!! Form::select('warehouses_id[]', $warehouses, isset($clients_warehouses) ? $clients_warehouses : null,
        array('multiple' => true, 'class' => 'multi-select', 'id' => 'allSelect')) !!}

    </div>
</div>


<div class="form-group mb-0">

    {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}

    <a href="{{ route('clients.index') }}" class="btn btn-warning">{{ trans('auth::ui.general.back') }}</a>

</div>