<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 17/03/2018
 * Time: 21:11
 */
?>

@extends('layouts.master2')

@section('content')
    <div class="col-md-12">
        <div class="well text-center">
            <div class="well">
                <div class="row">
                    <h3>Disculpe no se ha podido realizar la solicitud debido al siguiente error: </h3>
                    <div class="col">
                        <h5>{{ $error_change }}</h5>
                    </div>
                </div>
            </div>
            <div class="well">
                <div class="row">
                    @if($ot_id == 0)
                        <a class="btn btn-primary" href="{{ URL::previous() }}"> REGRESAR </a>
                    @else
                        <a class="btn btn-primary btn-outline" href="{{ route('ot.view_ot',$ot_id) }}"> REGRESAR </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection