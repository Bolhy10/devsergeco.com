<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 20/02/2018
 * Time: 20:27
 */
?>
@if (count($errors) > 0)
<script>
    $(function() {
        "use strict";
        var SweetAlert = function () {
        };

        //examples
        SweetAlert.prototype.init = function () {

            swal({
                title: 'ERROR en el formulario',
                type: 'info',
                text:
                    'Tiene errores en los siguientes campos: '+
                    @foreach ($errors->all() as $error)
                        '{{ $error }}, '+
                    @endforeach ' por favor revisar.'
                ,
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText:
                    'OK',
                confirmButtonAriaLabel: 'OK',
            });
        },

        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert;

        $.SweetAlert.init();

    });
</script>
@endif
