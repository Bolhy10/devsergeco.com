<!DOCTYPE html>
<html lang="es">
    <head>
        @include('partials.header')
        @include('partials.style')
    </head>
    <body>

    <div class="wrapper pa-0">

        @include('auth::partials.header_top')

        <div class="page-wrapper pa-0 ma-0 auth-page" style="min-height: 900px;">
            <div class="container-fluid">
                <!-- Row -->
                <div class="table-struct full-width full-height" style="height: 900px;">
                    <div class="table-cell vertical-align-middle auth-form-wrap">
                        <div class="auth-form  ml-auto mr-auto no-float">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="mb-30">
                                        <img src="{{ asset('assets/images/elements/logo.png')  }}" alt="Sergeco">
                                        <h3 class="text-center txt-dark mb-10">{{ trans('auth::ui.login.title') }}</h3>
                                    </div>
                                    <div class="form-wrap">
                                       @yield('content')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->
            </div>

        </div>

    </div>

    {{--@include('auth::partials.banner')--}}


    @include('partials.script')

    </body>
</html>
