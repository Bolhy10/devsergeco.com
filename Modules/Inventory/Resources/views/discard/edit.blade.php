<div class="row" v-if="!changeEdit">
    <div class="col-md-12">

        <form method="post" v-on:submit.prevent="updateDiscard(fillDiscard)">

            <div class="row mb-10">
                <div class="col-lg-12">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">
                            Actualizar Descarte
                        </button>

                        <a href="{{ route('inventory.discard') }}" class="btn btn-warning">
                            Volver
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">

                @include('inventory::discard.form')
                @include('inventory::discard.table')

            </div>

        </form>

    </div>
</div>