<?php

Route::group(['middleware' => 'web', 'prefix' => 'ot', 'namespace' => 'Modules\Ot\Http\Controllers'], function()
{
    Route::get('/', 'OtController@index')->name('ot.index');

    Route::post('/crear-ot/','OtController@create')->name('ot.create');
    Route::post('/guardar-ot','OtController@store')->name('ot.store');

    /**
     * Rutas relacionadas al panel de estado de una Ot
     */
    Route::get('/ver-ot/{id}','OtController@status_ot')->name('ot.view_ot');
    Route::post('ver-ot','OtController@change_status_ot')->name('ot.change_status');

    /**
     * Rutas relacionadas al momento de agregar los materiales del inventario a despachar en la OT
     */
    Route::get('/agregar-despacho/{id}','OtController@dispatch_item')->name('ot.dispatch');
    Route::post('/agregar-despacho','OtController@save_dispatch')->name('ot.save_dispatch');

    /**
     * Rutas para obtener data de los materiales de un inventario
     */
    Route::get('/get-inventario/{id}','OtController@getdata_inventary')->name('ot.get_inventario');
    Route::get('/obtener-inventario/{w_id}/{c_id}','OtController@obtener_inventario')->name('ot.obtener_inventario');

    /**
     * Obtener item a agregar e info de un item en especifico con su cantidad disponible
     */
    Route::post('/get-item','OtController@get_items')->name('ot.getitem');
    //Route::post('/get-item-info','OtController@get_info_item')->name('ot.getinfoitem');

    Route::get('/editar-ot','OtController@edit')->name('ot.edit');

    Route::get('/imprimir/{option}/{id}','OtController@prints')->name('ot.prints');

    /**
     * RUTAS DE ALMACENISTAS
     */

    Route::get('/ots-bodega/{id}','OtController@index')->name('ot.almacenista_view');
});
