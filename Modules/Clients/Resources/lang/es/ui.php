<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 03/08/18
 * Time: 02:18 PM
 */

return [

    'title' => 'Control de Clientes',
    'count' => [
        'panel_1'=>[
            'title' => 'Estado',
            'warehouse' => 'Almacenes Activos',
            'zone' => 'Cantidad de Zonas',
            'province' => 'Total de provincias'
        ]
    ],

    'clients' => [
        'title' => 'Listado de Clientes',
        'shortname'=> 'Nombre Corto',
        'fullname' => 'Nombre Completo',
        'phone' => 'Móvil',
        'address' => 'Dirección',
        'warehouses' => 'Almacenes',
        'status' => 'Estado',
        'operation_label' => 'Acciones',
        'new_client' => 'Nuevo Cliente',
        'button_add'    =>  'Agregar',
        'create' => [
            'title' => 'Nuevo Cliente',
            'btn_add' => 'Guardar',
        ],

        'update' => [
            'title' => 'Actualizar Cliente',
            'btn_update' => 'Actualizar',
        ],


        'message_delete'        =>  'El Cliente :name ha sido eliminado satisfactoriamente',
        'message_create'        =>  'El Cliente :name ha sido creado satisfactoriamente',
        'message_update'        =>  'El Cliente :name ha sido actualizado satisfactoriamente'

    ],

    'zones' => [
        'subtitle' => 'Listado de Zonas',
        'title'=> 'Control de Zonas',
        'name' => 'Nombre',
        'address' => 'Dirección',
        'provinces_id' => 'Provincia',
        'status' => 'Estado',
        'latitude' => 'Latitud',
        'longitude' => 'Longitud',
        'operation_label' => 'Acciones',
        'new_zones' => 'Nueva Zona',
        'button_add'    =>  'Agregar Zona',
        'create' => [
            'title' => 'Nueva Zona',
            'btn_add' => 'Guardar',
        ],

        'update' => [
            'title' => 'Actualizar Zona',
            'btn_update' => 'Actualizar',
        ],


        'message_delete'        =>  'La zona :name ha sido eliminada satisfactoriamente',
        'message_create'        =>  'La zona :name ha sido creada satisfactoriamente',
        'message_update'        =>  'La zona :name ha sido actualizada satisfactoriamente'

    ]

];