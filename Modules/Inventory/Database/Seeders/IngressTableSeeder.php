<?php

namespace Modules\Inventory\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class IngressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < 10; $i++){
            DB::table('ingress')->insert(array(
                'clients_id' => $faker->numberBetween($min = 1, $max = 5),
                'warehouses_id' => $faker->randomElement($array = array (1, 2)),
                'provider' => $faker->company,
                'warehouses_provider' => $faker->address,
                'no_document' => $faker->randomNumber(4),
                'deliver_to' => $faker->name,
                'no_order' => $faker->randomNumber(4),
                'no_transfer' => $faker->randomNumber(4),
                'users_id' => 1,
                'observations' => $faker->text($maxNbChars = 100),
                'status' => $faker->randomElement($array = array (true, false)),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ));
        }
    }
}
