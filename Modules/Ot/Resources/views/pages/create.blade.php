<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 01/03/2018
 * Time: 22:44
 */

?>
@extends('layouts.master')

@section('content')
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                @section('pageTitle','Formulario para la creación de Ordenes de Trabajos.')
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Creación de Ot</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        {!! Form::open(['route'=>'ot.store']) !!}
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="code_ot">Código de OT </label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-user"></i></div>
                                                {!! Form::text('code_ot', null, ['class' => 'form-control','id' => 'code_ot']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="zone">Zona: </label>
                                            <p>{{ $zona }}</p>
                                            <input type="hidden" value="{{ $id_zona }}" name="zone">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="cliente">Cliente </label>
                                            <select class="form-control" name="cliente" id="cliente" required>
                                                <option value="">Seleccionar...</option>
                                                @foreach($clients as $cli)
                                                    <option value="{{ $cli->id }}">{{ $cli->fullname }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="project_name">Nombre del proyecto </label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-user"></i></div>
                                                {!! Form::text('project_name', null, ['class' => 'form-control','id' => 'project_name']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="place">Lugar del proyecto </label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-user"></i></div>
                                                {!! Form::text('place', null, ['class' => 'form-control','id' => 'place']) !!}
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <label class="control-label mb-10">Datos del supervisor:</label>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-12 col-lg-12 col-sm-12">
                                                    <div class="form-group">
                                                        @if(Auth::user()->hasRole('admin'))
                                                        <label class="control-label mb-10" for="supervisor">Supervisor: </label>
                                                        <select class="form-control" name="user[]" id="supervisor">
                                                            <option value="">Seleccionar Supervisor a cargo de la Ot...</option>
                                                            @foreach($super->users as $s)
                                                                <option value="{{ $s->id }}">{{ $s->firstname }} {{ $s->lastname }}</option>
                                                            @endforeach
                                                        </select>
                                                        @elseif(Auth::user()->hasRole('supervisor'))
                                                            <p class="text-center" >Nombre:</p>
                                                            <p class="text-center">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
                                                            <input type="hidden" name="user[]" value="{{ Auth::user()->id }}">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <label class="control-label mb-10">Datos del inspector 1:</label>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-12 col-lg-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="inspector">Inspector: </label>
                                                        <select class="form-control" name="user[]" id="inspector">
                                                            <option value="">Seleccionar Inspector a cargo de la Ot...</option>
                                                            @foreach($inspect->users as $in)
                                                                <option value="{{ $in->id }}">{{ $in->firstname }} {{ $in->lastname }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @section('style')
        {!! Html::style('assets/css/sweetalert.css') !!}
    @endsection
    @section('script')
        {!! Html::script('assets/plugins/sweetalert.min.js') !!}
        @include('errors.error_alert')
    @endsection
@stop