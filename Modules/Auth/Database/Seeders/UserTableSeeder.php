<?php

namespace Modules\Auth\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('users')->insert(array(
            'firstname' => 'Anthony',
            'lastname' => 'Valdes',
            'username' => 'avaldes',
            'email' => 'avaldes@gruposergeco.com',
            'phone' => '6616-6492',
            'password' => Hash::make('GrupoSergeco1820'),
            'image' => $faker->randomElement(['default1.png', 'default2.png', 'default3.png']),
            'verified' => true,
            'verification_token' => null,
            'remember_token' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ));

        DB::table('role_user')->insert(array(
            'user_id' => 1,
            'role_id' => 1
        ));
    }

}