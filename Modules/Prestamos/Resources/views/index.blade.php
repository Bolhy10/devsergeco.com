@extends('layouts.master')

@section('content')
    @section('pageTitle',$titulo)
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Préstamos</h6>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block refresh">
                            <i class="fa fa-exchange"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <!-- PANEL BODY -->
                    <div class="panel-body row">
                    <div class="table-wrap">
										<div class="table-responsive">
											<table id="datable_prestamos" class="table table-hover dt-responsive" style="cursor: pointer;">
												<thead>
													<tr>
														<th>Id</th>
														<th>Codigo de Ot</th>
														<th>Inventario Origen</th>
														<th>Inventario Recibido</th>
                                                        <th>Descripción</th>
														<th>Status</th>
														<th>Fecha</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
                    </div>
                    <!-- END PANEL BODY -->
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">

        </div>
    </div>
@stop

@section('script')
    @include('partials.data-tables')
@endsection
