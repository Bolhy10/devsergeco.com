<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discard extends Model
{

    use SoftDeletes;

    protected $table = 'discard';

    protected $fillable = ['clients_id','warehouses_id', 'users_id', 'no_document', 'receive_to', 'observations', 'status'];

    protected $dates =['deleted_at'];

    public function discarditems(){

        return $this->hasMany('Modules\Inventory\Entities\Discarditems');
    }

    public function clients(){

        return $this->belongsTo('Modules\Clients\Entities\Clients');

    }

    public function users(){

        return $this->belongsTo('Modules\Auth\Entities\User');

    }

    public function warehouses(){

        return $this->belongsTo('Modules\Warehouses\Entities\Warehouses');

    }


}
