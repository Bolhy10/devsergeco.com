<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Auth\Entities\User;
use Modules\Auth\Http\Requests\ProfileRequest;
use Nwidart\Modules\Routing\Controller;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {

        $profile = Auth::user();

        if ($profile){

            return view('auth::profile.index', compact('profile'));
        }else{

            redirect('auth/logout');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(ProfileRequest $request)
    {
        if (Auth::check()){

            $profile = User::findOrFail($request->user()->id);

            $profile->update($request->all());

            Session::flash('message', trans('auth::ui.profile.message_update', array('username'=>$request->user()->username)));

            return redirect('auth/profile');
        }

       redirect('auth/logout');
    }


    public function uploadImage(){




    }

}
