<?php
/**
 * Created by PhpStorm.
 * User: reynaldo
 * Date: 19/03/2018
 * Time: 23:56
 */
?>
@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-right">
                        <h6>OT #{{ $despacho->ot->code_ot }}</h6>
                    </div>
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Despacho #{{ $despacho->id }}</h6>
                        @if( $despacho->status == 1 )
                            <p class="text-danger">Despacho aún sin finalizar.</p>
                        @elseif ($despacho->status == 2 && $despacho->prints == 0)
                            <p class="text-danger">Despacho finalizado</p>
                        @elseif($despacho->status == 2 && $despacho->prints > 0)
                            <p class="text-danger">Despacho finalizado, el documento ya ha sido impreso.</p>
                        @elseif($despacho->status == 3)
                            <p class="text-danger">El despacho ha sido eliminado</p>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Código</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Cantidad Solicitada</th>
                                @if($despacho->status == 1)
                                    <th scope="col">Cant. Disponible</th>
                                @elseif($despacho->status == 2)
                                    <th scope="col">Cant. Restante</th>
                                @endif
                                <th scope="col">Categoría</th>
                                <th scope="col">Inventario</th>
                                @if($despacho->status == 1)
                                    <th></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody id="body-items">
                            @php $cont = 1; @endphp
                            @foreach($despacho->status_item as $d_d)
                                @php  $val_sel = false; @endphp
                                <tr>
                                    <td>{{ $cont }}</td>
                                    <td>{{ $d_d->inventory->inventoryitems->code_sku }}</td>
                                    <td>{{ $d_d->inventory->inventoryitems->description }}</td>
                                    <td id="cant{{ $d_d->id }}">{{ $d_d->quantity }}</td>
                                    <td>{{ $d_d->inventory->quantity }}</td>
                                    <td>{{ $d_d->inventory->inventoryitems->inventorytypes->type }}</td>
                                    <td>{{ $d_d->inventory->clients->fullname }}</td>
                                    @if( $d_d->inventory->inventoryitems->inventorytypes->type == "Cables" || $d_d->inventory->inventoryitems->inventorytypes->type == "Transformador" )
                                        @foreach($d_d->inventory->uinventary as $ui)
                                            @if(!empty($ui->uistatus))
                                                @php  $val_sel = true; @endphp
                                            @else
                                                @php  $val_sel = false; @endphp
                                            @endif
                                        @endforeach
                                            @if($val_sel == !true)
                                                <td><a class="btn btn-primary">Ver selección</a></td>
                                            @elseif($val_sel == true)
                                                <td><a href="{{ route('uist.create',['d_id'=>$despacho->id,'id'=>$d_d->inventory->id]) }}" class="btn btn-small btn-primary">Selección</a></td>
                                            @endif
                                    @endif
                                </tr>
                                @php $cont++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-outline" href="{!! URL::previous() !!}"> REGRESAR </a>
                                    <input type="hidden" id="id-despacho" value="{{ $despacho->id }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                @php //se valida si el estado del despacho es 'en despacho' => 1 @endphp
                                @if($despacho->status === 2)
                                    <a class="btn btn-success btn-outline" href="{{ route( 'ot.prints',[ 'option' => 1,'id' => $despacho->id ]) }}" target="_blank">IMPRIMIR</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('dispatch::layouts.script_dispatch')
@endsection

