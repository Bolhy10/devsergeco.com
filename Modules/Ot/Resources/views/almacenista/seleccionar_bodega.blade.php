<?php
/**
 * Created by PhpStorm.
 * User: yvargas
 * Date: 04/13/2018
 * Time: 12:17 PM
 */
?>

@extends('layouts.master')

@section('content')
@section('pageTitle',$titulo)

<div class="col-md-12 col-lg-12">
    <div class="panel panel-default card-view panel-refresh">
        <div class="refresh-container">
            <div class="la-anim-1"></div>
        </div>
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">Seleccione la bodega:</h6>
            </div>
            <div class="pull-right">
                <a href="#" class="pull-left inline-block refresh">
                    <i class="fa fa-building"></i>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- BODY -->
        <div class="panel-wrapper collapse in">
            <div class="panel-body text-center">
                <div class="form-group">
                    @foreach($bodegas as $z)
                        <div class="col-md-3">
                            <a class="btn btn-primary" href="{{route('ot.almacenista_view',['id' => $z->zones_id])}}">
                                {{ $z->name }}
                            </a>
                        </div>

                    @endforeach

                </div>
            </div>
        </div>
        <!-- END BODY -->
    </div>
</div>

@stop
