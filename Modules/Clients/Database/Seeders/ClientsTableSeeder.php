<?php

namespace Modules\Clients\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;


class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //primer cliente sera sergeco
        DB::table('clients')->insert(array(
            'shortname' => 'Sergeco',
            'fullname' => 'Sergeco',
            'phone' => '0000000',
            'address' => 'Mañanitas',
            'status' => 1,
            'created_at' => Carbon::now(),
            'updated_at' =>Carbon::now()
        ));

        $faker = Faker::create();

        for ($i = 0; $i < 7; $i++){
            DB::table('clients')->insert(array(
                'shortname' => $faker->jobTitle,
                'fullname' => $faker->company,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'status' => $faker->randomNumber($array = array(1, 0)),
                'created_at' => Carbon::now(),
                'updated_at' =>Carbon::now()
            ));
        }
    }
}
