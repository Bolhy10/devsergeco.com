<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Uinventary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //tabla general del inventario unico
        Schema::create('uinventary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_id')->unsigned();
            $table->string('nombre');
            $table->string('codigounico');
            $table->integer('cantidad');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('inventory_id')->references('id')->on('inventory');
        });

        //tabla de los procesos en los cuales se involucran items del invntario unico
        Schema::create('uistatus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uinventary_id')->unsigned();
            $table->integer('dispatch_id')->unsigned();
            $table->integer('quantity');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('uinventary_id')->references('id')->on('uinventary');
            $table->foreign('dispatch_id')->references('id')->on('dispatch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
