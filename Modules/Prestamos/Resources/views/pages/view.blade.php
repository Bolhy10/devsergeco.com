@extends('layouts.master')
@section('pageTitle','Prestamo Solicitado')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <!-- panel heading -->
                <div class="panel-heading">
                    <div class="pull-right">
                        <h6>OT #{{ $prestamos->dispatch->ot->code_ot }}</h6>
                    </div>
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Despacho #{{ $prestamos->dispatch->id }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- end panel heading -->
                <div class="panel-wrapper collapse in">
                    <!-- panel body -->
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Proyecto</th>
                                    <th>Inventario Tomado</th>
                                    <th>Inventario Recibido</th>
                                    <th>Descripción</th>
                                    <th>Categoría</th>
                                    <th>Cantidad Prestada</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td>{{ $prestamos->id}}</td>
                                <td>{{ $prestamos->dispatch->ot->project_name }}</td>
                                <td>{{ $prestamos->inventory->clients->shortname }}</td>
                                <td>{{ $prestamos->dispatch->ot->clients->shortname }}</td>
                                <td>{{ $prestamos->inventory->inventoryitems->description }}</td>
                                <td>{{ $prestamos->inventory->inventoryitems->inventorytypes->type }}</td>
                                <td>{{ $cant_prestamo }}</td>
                                @if($prestamos->status == 1)
                                    <td>Prestado</td>
                                @elseif($prestamos->status == 2)
                                    <td>Devuelto</td>
                                @endif
                                <td>{{ $prestamos->created_at }}</td>
                                </tr>
                            </tbody>
                        </table>
                        @if($prestamos->status == 1)
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            @foreach($dat_cr as $dcr)
                                            <p class="text-primary">Inventario de {{ $prestamos->dispatch->ot->clients->shortname }}</p>
                                            <p>Code_SKU: {{ $dcr->inventoryitems->code_sku }}</p>
                                            <p>Descripción: {{ $dcr->inventoryitems->description }}</p>
                                            <p>Cantidad: <span class="badge">{{ $dcr->quantity }}</span></p>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!--a href="{{route('prestamos.return',$prestamos->id)}}" class="btn btn-primary">Devolver Material</a-->
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-md-12 text-center">
                                            @if($prestamos->dispatch->status == 2)
                                                @if($dat_cr[0]->quantity > $cant_prestamo )
                                                <p class="text-primary">Ya puedes realizar la devolución del prestamo.</p>
                                                <a class="btn btn-primary" href="{{route('prestamos.return',$prestamos->id)}}">Devolver</a>
                                                @else
                                                <p class="text-danger">El inventario aún no cuenta con la cantidad suficiente para realizar la devolución.</p>
                                                @endif
                                            @elseif($prestamos->dispatch->status == 1)
                                                <p class="text-danger">El despacho aun no ha sido finalizado.</p>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                        @foreach($dat_ct as $dct)
                                        <p class="text-primary">Inventario de {{ $prestamos->inventory->clients->shortname }}</p>
                                        <p>Code SKU: {{ $dct->inventoryitems->code_sku }}</p>
                                        <p>Descripción: {{ $dct->inventoryitems->description }}</p>
                                        <p>Cantidad: <span class="badge">{{ $dct->quantity }}</span></p>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @elseif($prestamos->status == 2)
                            <div class="col-md-4">
                                <h6 class="text-danger">El material ha sido devuelto.</h6>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- end panel body -->
                </div>
            </div>
        </div>
    </div>
@endsection