<?php

namespace Modules\InventarioUnico\Entities;

use Illuminate\Database\Eloquent\Model;

class uinventary extends Model
{
    protected $table = "uinventary";
    protected $fillable = [
        'inventory_id',
        'nombre',
        'codigounico',
        'cantidad'
    ];

    public function inventory()
    {
        return $this->belongsTo('Modules\Inventory\Entities\Inventory');
    }

    public function uistatus()
    {
        return $this->hasMany('Modules\InventarioUnico\Entities\Uistatus');
    }
}
