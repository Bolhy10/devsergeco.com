@extends('layouts.master')

@section('pageTitle', 'Items Unicos')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <!-- heading of panel -->
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6>Seleccione item de donde tomo el material.</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- end of heading-->
                <div class="panel-wrapper collapse in">
                    <!-- body panel -->
                    <div class="panel-body">
                        <!-- detail -->
                        <div class="col-md-12">
                            <div class="invoice-box row">
                                <div class="col-sm-6">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>Código SKU: {{ $data->inventoryitems->code_sku }}</td>
                                            </tr>
                                            <tr>
                                                <td>Descripción: {{ $data->inventoryitems->description }}</td>
                                            </tr>
                                            <tr>
                                                <td>Cantidad Total: {{ $data->quantity }}</td>
                                            </tr>
                                            <tr>
                                                <td> Tipo: {{ $data->inventoryitems->inventorytypes->type }}</td>
                                            </tr>
                                            <tr>
                                                <td>Cantidad de rollos de cables: {{ $data->uinventary->count() }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-4">
                                    {!! Form::open(['route'=>'uist.store']) !!}
                                    <p class="text-primary">Cantidad usada en el despacho: {{ $despacho->status_item[0]->quantity }}</p>
                                        <div class="form-group">
                                            <label for="uinput">Seleccione:</label>
                                            <select name="ui_item" id="uinput" class="form-control" required>
                                                <option value="">Elija el item.</option>
                                                @foreach($data->uinventary as $ui)
                                                    <option value="{{ $ui->id }}">{{$ui->nombre}}  {{ $ui->codigounico }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="cant">Cantidad:</label>
                                            <input type="number" class="form-control" name="cantidad" id="cant" required>
                                        </div>
                                        <input type="hidden" name="id_despacho" value="{{ $despacho->id }}">
                                        <input type="submit" class="btn btn-primary" value="GUARDAR">
                                    {!! Form::close() !!}        
                                </div>
                            </div>
                        </div>
                        <!-- end detail --> 
                    </div>
                    <!-- end body panel -->
                </div>
            </div>
        </div>
    </div>

@endsection