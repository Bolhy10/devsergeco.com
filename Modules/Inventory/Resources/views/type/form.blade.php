<div class="form-group">
    <label class="control-label mb-10">{{ trans('inventory::ui.types.type') }}</label>
    {!! Form::text('type', null, ['class' => 'form-control', 'placeholder'=> trans('inventory::ui.types.type') ]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('inventory::ui.types.status') }}</label>
    {!! Form::select('status', ['1' => 'Activo', '0' => 'Desactivado'], null, ['class'=> 'form-control','placeholder' => 'Seleccione el estado']) !!}
</div>

<div class="form-group mb-0">

    {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}

    <a href="{{ route('types.index') }}" class="btn btn-warning">{{ trans('auth::ui.general.back') }}</a>

</div>