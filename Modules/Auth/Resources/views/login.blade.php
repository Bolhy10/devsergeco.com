@extends('auth::layouts.master')

@section('content')
    {!! Form::open(array('route' => 'auth.login')) !!}
        <div class="form-group">
            <label class="control-label mb-10">{{ trans('auth::ui.login.email') }}</label>
            {!! Form::text('email', '', ['class'=> 'form-control', 'autocomplete'=>'off', 'placeholder'=>trans('auth::ui.login.email')]) !!}
        </div>
        <div class="form-group">
            <label class="pull-left control-label mb-10" for="exampleInputpwd_2">{{ trans('auth::ui.login.password') }}</label>
            <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="#"> {{ trans('auth::ui.login.forgot')  }}</a>
            <div class="clearfix"></div>
            {!! Form::password('password', ['class'=> 'form-control', 'placeholder'=>trans('auth::ui.login.password')]) !!}
        </div>

        <div class="form-group">
            <div class="checkbox checkbox-primary pr-10 pull-left">
                {!! Form::checkbox('remember', '', null, ['id'=>'checkbox_2']) !!}
                <label for="checkbox_2"> {{ trans('auth::ui.login.rememberme') }}</label>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group text-center">
            {!! Form::submit(trans('auth::ui.login.signin'), ['class' => 'btn btn-primary  btn-rounded']) !!}
        </div>
    {!! Form::close() !!}

    @include('errors.form_error')

@stop
