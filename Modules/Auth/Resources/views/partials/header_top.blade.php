<header class="sp-header">
	<div class="sp-logo-wrap pull-left">
		<span>
			<img class="brand-img mr-10" src="{{ asset('assets/images/elements/logo_vm_120_27.png') }}" alt="brand">
		</span>
	</div>
	<div class="clearfix"></div>
</header>