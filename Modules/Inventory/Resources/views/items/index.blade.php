@extends('layouts.master')

@section('pageTitle', trans('inventory::ui.title'))

@section('content')

    <div class="row">
        <div class="col-md-12">

            @include('partials.message')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('inventory::ui.items.title') }}</h6>
                    </div>

                    {{--@include('inventory::items.import')--}}

                    @if(Auth::user()->can('create-inventory'))
                        <a href="{{ route('items.create') }}" class="btn btn-success pull-right">
                            {{ trans("inventory::ui.items.new_items") }}
                        </a>
                    @endif

                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable" class="table table-striped" >
                                    <thead>
                                    <tr>
                                        <th>{{ trans('inventory::ui.items.sku') }}</th>
                                        <th>{{ trans('inventory::ui.items.description') }}</th>
                                        <th>{{ trans('inventory::ui.items.types') }}</th>
                                        <th>{{ trans('inventory::ui.items.status') }}</th>
                                        <th>{{ trans('inventory::ui.items.updated_at') }}</th>

                                        @if(Auth::user()->can(['update-inventory', 'delete-inventory']))
                                            <th>{{ trans('inventory::ui.types.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>{{ trans('inventory::ui.items.sku') }}</th>
                                        <th>{{ trans('inventory::ui.items.description') }}</th>
                                        <th>{{ trans('inventory::ui.items.types') }}</th>
                                        <th>{{ trans('inventory::ui.items.status') }}</th>
                                        <th>{{ trans('inventory::ui.items.updated_at') }}</th>

                                        @if(Auth::user()->can(['update-inventory', 'delete-inventory']))
                                            <th>{{ trans('inventory::ui.types.operation_label') }}</th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($items as $item)
                                        <tr>
                                            <td>{{ $item->code_sku }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>{{ $item->inventorytypes->type }}</td>
                                            <td>
                                                @if($item->status == 1)
                                                    <span class="badge badge-success">
                                                    {{ "Activo" }}
                                                    </span>
                                                @endif
                                                @if($item->status == 0)
                                                    <span class="badge badge-danger">
                                                    {{ "Desactivado" }}
                                                    </span>
                                                @endif
                                            </td>

                                            <td>{{ $item->updated_at }}</td>

                                            @if(Auth::user()->can(['update-inventory', 'delete-inventory']))
                                                <td>
                                                    @if(Auth::user()->can('update-inventory'))
                                                        <a href="{{ url('inventory/items/' . $item->id . '/edit') }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif

                                                    @if(Auth::user()->can('delete-inventory'))
                                                        <a href="#" data-url="{{ url('auth/user/'.$item->id) }}" class="btn btn-default btn-sm btn-icon-anim btn-circle">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                {!! $items->render() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
