# Laravel
Lumen es un microframework de laravel para la creacion de API REST. 
  - Facil de usar
  - Contiene la misma  estructura que laravel, lo cual si el proyecto es muy grande podemos migrarla. 
  - Se puede trabajar con MVC

# Aqui se define de como realice la instalacion de lumex, una documentacion de guia.
# Requerimientos!

- PHP >= 5.6.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension

Programas:  
  - Instalar el LAMP para linux O XAMPP para windows. 
  - Instalar composer https://getcomposer.org/
  
### Installation

Pasos para la instlacion de lumex

Primero que todo, si ya poseemos lamp o xampp y composer. Abriremos una terminal y teclearemos lo siguiente: 

```sh
$ composer global require "laravel/lumen-installer"
```
Esto nos instalara lumex en nuestro ordenador de forma global. Puede seguir la documentacion aqui: https://lumen.laravel.com/docs/5.4

Una vez realizada la instalacion descargaremos nuestro repositorio, clonar el repositorio en la ruta `/var/www/html` para linux, para windows en la carpeta `htdocs` que nos crea el XAMPP.
```sh
$ git clone https://Bolhy10@bitbucket.org/Bolhy10/backend.git
```
Para que nos funcione lumex en nuestro servidor de lamp o xampp debemos crear un virtual host para direccionar la carpeta "public" de nuestro proyecto y asi colocarla como principal y pueda leer el `index.php`. 

### Installation Linux 

Haremos lo siguiente, tomando en cuenta que la carpeta del proyecto se llame `backend.com`: 
Para linux de ubuntu, si contiene otra version de linux son los mismos pasos solo tiene que averiguar donde se encuentra las carpetas:  
- Colocamos nuestro proyecto en /var/www/html  o htdocs para windows
- Para linux le damos permisos al usuario root para que sea el propietario del directorio, para windows omite estos pasos.
```sh
$ sudo chown -R $USER:$USER /var/www/html/backend.com
```
La variable $USER contiene el valor del usuario que está ejecutando la acción. Aparte de esto, para poder acceder a las páginas correctamente, conviene dar otros permisos a la carpeta raíz del servidor:

```sh
$ sudo chmod -R 755 /var/www/html
```

- Crear el virtual host
Los archivos Virtual Host contienen información específica para cada dominio. Ya existe un archivo de configuración por defecto que utilizaremos como plantilla para crear el archivo de backend.com: 
```sh
$ sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/backend.com.conf
```
- Abrimos este archivo con permiso root, colocaremos el siguiente codigo
```sh
<VirtualHost *:80>
        ServerAdmin webadmin@dominio.com
        ServerName backend.com
        ServerAlias www.backend.com
        DocumentRoot /var/www/html/backend.com/public
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Aqui declaremos nuestro ServerName y colocamos la ruta, si pudo observar la ruta de nuestro proyecto va directamente a `public`. 
- Habilitar el nuevo virtual host
```sh
sudo a2ensite backend.com.conf
```
- Debemos abrir nuestro archivo hosts que se encuentra en /etc/hosts, agregamos la siguiente linea: 
```sh
127.0.0.1     backend.com
```
- Ya con esto reiniciamos apache, abrimos un navegador y colocamos nuestra ruta.  sudo /etc/init.d/apache2 restart

### Installation Windows
- Una vez instalada XAMPP, el siguiente paso es dirigirnos a: 
```sh
C:\WINDOWS\system32\drivers\etc\
```
En este archivo agregamos nuestro host virtual, para agregarlo lo hacemos de la siguiente manera:
```sh
127.0.0.1     backend.com
```
-  Ahora debemos modificar el archivo de configuración de Apache, para incluir el archivo de configuración de virtual host, lo podemos abrir de igual manera con un bloc de notas.
```sh
C:\xampp\apache\conf\httpd.conf
```
- Lo siguiente es buscar las siguientes dos líneas que están resaltadas:
`#Virtual hosts`
`Include conf/extra/httpd-vhosts.conf`
Eliminamos `#` en la segunda linea
- Además dentro del mismo archivo debemos asegurarnos de que el módulo Rewrite está habilitado, para ello buscamos la siguiente línea:
```sh
#LoadModule rewrite_module modules/mod_rewrite.so
```
Eliminamos el `#`, Y nos aseguramos de que no esté comentada. 
- Lo siguiente es abrir el archivo de configuración que nos provee XAMPP: 
```sh
C:\wamp\bin\apache\Apache2.2.21\conf\extra\httpd-vhosts.conf
```
Agregamos la siguiente linea: 
```sh
<VirtualHost *:80>
        ServerAdmin webadmin@dominio.com
        ServerName backend.com
        ServerAlias www.backend.com
        DocumentRoot /var/www/html/backend.com/public
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
`Nota: si te ha tocado cambiar el puerto donde escucha Apache que por defecto es 80 a otro puerto (ejemplo: 8080), en ese caso ese el número de puerto que debes de poner en el encabezado de Virtual Host ejemplo: <VirtualHost *:8080>`
- Ya con esto reiniciamos XAMPP, abrimos un navegador y colocamos nuestra ruta. 

### USO DE GIT

Para este proyecto usaremos GIT, lo cual debemos usar ramas en el proyecto. Aqui dejare los comandos que deben usar siempre cuando vayan a programar, siempre debes programar en su branch.  

| Accion | Proceso |
| ------ | ------ |
| Verificar que estamos en nuestro branch, sino estamos cambiamos de branch | git branch  - git branch checkout <branch> |
| Agregamos los archivos a nuestro branch  | git add . |
| Agregamos un comentario de las modificaciones realizadas | git commit -m "Comentario" |
| Subimos nuestras actualizaciones | git push |
| Ahora cambiamos al branch master | git checkout master |
| Debemos actualizar branch master | git pull |
| Ahora unimos nuestro branch con el master | git merge <branch>  |
| Verificamos el estado de los archivos | git status  |
| Ahora subimos los archivos al master | git push origin master  |
| Despues de realizar todo correcto, nos cambiamos a nuestro <branch> | git checkout <branch> |


### REALIZAR LOS SIGUIENTE: 
A cada proceso se debe realizar todos los metodos si lo requiere tanto el GET,POST, PUT, DELETE. 
En la raiz del documento hay un archivo llamado `documentation.txt`, donde debe colocar todas las routes para todos los metodos. Le dejo un ejemplo de guia.  


### Teams
- Bolivar Cortes
- Reynaldo Villarreal

License
----
MIT
**Free Software, Hell Yeah!**