<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 02/08/18
 * Time: 12:42 AM
 */

return [

    'dash'                              =>  [
        'data'                          =>  'Información sobre usuarios',
        'num_users'                     =>  [
            'label'                     => 'Usuarios',
            'title'                     => 'Cantidad de Usuarios'
        ],
        'num_roles'                     =>  [
            'label'                     => 'Roles',
            'title'                     => 'Cantidad de Roles'
        ],
        'num_permissions'               =>  [
            'label'                     => 'Permisos',
            'title'                     => 'Cantidad de Permisos'
        ],
    ],

    'users' => [
      'title' => 'Usuarios'
    ],

];