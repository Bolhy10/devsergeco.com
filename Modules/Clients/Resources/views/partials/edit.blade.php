@extends('layouts.master')

@section('style')

    {!! Html::style('assets/plugins/jquery-multi-select/css/multi-select.css') !!}

@stop

@section('content')

    <div class="row">
        <div class="col-md-8 col-lg-offset-2">

            @include('partials.message')

            @include('errors.form_error')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('clients::ui.clients.update.title') }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">

                            {!! Form::model($clients, ['method' => 'PUT', 'route' => ['clients.update', $clients->id]]) !!}

                            @include('clients::partials.form', array('clients' => $clients) + compact('warehouses', 'clients_warehouses'), ['button' => trans('clients::ui.clients.update.btn_update')])

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


@section('script')

    {!! Html::script('assets/plugins/jquery-multi-select/js/jquery.multi-select.js') !!}
    {!! Html::script('assets/js/query-init.js') !!}

@stop