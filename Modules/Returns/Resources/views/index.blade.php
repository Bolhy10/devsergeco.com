@extends('layouts.master')

@section('content')
    <div class="row">
        <!--div class="col-md-3 col-sm-12 col-lg-3 col-xs-12">
            
        </div-->
        <div class="col-md-12 col-lg-12 col-sm-12">
            <!-- inicio de la tabla -->
            <div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Devoluciones</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="datable_devolucion" class="table table-responsive table-hover display">
												<thead>
													<tr>
														<th>Id</th>
														<th>Codigo de Ot</th>
														<th>Proyecto</th>
														<th>Materiales</th>
														<th>Status</th>
														<th>Fecha de creación</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Id</th>
														<th>Codigo de Ot</th>
														<th>Proyecto</th>
														<th>Materiales</th>
														<th>Status</th>
														<th>Fecha de creación</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
            <!-- fin de la tabla--> 
        </div>
    </div>
@stop
@section('script')
    @include('partials.data-tables')
@endsection
