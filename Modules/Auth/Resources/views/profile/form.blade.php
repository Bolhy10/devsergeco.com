<div class="form-wrap">

    {!! Form::model($profile, ['method'=>'PUT', 'route'=>['profile.update'] ] ) !!}

        <div class="form-body overflow-hide">
            <div class="form-group">
                <label class="control-label mb-10" for="username">{{ trans('auth::ui.user.username') }}</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user"></i>
                    </div>
                    {!! Form::text('username', null, ['class'=> 'form-control', 'placeholder' => trans('auth::ui.user.username')]) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="control-label mb-10" for="firstname">{{ trans('auth::ui.user.firstname') }}</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user"></i>
                    </div>
                    {!! Form::text('firstname', null, ['class'=> 'form-control', 'placeholder' => trans('auth::ui.user.firstname')]) !!}
                </div>
            </div>


            <div class="form-group">
                <label class="control-label mb-10">{{ trans('auth::ui.user.lastname') }}</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                    {!! Form::text('lastname', null, ['class'=> 'form-control', 'placeholder' => trans('auth::ui.user.lastname')]) !!}
                </div>
            </div>


            <div class="form-group">
                <label class="control-label mb-10">{{ trans('auth::ui.user.email') }}</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                    {!! Form::text('email', null, ['class'=> 'form-control', 'placeholder' => trans('auth::ui.user.email')]) !!}
                </div>
            </div>


            <div class="form-group">
                <label class="control-label mb-10">{{ trans('auth::ui.user.phone') }}</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                    {!! Form::text('phone', null, ['class'=> 'form-control', 'placeholder' => trans('auth::ui.user.phone')]) !!}
                </div>
            </div>

        </div>

        <div class="form-actions mt-10">
            {!! Form::submit(trans('auth::ui.user.button_update'), ['class' => 'btn btn-success mr-10 mb-30']) !!}
        </div>

    {!! Form::close() !!}


</div>