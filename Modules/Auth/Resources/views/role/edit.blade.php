@extends('layouts.master')

@section('style')

	{!! Html::style('assets/plugins/jquery-multi-select/css/multi-select.css') !!}

@stop

@section('content')

	<div class="row">
		<div class="col-md-8 col-lg-offset-2">

			@include('partials.message')

			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">{{ trans('auth::ui.role.edit_role') }}</h6>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						@include('errors.form_error')
						<div class="form-wrap">

							{!! Form::model($role, ['method' => 'PUT', 'route' => ['role.update', $role->id]]) !!}

							@include('auth::role.form', array('role' => $role) + compact('permissions', 'role_permission'), ['button' => trans('auth::ui.role.button_update')])


							{!! Form::close() !!}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


@section('script')

	{!! Html::script('assets/plugins/jquery-multi-select/js/jquery.multi-select.js') !!}
	{!! Html::script('assets/js/query-init.js') !!}

@stop