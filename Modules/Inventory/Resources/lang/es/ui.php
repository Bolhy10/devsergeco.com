<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 03/18/18
 * Time: 12:31 PM
 */

return [

    'title' => 'Control de Inventarios',

    'types' => [
        'title' => 'Listado de Tipos de Inventarios',
        'type'=> 'Nombre',
        'status' => 'Estado',
        'operation_label' => 'Acciones',
        'new_types' => 'Nuevo Tipo',
        'button_add'    =>  'Agregar',
        'created_at' => 'Creado',
        'updated_at' => 'Actualizado',
        'create' => [
            'title' => 'Nuevo Tipo de inventario',
            'btn_add' => 'Guardar',
        ],

        'update' => [
            'title' => 'Actualizar Tipo',
            'btn_update' => 'Actualizar',
        ],


        'message_delete'        =>  'El Tipo :name ha sido eliminado satisfactoriamente',
        'message_create'        =>  'El Tipo :name ha sido creado satisfactoriamente',
        'message_update'        =>  'El Tipo :name ha sido actualizado satisfactoriamente'

    ],

    'inventory' => [

        'title' => 'Listado general',
        'subtitle' => 'Tabla de Inventarios',
        'filter' => 'Filtro por Cliente',
        'search' => 'Búsqueda por Código',
        'index' => 'ID',
        'code' => 'Código',
        'description' => 'Descripción',
        'type' => 'Tipo',
        'clients' => 'Cliente',
        'warehouse' => 'Almacén',
        'available' => 'Disponible',
        'update' => 'Actualizado',
        'not_found' => 'Hi! No hay items por el momento.'

    ],

    'items' => [
        'subtitle' => 'Listados de Artículos',
        'title'=> 'Control de Artículos',

        'sku' => 'Código',
        'description' => 'Descripción',
        'types' => 'Tipo',
        'unique' => 'Artículo único',
        'status' => 'Estado',

        'operation_label' => 'Acciones',
        'new_items' => 'Nuevo Artículo',
        'button_add'    =>  'Agregar Artículo',
        'updated_at'    =>  'Actualizado',
        'create' => [
            'title' => 'Nuevo Artículo',
            'btn_add' => 'Guardar',
        ],

        'update' => [
            'title' => 'Actualizar Artículo',
            'btn_update' => 'Actualizar',
        ],
        'error' => [
            'not_items' => 'Hi! No hay items por el momento.'
        ],
        'message_delete'        =>  'El Artículo :name ha sido eliminada satisfactoriamente',
        'message_create'        =>  'El Artículo :name ha sido creada satisfactoriamente',
        'message_update'        =>  'El Artículo :name ha sido actualizada satisfactoriamente'
    ],

    'ingress' => [
        'title' => "Control de ingresos",
        'subtitle' => 'Listado de Ingresos',
        'new_ingress' => 'Crear nuevo ingreso',
        'index' => 'ID',
        'clients' => 'Inventario de',
        'warehouse_client' => 'Almacén',
        'provider' => 'Proveedor',
        'ware_provider' => 'Almacén del Proveedor',
        'no_document' => '#Doc',
        'no_order' => '#Pedido',
        'no_transfer' => '#Transferencia',
        'deliver_to' => 'Entregado por',
        'receiver_to' => 'Recibido por',
        'observations' => 'Observaciones',
        'status' => 'Estado',
        'created_at' => 'Fecha',
        'updated_at' => 'Actualizado',
        'operation_label' => 'Acciones',
        'create' => [
            'title' => 'Nuevo Ingreso',
            'subtitle' => 'Formulario de registro: ',
            'title_inv' => 'Inventario',
            'hr_ingress' => 'Datos de ingreso'
        ],
        'table' => [
            'title' => 'Ingreso de Inventario',
            'code' => 'Código',
            'description' => 'Descripción',
            'type' => 'Tipo',
            'quantity' => 'Cantidad',
            'observations' => 'Observaciones'
        ],
        'error' => [
            'not_items' => 'Hi! No hay items por el momento.'
        ],
        'edit' => [
            'title' => 'Editar ingresos',
            'subtitle' => 'Formulario'

        ]
    ],

    'discard' => [
        'title' => "Control de descarte",
        'subtitle' => 'Listado de descarte',
        'new_discard' => 'Procesar descarte',
        'index' => 'ID',
        'clients' => 'Inventario de',
        'warehouse_client' => 'Almacén',
        'provider' => 'Proveedor',
        'ware_provider' => 'Almacén del Proveedor',
        'no_document' => '#Doc',
        'receive_to' => 'Recibido por',
        'observations' => 'Observaciones',
        'delivered' => 'Entregado por',
        'status' => 'Estado',
        'created_at' => 'Fecha',
        'updated_at' => 'Actualizado',
        'operation_label' => 'Acciones',
        'create' => [
            'title' => 'Nuevo Descarte',
            'subtitle' => 'Formulario de registro: ',
            'title_inv' => 'Inventario',
            'hr' => 'Datos de descarte'
        ],
        'table' => [
            'title' => 'Descarte de Inventario',
            'code' => 'Código',
            'description' => 'Descripción',
            'type' => 'Tipo',
            'quantity' => 'Cantidad',
            'observations' => 'Observaciones',
            'available' => 'Disponible',
        ],
        'error' => [
            'not_items' => 'Hi! No hay items por el momento.'
        ],
        'edit' => [
            'title' => 'Editar descarte',
            'subtitle' => 'Formulario'
        ]
    ]


];