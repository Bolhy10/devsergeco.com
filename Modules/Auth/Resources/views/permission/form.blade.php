<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.permission.name') }}</label>
	{!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=> 'Permiso']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.permission.display') }}</label>
	{!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder'=> 'Introduzca el nombre']) !!}
</div>

<div class="form-group">
	<label class="control-label mb-10">{{ trans('auth::ui.permission.description') }}</label>
	{!! Form::text('description', null, ['class' => 'form-control', 'placeholder'=> 'Introduzca una breve descripcion']) !!}
</div>

<div class="form-group mb-0">

	{!! Form::submit($button, ['class' => 'btn btn-primary']) !!}

	<a href="{{ route('permission.index') }}" class="btn btn-warning">{{ trans('auth::ui.general.back') }}</a>

</div>
