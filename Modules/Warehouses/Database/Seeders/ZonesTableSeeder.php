<?php

namespace Modules\Warehouses\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Warehouses\Entities\Zones;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $zones = [
            [
                'name' => 'Panamá',
                'address' => 'Mañanitas',
                'provinces_id' => 8,
                'latitude' => '9.087615',
                'longitude' => '-79.4078146',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1
            ],
            [
                'name' => 'Colón',
                'address' => 'Colón, Calle primera',
                'provinces_id' => 3,
                'latitude' => '90939393',
                'longitude' => '-79.93939393',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 1
            ]
        ];

        foreach ($zones as $key=>$value){
            Zones::create($value);
        }

    }
}
