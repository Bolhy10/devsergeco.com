<div class="form-group">
    <label class="control-label mb-10">{{ trans('warehouses::ui.warehouses.code') }}</label>
    {!! Form::text('code', null, ['class' => 'form-control', 'placeholder'=> trans('warehouses::ui.warehouses.code')]) !!}
</div>

<div class="form-group">
    <label class="control-label mb-10">{{ trans('warehouses::ui.warehouses.name') }}</label>
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=> trans('warehouses::ui.warehouses.name')]) !!}
</div>

<div class="form-group">
        <label class="control-label mb-10">{{ trans('warehouses::ui.warehouses.zone') }}</label>
        {!! Form::select('zones_id', $zones, null, ['class' => 'form-control', 'placeholder' => 'Seleccione la zona']) !!}
</div>


<div class="form-group">
    <label class="control-label mb-10">{{ trans('warehouses::ui.warehouses.status') }}</label>
    {!! Form::select('status', ['1' => 'Activo', '0' => 'Desactivado'], null, ['class'=> 'form-control','placeholder' => 'Seleccione el estado']) !!}
</div>

<div class="form-group mb-0">

    {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}

    <a href="{{ route('warehouses.index') }}" class="btn btn-warning">{{ trans('auth::ui.general.back') }}</a>

</div>