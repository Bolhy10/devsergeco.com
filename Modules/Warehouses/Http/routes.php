<?php

Route::group(['middleware' => 'web','namespace' => 'Modules\Warehouses\Http\Controllers'], function()
{
    //Route::get('/', 'WarehousesController@index');

    Route::resource('warehouses', 'WarehousesController');

    Route::resource('zones', 'ZonesController');

});
