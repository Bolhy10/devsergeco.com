@extends('layouts.master')

@section('pageTitle', trans('warehouses::ui.zones.title'))

@section('content')

    <div class="row">
        <div class="col-md-8 col-lg-offset-2">

            @include('partials.message')

            @include('errors.form_error')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ trans('warehouses::ui.zones.create.title') }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <div class="form-wrap">
                            {!! Form::open(array('route'=>'zones.store')) !!}

                            @include('warehouses::zones.form', ['button' => trans('warehouses::ui.zones.create.btn_add')])

                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop